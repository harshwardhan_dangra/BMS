USE [NMoneyBMS]
GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[UsersRegion_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[Users_List_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[UserRole_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[UserAuthToken_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[UserAuthToken_Select]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[UserAuthToken_Logout]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[UserAuthToken_Delete]
GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[User_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[User_Search]
GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[TableFilteredRow]
GO
/****** Object:  StoredProcedure [dbo].[Reason_Specific_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[Reason_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Branch_Specific_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[Branch_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Booking_UpSert]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[Booking_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Booking_Specific_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[Booking_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Booking_SelectById]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[Booking_SelectById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_DeleteById]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[Booking_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 8/28/2018 9:38:10 PM ******/
DROP PROCEDURE [dbo].[AccessRight_Select]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[AccessRight_Select] 
  @userId INT
 
AS

  SELECT [AccessRightsId]
     ,[UserId]
     ,[EntityType]
     ,[KorrectAccessRight]
     ,[SecurityPrincipalType]
     ,[AllowedRights]
     ,[DeniedRights]
    FROM [AccessRight]
    --INNER JOIN [USER] ON [AccessRight].UserId = [USER].UserID
  
  WHERE UserId = @userId


GO
/****** Object:  StoredProcedure [dbo].[Booking_DeleteById]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[Booking_DeleteById] 
	 @Id INT
	
AS

		DELETE
		
		FROM	Booking
		
		WHERE	Id = @Id



GO
/****** Object:  StoredProcedure [dbo].[Booking_SelectById]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Booking_SelectById]

@Id int

AS

Select * from Booking Where Id = CASE WHEN @Id IS NOT NULL THEN @Id ELSE Id END 

GO
/****** Object:  StoredProcedure [dbo].[Booking_Specific_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Booking_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select b.*,br.Name as Branch, re.Name as Reasonforenquiry  from Booking b left join Branch br on b.BranchId = br.Id
left join Reasonforenquiry re on b.ReasonforenquiryId = re.Id where b.Status = 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Booking_UpSert]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[Booking_UpSert] 	 
	 @Id INT OUT
	,@Name varchar(max) = null
	,@ContactNumber varchar(max) = null
	,@EmailAddress varchar(max) = null
	,@ReasonforenquiryId bigint = null
	,@BranchId bigint = null
	,@BookingDateandTime datetime = null
	, @Status bigint = null
	, @UserId bigint = null

AS

if(@Id > 0)
BEGIN

UPDATE [dbo].[Booking]
   SET [Name] = @Name
      ,[ContactNumber] = @ContactNumber
      ,[EmailAddress] = @EmailAddress
      ,[ReasonforenquiryId] = @ReasonforenquiryId
      ,[BranchId] = @BranchId
      ,[BookingDateandTime] = @BookingDateandTime
      ,[ModifiedDate] = GETDATE()
      ,[ModifiedBy] = @UserId
      ,[Status] = @Status
 WHERE Id = @Id

END

ELSE BEGIN

INSERT INTO [Booking]
           ([Name]
           ,[ContactNumber]
           ,[EmailAddress]
           ,[ReasonforenquiryId]
           ,[BranchId]
           ,[BookingDateandTime]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[Status])
     VALUES
           (@Name
           ,@ContactNumber
           ,@EmailAddress
           ,@ReasonforenquiryId
           ,@BranchId
           ,@BookingDateandTime
           ,GETDATE()
           ,@UserId
           ,@Status)
		   SET @Id = SCOPE_IDENTITY()
END
		   

GO
/****** Object:  StoredProcedure [dbo].[Branch_Specific_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Branch_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select * from Branch where Status = 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Reason_Specific_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Reason_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select * from Reasonforenquiry where Status = 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TableFilteredRow]
	(@TableName VARCHAR(500)
	,@Filters VARCHAR(500) = null
	,@SortExpression VARCHAR(500) = null
	,@Columns VARCHAR(MAX) = null
	,@PageSize INT = null
	,@Page INT = null)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @SqlStatement VARCHAR(MAX);

	IF (@Columns IS NULL OR LEN(@Columns) = 0)
	BEGIN
		SET @Columns = '*';
	END	

	SET @SqlStatement = 'SELECT ' + @Columns + ' FROM [' + LTRIM(@TableName) + '] WHERE 1 = 1';
	IF (@Filters IS NOT NULL AND LEN(@Filters) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' AND ' + @Filters;
	END

	IF (@SortExpression IS NOT NULL AND LEN(@SortExpression) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' ORDER BY ' + @SortExpression;
	END

	EXEC(@SqlStatement);
END

GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable READONLY
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
		Select *, FirstName + ' ' + LastName As FullName from [User] where Status <> 0




SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[User_UpSert] 	 
	 @Id INT OUT
	, @RoleId bigint = null
	, @FirstName varchar(200) = null
	, @LastName varchar(200) = null
	, @email varchar(200) = null
	, @Password varchar(200) = null
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	
	
	



AS

if(@Id > 0)
BEGIN

	update [User] set FirstName = @FirstName,
	LastName = @LastName,
	Email = @email,
	RoleId = @RoleId
	where UserID = @Id	

END

ELSE BEGIN

		INSERT INTO [dbo].[User]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[RoleId]
		   ,Status)
     VALUES
           (@FirstName
           ,@LastName
           ,@email
           ,@Password
           ,GETDATE()
           ,@CreatedBy
           ,@RoleId
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()
END
		   

GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	UserAuthToken
		
		WHERE	UserAuthTokenID = @Id



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Logout] 
    @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		UPDATE 	UserAuthToken

		SET		ExpiryDate = GETDATE()
						
		WHERE	TokenKey = @TokenKey 



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[UserAuthToken_Select] 
	 @Email NVARCHAR(100) = NULL
   , @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		SELECT 	UserAuthTokenID
				, UserAuthToken.UserId
				, TokenKey
				, LoginDate	
				, ExpiryDate

				, FirstName
				, LastName
				, Email
				, [Password]
			
		FROM	UserAuthToken
				INNER JOIN [USER] ON UserAuthToken.UserId = [USER].UserID				
		
		WHERE	Email = CASE WHEN @Email IS NOT NULL OR @Email <> '' THEN @Email ELSE Email END
				AND TokenKey = CASE WHEN @TokenKey IS NOT NULL OR @TokenKey <> '' THEN @TokenKey ELSE TokenKey END
				AND ExpiryDate > GETDATE()


GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_UpSert] 
	  @Id INT OUT
	, @UserId INT
	, @TokenKey UNIQUEIDENTIFIER
	, @LoginDate NVARCHAR(100)
	, @ExpiryDate DATETIME
	, @ExpiryHours INT
	, @Status INT 

AS

		IF EXISTS (SELECT * FROM UserAuthToken WHERE UserId = @UserId AND ExpiryDate > Getdate()) 
			BEGIN

				UPDATE	[UserAuthToken] 

				SET		@Id = UserAuthTokenID
						, ExpiryDate = DATEADD(hh, @ExpiryHours, getdate())

				WHERE UserId = @UserId AND ExpiryDate > GETDATE()

			END

		ELSE
			BEGIN

				INSERT INTO [UserAuthToken]  (UserId,  TokenKey,  LoginDate, ExpiryDate)
							Values	(@UserId, @TokenKey, @LoginDate, DATEADD(hh, @ExpiryHours, getdate()))

				SET @Id = SCOPE_IDENTITY()

			END


GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	Select * from UserRole Where Role like '%'+@searchText+'%' and UserRoleId <> 1
	



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Users_List_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly 
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 8/28/2018 9:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[UsersRegion_UpSert] 	 
	 @Id INT OUT
	, @RegionId bigint = null
	, @UserId bigint = null	
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	
	
	

AS


--- delete all then insert

Delete UsersRegion where UserId = @UserId


		INSERT INTO [dbo].[UsersRegion]
           (
		   RegionId
		   ,UserId
           ,[CreatedDate]
           ,[CreatedBy]          
		   ,Status)
     VALUES
           (@RegionId
           ,@UserId                      
           ,GETDATE()
           ,@CreatedBy           
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()

GO
