USE [NMMoneyBMS_Dev_v2]
GO

/****** Object:  StoredProcedure [dbo].[Booking_UpSert]    Script Date: 9/6/2018 2:03:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







ALTER  PROCEDURE [dbo].[Booking_UpSert] 	 
	 @Id INT OUT
	,@FirstName varchar(max) = null
	,@LastName varchar(max) = null
	,@Latitude varchar(max) = null
	,@BookingType bigint = null
	,@Longitude varchar(max) = null
	,@Location varchar(max)  = null
	,@ContactNumber varchar(max) = null
	,@EmailAddress varchar(max) = null
	,@ReasonforenquiryId bigint = null
	,@BranchId bigint = null
	,@BookingDateandTime datetime = null
	, @Status bigint = null
	, @UserId bigint = null
	,@Comment varchar(max)  = null

AS

if(@Id > 0)
BEGIN

UPDATE [dbo].[Booking]
   SET FirstName = @FirstName
		,LastName = @LastName
		,Latitude = @Latitude
		,Longitude = @Longitude
		,location = @Location
		,BookingType = @BookingType
      ,[ContactNumber] = @ContactNumber
      ,[EmailAddress] = @EmailAddress
      ,[ReasonforenquiryId] = @ReasonforenquiryId
      ,[BranchId] = @BranchId
      ,[BookingDateandTime] = @BookingDateandTime
      ,[ModifiedDate] = GETDATE()
      ,[ModifiedBy] = @UserId
      ,[Status] = @Status
	  ,[Comment] = @Comment
 WHERE Id = @Id

END

ELSE BEGIN

INSERT INTO [Booking]
           (FirstName
		   ,LastName
		   ,BookingType
		   ,Latitude
		   ,Longitude
		   ,Location
           ,[ContactNumber]
           ,[EmailAddress]
           ,[ReasonforenquiryId]
           ,[BranchId]
           ,[BookingDateandTime]
           ,[CreatedDate]
           ,[CreatedBy]
		   ,ModifiedDate
		   ,ModifiedBy
           ,[Status]
		   ,[Comment])
     VALUES
           (@FirstName
		   ,@LastName
		   ,@BookingType
		   ,@Latitude
		   ,@Longitude
		   ,@Location
           ,@ContactNumber
           ,@EmailAddress
           ,@ReasonforenquiryId
           ,@BranchId
           ,@BookingDateandTime
           ,GETDATE()
           ,@UserId
		   ,GETDATE()
           ,@UserId
           ,@Status
		   ,@Comment)
		   SET @Id = SCOPE_IDENTITY()
END
		   


GO


