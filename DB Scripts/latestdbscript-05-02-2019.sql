USE [NMoneyBMS]
GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[UsersRegion_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Users_List_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[UserRole_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_Select]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_Logout]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[UserAuthToken_Delete]
GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[User_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[User_SelectById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[User_SelectById]
GO
/****** Object:  StoredProcedure [dbo].[User_Select_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[User_Select_ById]
GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[User_Search]
GO
/****** Object:  StoredProcedure [dbo].[User_DeleteById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[User_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[User_Delete]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[User_Delete]
GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[TableFilteredRow]
GO
/****** Object:  StoredProcedure [dbo].[Settings_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Settings_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Settings_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Settings_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Reason_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Reason_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Branch_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Branch_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Branch_UnArchive_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Branch_UnArchive_ById]
GO
/****** Object:  StoredProcedure [dbo].[Branch_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Branch_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Branch_SelectById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Branch_SelectById]
GO
/****** Object:  StoredProcedure [dbo].[Branch_HeadBranch_Capacity]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Branch_HeadBranch_Capacity]
GO
/****** Object:  StoredProcedure [dbo].[Branch_DeleteById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Branch_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[Branch_Archive_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Branch_Archive_ById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Booking_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Booking_UnArchive_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Booking_UnArchive_ById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Booking_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Booking_SelectById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Booking_SelectById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_Search_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Booking_Search_ById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_DeleteById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Booking_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_Archive_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Booking_Archive_ById]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Appointment_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_TimeSlots_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Appointment_TimeSlots_Search]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Appointment_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_DeleteByDate]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Appointment_DeleteByDate]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_AllTimeSlots_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[Appointment_AllTimeSlots_Search]
GO
/****** Object:  StoredProcedure [dbo].[AllBranchTotalCapacity]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[AllBranchTotalCapacity]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 2/5/2019 10:15:44 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[AccessRight_Select]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccessRight_Select]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AccessRight_Select] AS' 
END
GO
ALTER  PROCEDURE [dbo].[AccessRight_Select] 
  @userId INT
 
AS

  SELECT [AccessRightsId]
     ,[UserId]
     ,[EntityType]
     ,[KorrectAccessRight]
     ,[SecurityPrincipalType]
     ,[AllowedRights]
     ,[DeniedRights]
    FROM [AccessRight]
    --INNER JOIN [USER] ON [AccessRight].UserId = [USER].UserID
  
  WHERE UserId = @userId

GO
/****** Object:  StoredProcedure [dbo].[AllBranchTotalCapacity]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AllBranchTotalCapacity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AllBranchTotalCapacity] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AllBranchTotalCapacity]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@appointmentType bigint = null,
		@AppointmentDate date = null,
		@AppointmentTime time = null,
		@AppointmentAvailable bigint = null,
		@currentslotavailable bigint = null,
		@branchId bigint = null
	

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentType')
BEGIN
	SET @appointmentType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentType')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentDate')
BEGIN
	SET @AppointmentDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentDate')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentTime')
BEGIN
	SET @AppointmentTime =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentTime')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'currentslotavailable')
BEGIN
	SET @currentslotavailable =(Select Value FROM  @ItemsTable WHERE [KEY] = 'currentslotavailable')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'branchId')
BEGIN
	SET @branchId =(Select Value FROM  @ItemsTable WHERE [KEY] = 'branchId')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
select distinct @AppointmentAvailable = 
--Sum(1) OVER() as TotalBooked, 
--(Select VideoCapacity from Branch Where Id = 3) as HeadBranchCapacity,
Case  When Sum(1) OVER() < (Select VideoCapacity from Branch Where Id = 3) then 1
When Sum(1) OVER() >= (Select VideoCapacity from Branch Where Id = 3) then 0 
end 
 from Appointment
 
  where 
  AppointmentDate = @AppointmentDate
 and AppointmentTime = @AppointmentTime
 and 
AppointmentType=@appointmentType
and BranchId  = @branchId
if ISNULL(@currentslotavailable,0) > (Select VideoCapacity from Branch Where Id = 3)
Begin
SET @AppointmentAvailable = 0
End

if ISNULL(@currentslotavailable,0) > (Select VideoCapacity from Branch Where Id = @branchId)
Begin
SET @AppointmentAvailable = 0
End

Select isnull(@AppointmentAvailable,1) AppointmentAvailable
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[Appointment_AllTimeSlots_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Appointment_AllTimeSlots_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Appointment_AllTimeSlots_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Appointment_AllTimeSlots_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@appointmentType bigint = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentType')
BEGIN
	SET @appointmentType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentType')
END



IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select *, ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) as SortOrder from (
Select distinct AppointmentTime, CONVERT(CHAR(5), AppointmentTime, 108) as AppointmentTimeString
 from Appointment a Where 1=1 
 and 1=
 Case When ISNULL(@appointmentType,0) = 0 then 1
 When ISNULL(@appointmentType,0) <> 0 and AppointmentType = @appointmentType then 1
 end
order by AppointmentTime asc OFFSET 0 ROWS
   )
   appointment

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[Appointment_DeleteByDate]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Appointment_DeleteByDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Appointment_DeleteByDate] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Appointment_DeleteByDate] 	 
	@AppointmentDate datetime = null
	,@AppointmentType bigint = null
	,@BranchId bigint = null	
AS
Delete from Appointment where AppointmentDate = CONVERT(DATE, CONVERT(CHAR(8), @AppointmentDate, 112)) and AppointmentType = @AppointmentType
and BranchId = @BranchId

GO
/****** Object:  StoredProcedure [dbo].[Appointment_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Appointment_Specific_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Appointment_Specific_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Appointment_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@appointmentType bigint = null,
		@branchId bigint = null,
		@currentDateTime datetime = null,
		@selectedDate datetime = null,
		@selectedTime	datetime = null,
		@sortOrder varchar(max) = null,
		@selectedDay varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentType')
BEGIN
	SET @appointmentType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentType')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'branchId')
BEGIN
	SET @branchId =(Select Value FROM  @ItemsTable WHERE [KEY] = 'branchId')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'currentDateTime')
BEGIN
	SET @currentDateTime =(Select Value FROM  @ItemsTable WHERE [KEY] = 'currentDateTime')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedDate')
BEGIN
	SET @selectedDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedDate')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedTime')
BEGIN
	SET @selectedTime =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedTime')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'sortOrder')
BEGIN
	SET @sortOrder =(Select Value FROM  @ItemsTable WHERE [KEY] = 'sortOrder')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedDay')
BEGIN
	SET @selectedDay =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedDay')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
IF @currentDateTime is not null
Begin 
Declare @appointment table (
AppointmentAvailable int null,
Id int null,
AppointmentDate date null,
AppointmentTime time null,
AppointmentDay varchar(max) null,
AppointmentType bigint null,
TotalCapacity int null,
CreatedBy bigint null,
CreatedDate datetime null,
ModifiedBy bigint null,
ModifiedDate datetime null,
Status int null,
Branch varchar(max) null,
BranchId int null,
BookedAppointment int null
)

Declare @lastWeekDateTime datetime, @updatedcurrentDateTime datetime
Select @lastWeekDateTime = DateAdd(DD,180,@currentDateTime)
Select @updatedcurrentDateTime = @currentDateTime
While @updatedcurrentDateTime <= @lastWeekDateTime
Begin

 Select @selectedDay = DATENAME(dw,@updatedcurrentDateTime)
insert into @appointment
Select AppointmentAvailable = (Case When TotalCapacity <= BookedAppointment or Status = 0 
or ISNULL((Select Status from Appointment a where a.BranchId = 3 and a.AppointmentDate = appointment.AppointmentDate 
and a.AppointmentType = appointment.AppointmentType
and a.AppointmentTime = appointment.AppointmentTime
),0) = 0 or TotalCapacity < = SUM(BookedAppointment) OVER (PArtition By appointment.AppointmentDate,appointment.AppointmentTime,appointment.AppointmentType)
then 0
						Else 1 end), 
						Id,AppointmentDate,AppointmentTime,AppointmentDay,AppointmentType,TotalCapacity,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,
						Status,Branch,BranchId,
						Case When @branchId Is null or @branchId <> 3 then BookedAppointment
						When @branchId = 3 then (Select SUM(cnt) from (Select count(*) as cnt from Booking where convert(varchar, BookingDateandTime, 23) = appointment.AppointmentDate
 and convert(varchar, BookingDateandTime, 14) = appointment.AppointmentTime) as t)
						End as BookedAppointment

 from (
Select *
,(Select Name from Branch  where Id = a.BranchId) as Branch
, BookedAppointment = (Select count(*) from Booking where convert(varchar, BookingDateandTime, 23) = a.AppointmentDate
 and convert(varchar, BookingDateandTime, 14) = a.AppointmentTime and BranchId = a.BranchId)
 
 from Appointment a Where 1=1 
  and 1=
 Case When ISNULL(@appointmentType,0) = 0 then 1
 When ISNULL(@appointmentType,0) <> 0 and AppointmentType = @appointmentType then 1
 end
 and 
 1= Case When @branchId is null and @branchId <> 3 then 1
 When BranchId = @branchId then 1
  end
 and 1= Case When ISNULL(@updatedcurrentDateTime,0) = 0 then 1
 When ISNULL(@updatedcurrentDateTime,0) <> 0  and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112) 
  + ' ' + CONVERT(CHAR(8), AppointmentTime, 108)) = @updatedcurrentDateTime  
 Then 1
  end
   and 1= Case When ISNULL(@selectedDate,0) = 0 then 1
 When ISNULL(@selectedDate,0) <> 0  and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112)) = @selectedDate  
 Then 1
   end
     and 1= Case When ISNULL(@selectedTime,0) = 0 then 1
 When ISNULL(@selectedTime,0) <> 0  and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentTime, 108)) = @selectedTime  
 Then 1
   end
 ) appointment order by
 (CASE  WHEN isnull(@sortOrder,'')=''  THEN AppointmentDate end) 
 ,(CASE  WHEN isnull(@sortOrder,'')<>'' and @sortOrder = 'desc' THEN AppointmentDate end) DESC

 
   If (Select count(*) from @appointment Where AppointmentDate = Convert(date,@updatedcurrentDateTime) ) = 0
   Begin
   Declare @openinghours int
   Declare @closinghours int
      Declare @headbranchopeninghours int
   Declare @headbranchclosinghours int
   IF @selectedDay = 'Sunday'
   BEGIN
	Set @openinghours = Convert(int,(Select Top 1 Value from string_split((Select SundayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select SundayClosingHours from Branch where Id = @branchId),':'))) 
 	Set @headbranchopeninghours = Convert(int,(Select Top 1 Value from string_split((Select SundayOpeningHours from Branch where Id = 3),':')))
	Set @headbranchclosinghours =  Convert(int,(Select Top 1 Value from string_split((Select SundayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Monday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select MondayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours = Convert(int,(Select Top 1 Value from string_split((Select MondayClosingHours from Branch where Id = @branchId),':')))
 	Set @headbranchopeninghours = Convert(int,(Select Top 1 Value from string_split((Select MondayOpeningHours from Branch where Id = 3),':')))
	Set @headbranchclosinghours =  Convert(int,(Select Top 1 Value from string_split((Select MondayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Tuesday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select TuesdayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select TuesdayClosingHours from Branch where Id = @branchId),':')))
 	Set @headbranchopeninghours = Convert(int,(Select Top 1 Value from string_split((Select TuesdayOpeningHours from Branch where Id = 3),':')))
	Set @headbranchclosinghours =  Convert(int,(Select Top 1 Value from string_split((Select TuesdayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Wednesday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select WednesdayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select WednesdayClosingHours from Branch where Id = @branchId),':')))
 	Set @headbranchopeninghours = Convert(int,(Select Top 1 Value from string_split((Select WednesdayOpeningHours from Branch where Id = 3),':')))
	Set @headbranchclosinghours =  Convert(int,(Select Top 1 Value from string_split((Select WednesdayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Thursday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select ThursdayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select ThursdayClosingHours from Branch where Id = @branchId),':')))
 	Set @headbranchopeninghours = Convert(int,(Select Top 1 Value from string_split((Select ThursdayOpeningHours from Branch where Id = 3),':')))
	Set @headbranchclosinghours =  Convert(int,(Select Top 1 Value from string_split((Select ThursdayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Friday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select FridayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select FridayClosingHours from Branch where Id = @branchId),':')))
 	Set @headbranchopeninghours = Convert(int,(Select Top 1 Value from string_split((Select FridayOpeningHours from Branch where Id = 3),':')))
	Set @headbranchclosinghours =  Convert(int,(Select Top 1 Value from string_split((Select FridayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Saturday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select SaturdayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select SaturdayClosingHours from Branch where Id = @branchId),':')))
 	Set @headbranchopeninghours = Convert(int,(Select Top 1 Value from string_split((Select SaturdayOpeningHours from Branch where Id = 3),':')))
	Set @headbranchclosinghours =  Convert(int,(Select Top 1 Value from string_split((Select SaturdayClosingHours from Branch where Id = 3),':')))
   END
   IF @appointmentType = 1 and @branchId = 0
   BEGIN
	set @openinghours = @headbranchopeninghours
	set @closinghours = @headbranchclosinghours
   END
   Declare @i  int = 1
   IF @openinghours is not null and @closinghours is not null
   Begin
	While @openinghours <= @closinghours
	Begin		
	Insert into @appointment 
	Select AppointmentAvailable = (Case When TotalCapacity <= BookedAppointment or Status = 0 
or  (@openinghours < @headbranchopeninghours or @openinghours > @headbranchclosinghours)
or isnull(@headbranchopeninghours,0) = 0 
or isnull(@headbranchclosinghours,0) = 0 
 or TotalCapacity < = SUM(BookedAppointment) OVER (PArtition By convert(date, @updatedcurrentDateTime),convert(time, @updatedcurrentDateTime),@appointmentType)
then 0
						Else 1 end), 
			* from (	
	Select
	0 as Id ,@updatedcurrentDateTime as AppointmentDate,Convert(varchar(max),@openinghours) + ':00' as AppointmentTime,@selectedDay as AppointmentDay,
	@appointmentType as AppointmentType,(Select VideoCapacity from Branch where Id = @branchId) as TotalCapacity,1 as CreatedBy,GetdAte() as CreatedDate,
	1 as ModifiedBy,Getdate() as ModifiedDate,1 as Status,(Select Name from Branch where Id = @branchId) as Branch,@branchId as BranchId, 
	BookedAppointment = (Select count(*) from Booking where convert(varchar, BookingDateandTime, 23) = convert(varchar, @updatedcurrentDateTime, 23)
 and convert(varchar, BookingDateandTime, 14) = Convert(varchar(max),@openinghours) + ':00:00:000' and BranchId = @branchId)) t

	Set @i = @i + 1
	Set @openinghours = @openinghours + 1
	END
   END
   END
    Set @updatedcurrentDateTime =  DateAdd(DD,1,@updatedcurrentDateTime)
End

 Select * from @appointment Where  1= 1 and 1= Case When ISNULL(@currentDateTime,0) = 0 then 1
 When ISNULL(@currentDateTime,0) <> 0  and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112) 
  + ' ' + CONVERT(CHAR(8), AppointmentTime, 108)) >= @currentDateTime  
 Then 1 end
 END
 If @currentDateTime is null 
 BEgin
 Select * from Appointment where BranchId = @branchId and AppointmentType = @appointmentType
 End
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[Appointment_TimeSlots_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Appointment_TimeSlots_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Appointment_TimeSlots_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Appointment_TimeSlots_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@appointmentType bigint = null,
		@branchId bigint = null,
		@currentDateTime datetime = null,
		@selectedStartDate datetime = null,
		@selectedEndDate	datetime = null,
		@selectedDay varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentType')
BEGIN
	SET @appointmentType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentType')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'branchId')
BEGIN
	SET @branchId =(Select Value FROM  @ItemsTable WHERE [KEY] = 'branchId')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedStartDate')
BEGIN
	SET @selectedStartDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedStartDate')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedEndDate')
BEGIN
	SET @selectedEndDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedEndDate')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedDay')
BEGIN
	SET @selectedDay =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedDay')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
Declare @appointment table (
AppointmentTime time null,
TotalCapacity int null,
Status int null,
AppointmentTimeString varchar(max) null,
SortOrder int null
)

insert into @appointment 
Select *, ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) as SortOrder from (
Select distinct AppointmentTime, TotalCapacity, Status, CONVERT(CHAR(5), AppointmentTime, 108) as AppointmentTimeString
 from Appointment a Where 1=1 
 and 1=
 Case When ISNULL(@appointmentType,0) = 0 then 1
 When ISNULL(@appointmentType,0) <> 0 and AppointmentType = @appointmentType then 1
 end
 and 
 1= Case When ISNULL(@branchId,0) = 0 and BranchId = @branchId then 1
 When ISNULL(@branchId,0) <> 0 and BranchId = @branchId then 1
 end
   and 1= Case When ISNULL(@selectedStartDate,0) = 0 then 1
 When ISNULL(@selectedStartDate,0) <> 0 and ISNULL(@selectedEndDate,0) <> 0   and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112)) >= @selectedStartDate  
 Then 1
 When ISNULL(@selectedStartDate,0) <> 0   and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112)) = @selectedStartDate  
 Then 1
   end

      and 1= Case When ISNULL(@selectedEndDate,0) = 0 then 1
 When ISNULL(@selectedEndDate,0) <> 0  and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112)) <= @selectedEndDate  
 Then 1
   end 
order by AppointmentTime asc OFFSET 0 ROWS
   )
   appointment

   If (Select count(*) from @appointment) = 0
   Begin
   Declare @openinghours int
   Declare @closinghours int
   IF @selectedDay = 'Sunday'
   BEGIN
	Set @openinghours = Convert(int,(Select Top 1 Value from string_split((Select SundayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select SundayClosingHours from Branch where Id = @branchId),':')))
   END
   IF @selectedDay = 'Monday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select MondayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours = Convert(int,(Select Top 1 Value from string_split((Select MondayClosingHours from Branch where Id = @branchId),':')))
   END
   IF @selectedDay = 'Tuesday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select TuesdayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select TuesdayClosingHours from Branch where Id = @branchId),':')))
   END
   IF @selectedDay = 'Wednesday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select WednesdayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select WednesdayClosingHours from Branch where Id = @branchId),':')))
   END
   IF @selectedDay = 'Thursday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select ThursdayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select ThursdayClosingHours from Branch where Id = @branchId),':')))
   END
   IF @selectedDay = 'Friday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select FridayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select FridayClosingHours from Branch where Id = @branchId),':')))
   END
   IF @selectedDay = 'Saturday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select SaturdayOpeningHours from Branch where Id = @branchId),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select SaturdayClosingHours from Branch where Id = @branchId),':')))
   END
   Declare @i  int = 1
   IF @openinghours is null or @closinghours is null
   Begin
	IF @selectedDay = 'Sunday'
   BEGIN
	Set @openinghours = Convert(int,(Select Top 1 Value from string_split((Select SundayOpeningHours from Branch where Id = 3),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select SundayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Monday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select MondayOpeningHours from Branch where Id = 3),':')))
	Set @closinghours = Convert(int,(Select Top 1 Value from string_split((Select MondayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Tuesday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select TuesdayOpeningHours from Branch where Id = 3),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select TuesdayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Wednesday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select WednesdayOpeningHours from Branch where Id = 3),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select WednesdayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Thursday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select ThursdayOpeningHours from Branch where Id = 3),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select ThursdayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Friday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select FridayOpeningHours from Branch where Id = 3),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select FridayClosingHours from Branch where Id = 3),':')))
   END
   IF @selectedDay = 'Saturday'
   BEGIN
	Set @openinghours =  Convert(int,(Select Top 1 Value from string_split((Select SaturdayOpeningHours from Branch where Id = 3),':')))
	Set @closinghours =  Convert(int,(Select Top 1 Value from string_split((Select SaturdayClosingHours from Branch where Id = 3),':')))
   END
   END
   IF @openinghours is not null and @closinghours is not null
   Begin
	While @openinghours <= @closinghours
	Begin
	Insert into @appointment values(Convert(varchar(max),@openinghours) + ':00',0,1,Convert(varchar(max),@openinghours) + ':00', @i)
	Set @i = @i + 1
	Set @openinghours = @openinghours + 1
	END
   END
   END

   Select * from @appointment

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[Appointment_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Appointment_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Appointment_UpSert] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Appointment_UpSert] 	 
	 @Id INT OUT
	,@AppointmentDate datetime = null
	,@AppointmentTime datetime = null
	,@AppointmentDay varchar(max) = null
	,@AppointmentType bigint = null
	,@OpeningTimesStart datetime = null
	,@OpeningTimesEnd datetime = null
	,@TotalCapacity bigint = null
	,@BranchId bigint = null
	, @Status bigint = null
	, @UserId bigint = null

AS

--if(@Id > 0)
--BEGIN

--UPDATE [dbo].Appointment
--   SET AppointmentDate = @AppointmentDate
--		,AppointmentTime = @AppointmentTime
--		,AppointmentType = @AppointmentType
--		,AppointmentDay = @AppointmentDay
--		,OpeningTimesStart = @OpeningTimesStart
--		,OpeningTimesEnd = @OpeningTimesEnd
--		,TotalCapacity = @TotalCapacity
--      ,BranchId = @BranchId
--      ,[ModifiedDate] = GETDATE()
--      ,[ModifiedBy] = @UserId
--      ,[Status] = @Status
-- WHERE Id = @Id

--END

--ELSE BEGIN
--Select CONVERT(DATE, CONVERT(CHAR(8), @AppointmentDate, 112)) 
INSERT INTO Appointment
           (AppointmentDate
		   ,AppointmentTime
		   ,AppointmentDay
		   ,AppointmentType
		   ,OpeningTimesStart
		   ,OpeningTimesEnd
           ,TotalCapacity
           ,BranchId         
           ,[CreatedDate]
           ,[CreatedBy]
		   ,ModifiedDate
		   ,ModifiedBy
           ,[Status])
     VALUES
           (@AppointmentDate
		   ,@AppointmentTime
		   ,@AppointmentDay
		   ,@AppointmentType
		   ,@OpeningTimesStart
		   ,@OpeningTimesEnd
           ,@TotalCapacity
           ,@BranchId
           ,GETDATE()
           ,@UserId
		   ,GETDATE()
           ,@UserId
           ,@Status)
		   SET @Id = SCOPE_IDENTITY()
--END

GO
/****** Object:  StoredProcedure [dbo].[Booking_Archive_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Booking_Archive_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Booking_Archive_ById] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Booking_Archive_ById] 
	 @Id INT
	
AS

		Update Booking 
		set Status = 0
		
		WHERE	Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[Booking_DeleteById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Booking_DeleteById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Booking_DeleteById] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Booking_DeleteById] 
	 @Id INT
	
AS

		update Booking
		
		Set Status = 2
		
		WHERE	Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[Booking_Search_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Booking_Search_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Booking_Search_ById] AS' 
END
GO
ALTER PROCEDURE [dbo].[Booking_Search_ById]

@Id int

AS

Select distinct bo.*,br.Name as Branch, roe.Name as Reasonforenquiry
, u.FirstName + ' ' + u.LastName as AssignedUser
 from Booking bo
left join Branch br on bo.BranchId = br.Id
left join Reasonforenquiry roe on bo.ReasonforenquiryId = roe.Id
left join [user] u on bo.AssignedTo = u.UserId
 Where bo.Id = CASE WHEN @Id IS NOT NULL THEN @Id ELSE bo.Id END

GO
/****** Object:  StoredProcedure [dbo].[Booking_SelectById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Booking_SelectById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Booking_SelectById] AS' 
END
GO
ALTER PROCEDURE [dbo].[Booking_SelectById]

@Id int

AS

Select * from Booking Where Id = CASE WHEN @Id IS NOT NULL THEN @Id ELSE Id END

GO
/****** Object:  StoredProcedure [dbo].[Booking_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Booking_Specific_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Booking_Specific_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Booking_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@bookingType int = null,
		@status int = null,
		@branchId bigint  = null,
		@bookingStartDate datetime = null,
		@bookingEndDate datetime = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'bookingType')
BEGIN
	SET @bookingType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'bookingType')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'status')
BEGIN
	SET @status =(Select Value FROM  @ItemsTable WHERE [KEY] = 'status')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'branchId')
BEGIN
	SET @branchId =(Select Value FROM  @ItemsTable WHERE [KEY] = 'branchId')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'bookingStartDate')
BEGIN
	SET @bookingStartDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'bookingStartDate')
END


IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'bookingEndDate')
BEGIN
	SET @bookingEndDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'bookingEndDate')
END


IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
Select b.*,br.Name as Branch, re.Name as Reasonforenquiry , u.FirstName + ' ' + u.LastName as AssignedUser,
 Case When @status is null then 1
when @status is not null and  b.Status = @status then 1 end
 from Booking b left join Branch br on b.BranchId = br.Id
left join Reasonforenquiry re on b.ReasonforenquiryId = re.Id
left join [user] u on b.AssignedTo = u.UserId
 where 1=1

and 
1 = Case When @status is null then 1
when @status is not null and  b.Status = @status then 1 end

and 1= Case When ISNULL(@bookingType,0) = 0 then 1
When ISNULL(@bookingType,0)<> 0 and b.BookingType = @bookingType then 1 end

and 1= Case When ISNULL(@branchId,0) = 0 then 1
When ISNULL(@branchId,0)<> 0 and b.BranchId = @branchId then 1 end

and 1= Case When ISNULL(@bookingStartDate,'') = '' then 1
When ISNULL(@bookingStartDate,'')<> '' and Convert(date,b.BookingDateandTime) >= Convert(date,@bookingStartDate) then 1 end

and 1= Case When ISNULL(@bookingEndDate,'') = '' then 1
When ISNULL(@bookingEndDate,'')<> '' and Convert(date,b.BookingDateandTime) <= Convert(date,@bookingEndDate) then 1 end
order by b.BookingDateandTime desc

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[Booking_UnArchive_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Booking_UnArchive_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Booking_UnArchive_ById] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Booking_UnArchive_ById] 
	 @Id INT
	
AS

		Update Booking 
		set Status = 1
		
		WHERE	Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[Booking_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Booking_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Booking_UpSert] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Booking_UpSert] 	 
	 @Id INT OUT
	,@FirstName varchar(max) = null
	,@LastName varchar(max) = null
	,@Latitude varchar(max) = null
	,@BookingType bigint = null
	,@Longitude varchar(max) = null
	,@Location varchar(max)  = null
	,@ContactNumber varchar(max) = null
	,@EmailAddress varchar(max) = null
	,@ReasonforenquiryId bigint = null
	,@BranchId bigint = null
	,@BookingDateandTime datetime = null
	, @Status bigint = null
	, @UserId bigint = null
	,@Comment varchar(max) = null
	,@AccessCode varchar(max) = null
	,@AssignedTo bigint = null
	,@AccessCodeChanged bit = null
	,@Referrer varchar(max) = null

AS

if(@Id > 0)
BEGIN

UPDATE [dbo].[Booking]
   SET FirstName = @FirstName
		,LastName = @LastName
		,Latitude = @Latitude
		,Longitude = @Longitude
		,location = @Location
		,BookingType = @BookingType
      ,[ContactNumber] = @ContactNumber
      ,[EmailAddress] = @EmailAddress
      ,[ReasonforenquiryId] = @ReasonforenquiryId
      ,[BranchId] = @BranchId
      ,[BookingDateandTime] = @BookingDateandTime
      ,[ModifiedDate] = GETDATE()
      ,[ModifiedBy] = @UserId
      ,[Status] = @Status
	  ,Comment = @Comment
	  ,AssignedTo = @AssignedTo
	  ,AccessCode = @AccessCode
	  ,Referrer = @Referrer
 WHERE Id = @Id

END

ELSE BEGIN

INSERT INTO [Booking]
           (FirstName
		   ,LastName
		   ,BookingType
		   ,Latitude
		   ,Longitude
		   ,Location
           ,[ContactNumber]
           ,[EmailAddress]
           ,[ReasonforenquiryId]
           ,[BranchId]
           ,[BookingDateandTime]
           ,[CreatedDate]
           ,[CreatedBy]
		   ,ModifiedDate
		   ,ModifiedBy
           ,[Status]
		   ,Comment
			,AssignedTo 
			,AccessCode
			,Referrer)
     VALUES
           (@FirstName
		   ,@LastName
		   ,@BookingType
		   ,@Latitude
		   ,@Longitude
		   ,@Location
           ,@ContactNumber
           ,@EmailAddress
           ,@ReasonforenquiryId
           ,@BranchId
           ,@BookingDateandTime
           ,GETDATE()
           ,@UserId
		   ,GETDATE()
           ,@UserId
           ,@Status
		   ,@Comment
		   ,@AssignedTo
		   ,@AccessCode
		   ,@Referrer)
		   SET @Id = SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [dbo].[Branch_Archive_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Branch_Archive_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Branch_Archive_ById] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Branch_Archive_ById] 
	 @Id INT
	
AS

		Update Branch 
		set Status = 0
		
		WHERE	Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[Branch_DeleteById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Branch_DeleteById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Branch_DeleteById] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Branch_DeleteById] 
	 @Id INT
	
AS

		update	Branch
		set status = 2
		
		WHERE	Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[Branch_HeadBranch_Capacity]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Branch_HeadBranch_Capacity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Branch_HeadBranch_Capacity] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Branch_HeadBranch_Capacity] 
	
AS

		Select * from Branch Where Id = 3

GO
/****** Object:  StoredProcedure [dbo].[Branch_SelectById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Branch_SelectById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Branch_SelectById] AS' 
END
GO
ALTER PROCEDURE [dbo].[Branch_SelectById]

@Id int

AS

Select * from Branch Where Id = CASE WHEN @Id IS NOT NULL THEN @Id ELSE Id END

GO
/****** Object:  StoredProcedure [dbo].[Branch_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Branch_Specific_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Branch_Specific_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Branch_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,		
		@status int = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'status')
BEGIN
	SET @status =(Select Value FROM  @ItemsTable WHERE [KEY] = 'status')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select * from Branch where 1=1 
and 
1 = Case When @status is null and Status = 1  then 1
when @status is not null and  Status = @status then 1 end
and Id <> 3

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[Branch_UnArchive_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Branch_UnArchive_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Branch_UnArchive_ById] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Branch_UnArchive_ById] 
	 @Id INT
	
AS

		Update Branch 
		set Status = 1
		
		WHERE	Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[Branch_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Branch_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Branch_UpSert] AS' 
END
GO
ALTER  PROCEDURE [dbo].[Branch_UpSert] 	 
	 @Id INT OUT
	,@Name varchar(max) = null
	,@Address varchar(max) = null
	,@VideoCapacity bigint = null	
	, @Status bigint = null
	, @UserId bigint = null
	,@BookingNotes varchar(max) = null
	,@MondayOpeningHours varchar(max) = null
	,@MondayClosingHours varchar(max) = null
	,@TuesdayOpeningHours varchar(max) = null
	,@TuesdayClosingHours varchar(max) = null
	,@WednesdayOpeningHours varchar(max) = null
	,@WednesdayClosingHours varchar(max) = null
	,@ThursdayOpeningHours varchar(max) = null
	,@ThursdayClosingHours varchar(max) = null
	,@FridayOpeningHours varchar(max) = null
	,@FridayClosingHours varchar(max) = null
	,@SaturdayOpeningHours varchar(max) = null
	,@SaturdayClosingHours varchar(max) = null
	,@SundayOpeningHours varchar(max) = null
	,@SundayClosingHours varchar(max) = null

AS

if(@Id > 0)
BEGIN

UPDATE [dbo].Branch
   SET Name = @Name
   ,Address = @Address
	,VideoCapacity = @VideoCapacity
      ,[ModifiedDate] = GETDATE()
      ,[ModifiedBy] = @UserId
      ,[Status] = @Status
	  ,BookingNotes = @BookingNotes
	  ,MondayOpeningHours = @MondayOpeningHours
	,MondayClosingHours = @MondayClosingHours
	,TuesdayOpeningHours = @TuesdayOpeningHours
	,TuesdayClosingHours = @TuesdayClosingHours
	,WednesdayOpeningHours = @WednesdayOpeningHours
	,WednesdayClosingHours = @WednesdayClosingHours
	,ThursdayOpeningHours = @ThursdayOpeningHours
	,ThursdayClosingHours = @ThursdayClosingHours
	,FridayOpeningHours = @FridayOpeningHours
	,FridayClosingHours = @FridayClosingHours
	,SaturdayOpeningHours = @SaturdayOpeningHours
	,SaturdayClosingHours = @SaturdayClosingHours
	,SundayOpeningHours = @SundayOpeningHours
	,SundayClosingHours = @SundayClosingHours

 WHERE Id = @Id

END

ELSE BEGIN

INSERT INTO Branch
           (Name,
		   VideoCapacity
		   ,Address
           ,[CreatedDate]
           ,[CreatedBy]
		    ,[ModifiedDate] 
      ,[ModifiedBy]
           ,[Status]
		   ,BookingNotes
		   ,MondayOpeningHours 
	,MondayClosingHours 
	,TuesdayOpeningHours 
	,TuesdayClosingHours 
	,WednesdayOpeningHours 
	,WednesdayClosingHours 
	,ThursdayOpeningHours 
	,ThursdayClosingHours 
	,FridayOpeningHours 
	,FridayClosingHours 
	,SaturdayOpeningHours 
	,SaturdayClosingHours 
	,SundayOpeningHours 
	,SundayClosingHours )
     VALUES
           (@Name,
		   @VideoCapacity
		   ,@Address
           ,GETDATE()
           ,@UserId
		   ,GETDATE()
           ,@UserId
           ,@Status,
		   @BookingNotes
		   ,@MondayOpeningHours
		   ,@MondayClosingHours
		   ,@TuesdayOpeningHours
		   ,@TuesdayClosingHours
		   ,@WednesdayOpeningHours
		   ,@WednesdayClosingHours
		   ,@ThursdayOpeningHours
		   ,@ThursdayClosingHours
		   ,@FridayOpeningHours
		   ,@FridayClosingHours
		   ,@SaturdayOpeningHours
		   ,@SaturdayClosingHours
		   ,@SundayOpeningHours
		   ,@SundayClosingHours)
		   SET @Id = SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [dbo].[Reason_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reason_Specific_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Reason_Specific_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Reason_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select * from Reasonforenquiry where Status = 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[Settings_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Settings_Specific_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Settings_Specific_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Settings_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select * from Settings where Status = 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[Settings_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Settings_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Settings_UpSert] AS' 
END
GO
ALTER PROCEDURE [dbo].[Settings_UpSert] 	 
	 @Id INT OUT
	,@SettingKey varchar(max) = null
	,@SettingValue varchar(max) = null
	, @Status bigint = null
	, @UserId bigint = null

AS
SET @Id = (Select Id from Settings Where SettingKey = @SettingKey)

if(@Id > 0)
BEGIN

UPDATE [dbo].Settings
   SET SettingKey = @SettingKey
   ,SettingValue = @SettingValue
      ,[ModifiedDate] = GETDATE()
      ,[ModifiedBy] = @UserId
      ,[Status] = @Status
 WHERE Id = @Id

END

ELSE BEGIN

INSERT INTO Settings
           (SettingKey,
		   SettingValue
           ,[CreatedDate]
           ,[CreatedBy]
		   ,ModifiedDate
		   ,ModifiedBy
           ,[Status])
     VALUES
           (@SettingKey,
		   @SettingValue		   
           ,GETDATE()
           ,@UserId
		   ,GETDATE()
           ,@UserId
           ,@Status)
		   SET @Id = SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TableFilteredRow]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[TableFilteredRow] AS' 
END
GO
ALTER PROCEDURE [dbo].[TableFilteredRow]
	(@TableName VARCHAR(500)
	,@Filters VARCHAR(500) = null
	,@SortExpression VARCHAR(500) = null
	,@Columns VARCHAR(MAX) = null
	,@PageSize INT = null
	,@Page INT = null)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @SqlStatement VARCHAR(MAX);

	IF (@Columns IS NULL OR LEN(@Columns) = 0)
	BEGIN
		SET @Columns = '*';
	END	

	SET @SqlStatement = 'SELECT ' + @Columns + ' FROM [' + LTRIM(@TableName) + '] WHERE 1 = 1';
	IF (@Filters IS NOT NULL AND LEN(@Filters) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' AND ' + @Filters;
	END

	IF (@SortExpression IS NOT NULL AND LEN(@SortExpression) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' ORDER BY ' + @SortExpression;
	END

	EXEC(@SqlStatement);
END

GO
/****** Object:  StoredProcedure [dbo].[User_Delete]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[User_Delete] AS' 
END
GO
ALTER  PROCEDURE [dbo].[User_Delete] 
	 @Id INT
	
AS

		update [User]
		set status = 2
		
		WHERE	UserID = @Id

GO
/****** Object:  StoredProcedure [dbo].[User_DeleteById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_DeleteById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[User_DeleteById] AS' 
END
GO
ALTER  PROCEDURE [dbo].[User_DeleteById] 
	 @Id INT
	
AS

		update [User]
		set status = 2
		
		WHERE	UserID = @Id

GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[User_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[User_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable READONLY
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
		Select *, FirstName + ' ' + LastName As FullName from [User]  u
		inner join UserRole UR on ur.UserRoleId = u.RoleId
where u.Status = 1




SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[User_Select_ById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_Select_ById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[User_Select_ById] AS' 
END
GO
ALTER PROCEDURE [dbo].[User_Select_ById]

@UserId int

AS

Select u.*,ur.Role from [User] u
Left join UserRole ur on u.RoleId = ur.UserRoleId
 Where UserID = CASE WHEN @UserId IS NOT NULL THEN @UserId ELSE UserID END

GO
/****** Object:  StoredProcedure [dbo].[User_SelectById]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_SelectById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[User_SelectById] AS' 
END
GO
ALTER PROCEDURE [dbo].[User_SelectById]

@Id int

AS

Select * from [User] Where UserID = CASE WHEN @Id IS NOT NULL THEN @Id ELSE UserID END

GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[User_UpSert] AS' 
END
GO
ALTER  PROCEDURE [dbo].[User_UpSert] 	 
	 @Id INT OUT
	, @RoleId bigint = null
	, @FirstName varchar(200) = null
	, @LastName varchar(200) = null
	, @email varchar(200) = null
	, @Password varchar(200) = null
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	,@ContactNumber varchar(max) = null
	
	
	



AS

if(@Id > 0)
BEGIN

	update [User] set FirstName = @FirstName,
	LastName = @LastName,
	Email = @email,
	RoleId = @RoleId,
	ContactNumber = @ContactNumber,
	Password = @Password,
	ModifiedDate = GETDATE(),
	ModifiedBy = @ModifiedBy
	where UserID = @Id	

END

ELSE BEGIN

		INSERT INTO [dbo].[User]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[RoleId]
		   ,ContactNumber
		   ,Status)
     VALUES
           (@FirstName
           ,@LastName
           ,@email
           ,@Password
           ,GETDATE()
           ,@CreatedBy
           ,@RoleId
		   ,@ContactNumber
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UserAuthToken_Delete] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UserAuthToken_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	UserAuthToken
		
		WHERE	UserAuthTokenID = @Id

GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_Logout]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UserAuthToken_Logout] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UserAuthToken_Logout] 
    @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		UPDATE 	UserAuthToken

		SET		ExpiryDate = GETDATE()
						
		WHERE	TokenKey = @TokenKey

GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_Select]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UserAuthToken_Select] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UserAuthToken_Select] 
	 @Email NVARCHAR(100) = NULL
   , @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		SELECT 	UserAuthTokenID
				, UserAuthToken.UserId
				, TokenKey
				, LoginDate	
				, ExpiryDate

				, FirstName
				, LastName
				, Email
				, [Password]
			
		FROM	UserAuthToken
				INNER JOIN [USER] ON UserAuthToken.UserId = [USER].UserID				
		
		WHERE	Email = CASE WHEN @Email IS NOT NULL OR @Email <> '' THEN @Email ELSE Email END
				AND TokenKey = CASE WHEN @TokenKey IS NOT NULL OR @TokenKey <> '' THEN @TokenKey ELSE TokenKey END
				AND ExpiryDate > GETDATE()

GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthToken_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UserAuthToken_UpSert] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UserAuthToken_UpSert] 
	  @Id INT OUT
	, @UserId INT
	, @TokenKey UNIQUEIDENTIFIER
	, @LoginDate NVARCHAR(100)
	, @ExpiryDate DATETIME
	, @ExpiryHours INT
	, @Status INT 

AS

		IF EXISTS (SELECT * FROM UserAuthToken WHERE UserId = @UserId AND ExpiryDate > Getdate()) 
			BEGIN

				UPDATE	[UserAuthToken] 

				SET		@Id = UserAuthTokenID
						, ExpiryDate = DATEADD(hh, @ExpiryHours, getdate())

				WHERE UserId = @UserId AND ExpiryDate > GETDATE()

			END

		ELSE
			BEGIN

				INSERT INTO [UserAuthToken]  (UserId,  TokenKey,  LoginDate, ExpiryDate)
							Values	(@UserId, @TokenKey, @LoginDate, DATEADD(hh, @ExpiryHours, getdate()))

				SET @Id = SCOPE_IDENTITY()

			END

GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRole_Specific_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UserRole_Specific_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[UserRole_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	Select * from UserRole 
	Where 
	1=1 and 1= 
	Case When ISNULL(@searchText,0) = 0 Then 1
	When ISNULL(@searchText,0)<> 0 and	Role like '%'+@searchText+'%' Then 1
	End	
	



SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users_List_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Users_List_Search] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Users_List_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly 
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 2/5/2019 10:15:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersRegion_UpSert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UsersRegion_UpSert] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UsersRegion_UpSert] 	 
	 @Id INT OUT
	, @RegionId bigint = null
	, @UserId bigint = null	
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	
	
	

AS


--- delete all then insert

Delete UsersRegion where UserId = @UserId


		INSERT INTO [dbo].[UsersRegion]
           (
		   RegionId
		   ,UserId
           ,[CreatedDate]
           ,[CreatedBy]          
		   ,Status)
     VALUES
           (@RegionId
           ,@UserId                      
           ,GETDATE()
           ,@CreatedBy           
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()

GO
