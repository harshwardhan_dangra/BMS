DECLARE @sql NVARCHAR(MAX);
SET @sql = N'';

SELECT @sql = @sql + N'
  ALTER TABLE ' + QUOTENAME(s.name) + N'.'
  + QUOTENAME(t.name) + N' DROP CONSTRAINT '
  + QUOTENAME(c.name) + ';'
FROM sys.objects AS c
INNER JOIN sys.tables AS t
ON c.parent_object_id = t.[object_id]
INNER JOIN sys.schemas AS s 
ON t.[schema_id] = s.[schema_id]
WHERE c.[type] IN ('D','C','F','PK','UQ')
ORDER BY c.[type];

--PRINT @sql;
EXEC sys.sp_executesql @sql;

Alter Table Booking
Alter Column Status int 

Alter Table Branch
Alter Column Status int 

Alter Table Appointment
Alter Column Status int 

Alter Table Settings
Alter Column Status int 

Alter Table [User]
Alter Column Status int 

Alter Table UserRole
Alter Column Status int 


Alter Table Reasonforenquiry
Alter Column Status int 
 

Alter Table AccessRight
Alter Column Status int 