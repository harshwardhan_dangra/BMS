USE [NMoneyBMS]
GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[UsersRegion_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Users_List_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[UserRole_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[UserAuthToken_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[UserAuthToken_Select]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[UserAuthToken_Logout]
GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[UserAuthToken_Delete]
GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[User_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[User_SelectById]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[User_SelectById]
GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[User_Search]
GO
/****** Object:  StoredProcedure [dbo].[User_DeleteById]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[User_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[TableFilteredRow]
GO
/****** Object:  StoredProcedure [dbo].[Settings_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Settings_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Reason_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Reason_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Branch_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Branch_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Branch_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Branch_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Branch_SelectById]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Branch_SelectById]
GO
/****** Object:  StoredProcedure [dbo].[Branch_DeleteById]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Branch_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Booking_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Booking_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Booking_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Booking_SelectById]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Booking_SelectById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_Search_ById]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Booking_Search_ById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_DeleteById]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Booking_DeleteById]
GO
/****** Object:  StoredProcedure [dbo].[Booking_Archive_ById]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Booking_Archive_ById]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Appointment_UpSert]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_TimeSlots_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Appointment_TimeSlots_Search]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Appointment_Specific_Search]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_DeleteByDate]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Appointment_DeleteByDate]
GO
/****** Object:  StoredProcedure [dbo].[Appointment_AllTimeSlots_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[Appointment_AllTimeSlots_Search]
GO
/****** Object:  StoredProcedure [dbo].[AllBranchTotalCapacity]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[AllBranchTotalCapacity]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 9/10/2018 4:01:30 AM ******/
DROP PROCEDURE [dbo].[AccessRight_Select]
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[AccessRight_Select] 
  @userId INT
 
AS

  SELECT [AccessRightsId]
     ,[UserId]
     ,[EntityType]
     ,[KorrectAccessRight]
     ,[SecurityPrincipalType]
     ,[AllowedRights]
     ,[DeniedRights]
    FROM [AccessRight]
    --INNER JOIN [USER] ON [AccessRight].UserId = [USER].UserID
  
  WHERE UserId = @userId


GO
/****** Object:  StoredProcedure [dbo].[AllBranchTotalCapacity]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AllBranchTotalCapacity]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@appointmentType bigint = null,
		@AppointmentDate date = null,
		@AppointmentTime time = null
	

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentType')
BEGIN
	SET @appointmentType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentType')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentDate')
BEGIN
	SET @AppointmentDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentDate')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentTime')
BEGIN
	SET @AppointmentTime =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentTime')
END


IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

select distinct 
--Sum(1) OVER() as TotalBooked, 
--(Select VideoCapacity from Branch Where Id = 3) as HeadBranchCapacity,
Case  When Sum(1) OVER() <= (Select VideoCapacity from Branch Where Id = 3) then 1
When Sum(1) OVER() >= (Select VideoCapacity from Branch Where Id = 3) then 0 
end as AppointmentAvailable
 from Appointment where AppointmentDate = @AppointmentDate
 and AppointmentTime = @AppointmentTime
 
and AppointmentType
=@appointmentType

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Appointment_AllTimeSlots_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Appointment_AllTimeSlots_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@appointmentType bigint = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentType')
BEGIN
	SET @appointmentType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentType')
END



IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select *, ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) as SortOrder from (
Select distinct AppointmentTime, TotalCapacity, Status, CONVERT(CHAR(5), AppointmentTime, 108) as AppointmentTimeString
 from Appointment a Where 1=1 
 and 1=
 Case When ISNULL(@appointmentType,0) = 0 then 1
 When ISNULL(@appointmentType,0) <> 0 and AppointmentType = @appointmentType then 1
 end
order by AppointmentTime asc OFFSET 0 ROWS
   )
   appointment

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Appointment_DeleteByDate]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[Appointment_DeleteByDate] 	 
	@AppointmentDate datetime = null
	,@AppointmentType bigint = null
	,@BranchId bigint = null	
AS
Delete from Appointment where AppointmentDate = CONVERT(DATE, CONVERT(CHAR(8), @AppointmentDate, 112)) and AppointmentType = @AppointmentType
and BranchId = @BranchId
GO
/****** Object:  StoredProcedure [dbo].[Appointment_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Appointment_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@appointmentType bigint = null,
		@branchId bigint = null,
		@currentDateTime datetime = null,
		@selectedDate datetime = null,
		@selectedTime	datetime = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentType')
BEGIN
	SET @appointmentType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentType')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'branchId')
BEGIN
	SET @branchId =(Select Value FROM  @ItemsTable WHERE [KEY] = 'branchId')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'currentDateTime')
BEGIN
	SET @currentDateTime =(Select Value FROM  @ItemsTable WHERE [KEY] = 'currentDateTime')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedDate')
BEGIN
	SET @selectedDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedDate')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedTime')
BEGIN
	SET @selectedTime =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedTime')
END


IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select *,
AppointmentAvailable = (Case When TotalCapacity <= BookedAppointment then 0
						Else 1 end)
 from (
Select *
,(Select Name from Branch  where Id = a.BranchId) as Branch
, BookedAppointment = (Select count(*) from Booking where convert(varchar, BookingDateandTime, 23) = a.AppointmentDate
 and convert(varchar, BookingDateandTime, 14) = a.AppointmentTime)
 from Appointment a Where 1=1 
 and 1=
 Case When ISNULL(@appointmentType,0) = 0 then 1
 When ISNULL(@appointmentType,0) <> 0 and AppointmentType = @appointmentType then 1
 end
 and 
 1= Case When ISNULL(@branchId,0) = 0 then 1
 When ISNULL(@branchId,0) <> 0 and BranchId = @branchId then 1
 end
 and 1= Case When ISNULL(@currentDateTime,0) = 0 then 1
 When ISNULL(@currentDateTime,0) <> 0  and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112) 
  + ' ' + CONVERT(CHAR(8), AppointmentTime, 108)) >= @currentDateTime  
 Then 1
   end
   and 1= Case When ISNULL(@selectedDate,0) = 0 then 1
 When ISNULL(@selectedDate,0) <> 0  and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112)) = @selectedDate  
 Then 1
   end
     and 1= Case When ISNULL(@selectedTime,0) = 0 then 1
 When ISNULL(@selectedTime,0) <> 0  and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentTime, 108)) = @selectedTime  
 Then 1
   end
 ) appointment order by AppointmentDate

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Appointment_TimeSlots_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Appointment_TimeSlots_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@appointmentType bigint = null,
		@branchId bigint = null,
		@currentDateTime datetime = null,
		@selectedStartDate datetime = null,
		@selectedEndDate	datetime = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'appointmentType')
BEGIN
	SET @appointmentType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'appointmentType')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'branchId')
BEGIN
	SET @branchId =(Select Value FROM  @ItemsTable WHERE [KEY] = 'branchId')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedStartDate')
BEGIN
	SET @selectedStartDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedStartDate')
END
IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'selectedEndDate')
BEGIN
	SET @selectedEndDate =(Select Value FROM  @ItemsTable WHERE [KEY] = 'selectedEndDate')
END


IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select *, ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) as SortOrder from (
Select distinct AppointmentTime, TotalCapacity, Status, CONVERT(CHAR(5), AppointmentTime, 108) as AppointmentTimeString
 from Appointment a Where 1=1 
 and 1=
 Case When ISNULL(@appointmentType,0) = 0 then 1
 When ISNULL(@appointmentType,0) <> 0 and AppointmentType = @appointmentType then 1
 end
 and 
 1= Case When ISNULL(@branchId,0) = 0 then 1
 When ISNULL(@branchId,0) <> 0 and BranchId = @branchId then 1
 end
   and 1= Case When ISNULL(@selectedStartDate,0) = 0 then 1
 When ISNULL(@selectedStartDate,0) <> 0 and ISNULL(@selectedEndDate,0) <> 0   and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112)) >= @selectedStartDate  
 Then 1
 When ISNULL(@selectedStartDate,0) <> 0   and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112)) = @selectedStartDate  
 Then 1
   end

      and 1= Case When ISNULL(@selectedEndDate,0) = 0 then 1
 When ISNULL(@selectedEndDate,0) <> 0  and CONVERT(DATETIME, CONVERT(CHAR(8), AppointmentDate, 112)) <= @selectedEndDate  
 Then 1
   end 
order by AppointmentTime asc OFFSET 0 ROWS
   )
   appointment

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Appointment_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[Appointment_UpSert] 	 
	 @Id INT OUT
	,@AppointmentDate datetime = null
	,@AppointmentTime datetime = null
	,@AppointmentDay varchar(max) = null
	,@AppointmentType bigint = null
	,@OpeningTimesStart datetime = null
	,@OpeningTimesEnd datetime = null
	,@TotalCapacity bigint = null
	,@BranchId bigint = null
	, @Status bigint = null
	, @UserId bigint = null

AS

--if(@Id > 0)
--BEGIN

--UPDATE [dbo].Appointment
--   SET AppointmentDate = @AppointmentDate
--		,AppointmentTime = @AppointmentTime
--		,AppointmentType = @AppointmentType
--		,AppointmentDay = @AppointmentDay
--		,OpeningTimesStart = @OpeningTimesStart
--		,OpeningTimesEnd = @OpeningTimesEnd
--		,TotalCapacity = @TotalCapacity
--      ,BranchId = @BranchId
--      ,[ModifiedDate] = GETDATE()
--      ,[ModifiedBy] = @UserId
--      ,[Status] = @Status
-- WHERE Id = @Id

--END

--ELSE BEGIN
--Select CONVERT(DATE, CONVERT(CHAR(8), @AppointmentDate, 112)) 
INSERT INTO Appointment
           (AppointmentDate
		   ,AppointmentTime
		   ,AppointmentDay
		   ,AppointmentType
		   ,OpeningTimesStart
		   ,OpeningTimesEnd
           ,TotalCapacity
           ,BranchId         
           ,[CreatedDate]
           ,[CreatedBy]
		   ,ModifiedDate
		   ,ModifiedBy
           ,[Status])
     VALUES
           (@AppointmentDate
		   ,@AppointmentTime
		   ,@AppointmentDay
		   ,@AppointmentType
		   ,@OpeningTimesStart
		   ,@OpeningTimesEnd
           ,@TotalCapacity
           ,@BranchId
           ,GETDATE()
           ,@UserId
		   ,GETDATE()
           ,@UserId
           ,@Status)
		   SET @Id = SCOPE_IDENTITY()
--END
		   

GO
/****** Object:  StoredProcedure [dbo].[Booking_Archive_ById]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




create  PROCEDURE [dbo].[Booking_Archive_ById] 
	 @Id INT
	
AS

		Update Booking 
		set Status = 0
		
		WHERE	Id = @Id



GO
/****** Object:  StoredProcedure [dbo].[Booking_DeleteById]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[Booking_DeleteById] 
	 @Id INT
	
AS

		DELETE
		
		FROM	Booking
		
		WHERE	Id = @Id



GO
/****** Object:  StoredProcedure [dbo].[Booking_Search_ById]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Booking_Search_ById]

@Id int

AS

Select distinct bo.*,br.Name as Branch, roe.Name as Reasonforenquiry from Booking bo
left join Branch br on bo.BranchId = br.Id
left join Reasonforenquiry roe on bo.ReasonforenquiryId = roe.Id Where bo.Id = CASE WHEN @Id IS NOT NULL THEN @Id ELSE bo.Id END 

GO
/****** Object:  StoredProcedure [dbo].[Booking_SelectById]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Booking_SelectById]

@Id int

AS

Select * from Booking Where Id = CASE WHEN @Id IS NOT NULL THEN @Id ELSE Id END 

GO
/****** Object:  StoredProcedure [dbo].[Booking_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Booking_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@bookingType int = null,
		@status int = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'bookingType')
BEGIN
	SET @bookingType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'bookingType')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'status')
BEGIN
	SET @status =(Select Value FROM  @ItemsTable WHERE [KEY] = 'status')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
Select b.*,br.Name as Branch, re.Name as Reasonforenquiry ,
 Case When @status is null then 1
when @status is not null and  b.Status = @status then 1 end
 from Booking b left join Branch br on b.BranchId = br.Id
left join Reasonforenquiry re on b.ReasonforenquiryId = re.Id where 1=1
and 
1 = Case When @status is null then 1
when @status is not null and  b.Status = @status then 1 end
and 1= Case When ISNULL(@bookingType,0) = 0 then 1
When ISNULL(@bookingType,0)<> 0 and b.BookingType = @bookingType then 1 end

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Booking_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[Booking_UpSert] 	 
	 @Id INT OUT
	,@FirstName varchar(max) = null
	,@LastName varchar(max) = null
	,@Latitude varchar(max) = null
	,@BookingType bigint = null
	,@Longitude varchar(max) = null
	,@Location varchar(max)  = null
	,@ContactNumber varchar(max) = null
	,@EmailAddress varchar(max) = null
	,@ReasonforenquiryId bigint = null
	,@BranchId bigint = null
	,@BookingDateandTime datetime = null
	, @Status bigint = null
	, @UserId bigint = null
	,@Comment varchar(max) = null
	,@AccessCode varchar(max) = null
	,@AssignedTo bigint = null

AS

if(@Id > 0)
BEGIN

UPDATE [dbo].[Booking]
   SET FirstName = @FirstName
		,LastName = @LastName
		,Latitude = @Latitude
		,Longitude = @Longitude
		,location = @Location
		,BookingType = @BookingType
      ,[ContactNumber] = @ContactNumber
      ,[EmailAddress] = @EmailAddress
      ,[ReasonforenquiryId] = @ReasonforenquiryId
      ,[BranchId] = @BranchId
      ,[BookingDateandTime] = @BookingDateandTime
      ,[ModifiedDate] = GETDATE()
      ,[ModifiedBy] = @UserId
      ,[Status] = @Status
	  ,Comment = @Comment
	  ,AssignedTo = @AssignedTo
	  ,AccessCode = @AccessCode
 WHERE Id = @Id

END

ELSE BEGIN

INSERT INTO [Booking]
           (FirstName
		   ,LastName
		   ,BookingType
		   ,Latitude
		   ,Longitude
		   ,Location
           ,[ContactNumber]
           ,[EmailAddress]
           ,[ReasonforenquiryId]
           ,[BranchId]
           ,[BookingDateandTime]
           ,[CreatedDate]
           ,[CreatedBy]
		   ,ModifiedDate
		   ,ModifiedBy
           ,[Status]
		   ,Comment
			,AssignedTo 
			,AccessCode)
     VALUES
           (@FirstName
		   ,@LastName
		   ,@BookingType
		   ,@Latitude
		   ,@Longitude
		   ,@Location
           ,@ContactNumber
           ,@EmailAddress
           ,@ReasonforenquiryId
           ,@BranchId
           ,@BookingDateandTime
           ,GETDATE()
           ,@UserId
		   ,GETDATE()
           ,@UserId
           ,@Status
		   ,@Comment
		   ,@AssignedTo
		   ,@AccessCode)
		   SET @Id = SCOPE_IDENTITY()
END
		   

GO
/****** Object:  StoredProcedure [dbo].[Branch_DeleteById]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[Branch_DeleteById] 
	 @Id INT
	
AS

		DELETE
		
		FROM	Branch
		
		WHERE	Id = @Id



GO
/****** Object:  StoredProcedure [dbo].[Branch_SelectById]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Branch_SelectById]

@Id int

AS

Select * from Branch Where Id = CASE WHEN @Id IS NOT NULL THEN @Id ELSE Id END 

GO
/****** Object:  StoredProcedure [dbo].[Branch_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Branch_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select * from Branch where Status = 1 and Id <> 3

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Branch_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[Branch_UpSert] 	 
	 @Id INT OUT
	,@Name varchar(max) = null
	,@Address varchar(max) = null
	,@VideoCapacity bigint = null	
	, @Status bigint = null
	, @UserId bigint = null

AS

if(@Id > 0)
BEGIN

UPDATE [dbo].Branch
   SET Name = @Name
   ,Address = @Address
	,VideoCapacity = @VideoCapacity
      ,[ModifiedDate] = GETDATE()
      ,[ModifiedBy] = @UserId
      ,[Status] = @Status
 WHERE Id = @Id

END

ELSE BEGIN

INSERT INTO Branch
           (Name,
		   VideoCapacity
		   ,Address
           ,[CreatedDate]
           ,[CreatedBy]
           ,[Status])
     VALUES
           (@Name,
		   @VideoCapacity
		   ,@Address
           ,GETDATE()
           ,@UserId
           ,@Status)
		   SET @Id = SCOPE_IDENTITY()
END
		   

GO
/****** Object:  StoredProcedure [dbo].[Reason_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Reason_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select * from Reasonforenquiry where Status = 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Settings_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[Settings_UpSert] 	 
	 @Id INT OUT
	,@SettingKey varchar(max) = null
	,@SettingValue varchar(max) = null
	, @Status bigint = null
	, @UserId bigint = null

AS

if(@Id > 0)
BEGIN

UPDATE [dbo].Settings
   SET SettingKey = @SettingKey
   ,SettingValue = @SettingValue
      ,[ModifiedDate] = GETDATE()
      ,[ModifiedBy] = @UserId
      ,[Status] = @Status
 WHERE Id = @Id

END

ELSE BEGIN

INSERT INTO Settings
           (SettingKey,
		   SettingValue
           ,[CreatedDate]
           ,[CreatedBy]
		   ,ModifiedDate
		   ,ModifiedBy
           ,[Status])
     VALUES
           (@SettingKey,
		   @SettingValue		   
           ,GETDATE()
           ,@UserId
		   ,GETDATE()
           ,@UserId
           ,@Status)
		   SET @Id = SCOPE_IDENTITY()
END
		   

GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TableFilteredRow]
	(@TableName VARCHAR(500)
	,@Filters VARCHAR(500) = null
	,@SortExpression VARCHAR(500) = null
	,@Columns VARCHAR(MAX) = null
	,@PageSize INT = null
	,@Page INT = null)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @SqlStatement VARCHAR(MAX);

	IF (@Columns IS NULL OR LEN(@Columns) = 0)
	BEGIN
		SET @Columns = '*';
	END	

	SET @SqlStatement = 'SELECT ' + @Columns + ' FROM [' + LTRIM(@TableName) + '] WHERE 1 = 1';
	IF (@Filters IS NOT NULL AND LEN(@Filters) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' AND ' + @Filters;
	END

	IF (@SortExpression IS NOT NULL AND LEN(@SortExpression) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' ORDER BY ' + @SortExpression;
	END

	EXEC(@SqlStatement);
END

GO
/****** Object:  StoredProcedure [dbo].[User_DeleteById]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATe  PROCEDURE [dbo].[User_DeleteById] 
	 @Id INT
	
AS

		DELETE
		
		FROM	[User]
		
		WHERE	UserID = @Id



GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable READONLY
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
		Select *, FirstName + ' ' + LastName As FullName from [User]  u
		inner join UserRole UR on ur.UserRoleId = u.RoleId
where u.Status <> 0 




SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[User_SelectById]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[User_SelectById]

@Id int

AS

Select * from [User] Where UserID = CASE WHEN @Id IS NOT NULL THEN @Id ELSE UserID END 

GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[User_UpSert] 	 
	 @Id INT OUT
	, @RoleId bigint = null
	, @FirstName varchar(200) = null
	, @LastName varchar(200) = null
	, @email varchar(200) = null
	, @Password varchar(200) = null
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	
	
	



AS

if(@Id > 0)
BEGIN

	update [User] set FirstName = @FirstName,
	LastName = @LastName,
	Email = @email,
	RoleId = @RoleId
	where UserID = @Id	

END

ELSE BEGIN

		INSERT INTO [dbo].[User]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[RoleId]
		   ,Status)
     VALUES
           (@FirstName
           ,@LastName
           ,@email
           ,@Password
           ,GETDATE()
           ,@CreatedBy
           ,@RoleId
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()
END
		   

GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	UserAuthToken
		
		WHERE	UserAuthTokenID = @Id



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Logout] 
    @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		UPDATE 	UserAuthToken

		SET		ExpiryDate = GETDATE()
						
		WHERE	TokenKey = @TokenKey 



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[UserAuthToken_Select] 
	 @Email NVARCHAR(100) = NULL
   , @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		SELECT 	UserAuthTokenID
				, UserAuthToken.UserId
				, TokenKey
				, LoginDate	
				, ExpiryDate

				, FirstName
				, LastName
				, Email
				, [Password]
			
		FROM	UserAuthToken
				INNER JOIN [USER] ON UserAuthToken.UserId = [USER].UserID				
		
		WHERE	Email = CASE WHEN @Email IS NOT NULL OR @Email <> '' THEN @Email ELSE Email END
				AND TokenKey = CASE WHEN @TokenKey IS NOT NULL OR @TokenKey <> '' THEN @TokenKey ELSE TokenKey END
				AND ExpiryDate > GETDATE()


GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_UpSert] 
	  @Id INT OUT
	, @UserId INT
	, @TokenKey UNIQUEIDENTIFIER
	, @LoginDate NVARCHAR(100)
	, @ExpiryDate DATETIME
	, @ExpiryHours INT
	, @Status INT 

AS

		IF EXISTS (SELECT * FROM UserAuthToken WHERE UserId = @UserId AND ExpiryDate > Getdate()) 
			BEGIN

				UPDATE	[UserAuthToken] 

				SET		@Id = UserAuthTokenID
						, ExpiryDate = DATEADD(hh, @ExpiryHours, getdate())

				WHERE UserId = @UserId AND ExpiryDate > GETDATE()

			END

		ELSE
			BEGIN

				INSERT INTO [UserAuthToken]  (UserId,  TokenKey,  LoginDate, ExpiryDate)
							Values	(@UserId, @TokenKey, @LoginDate, DATEADD(hh, @ExpiryHours, getdate()))

				SET @Id = SCOPE_IDENTITY()

			END


GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	Select * from UserRole Where Role like '%'+@searchText+'%' and UserRoleId <> 1
	



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Users_List_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly 
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 9/10/2018 4:01:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[UsersRegion_UpSert] 	 
	 @Id INT OUT
	, @RegionId bigint = null
	, @UserId bigint = null	
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	
	
	

AS


--- delete all then insert

Delete UsersRegion where UserId = @UserId


		INSERT INTO [dbo].[UsersRegion]
           (
		   RegionId
		   ,UserId
           ,[CreatedDate]
           ,[CreatedBy]          
		   ,Status)
     VALUES
           (@RegionId
           ,@UserId                      
           ,GETDATE()
           ,@CreatedBy           
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()

GO
