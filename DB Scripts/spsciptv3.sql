USE [NMoneyBMS]
GO
/****** Object:  StoredProcedure [dbo].[Booking_Specific_Search]    Script Date: 9/1/2018 2:35:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Booking_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null,
		@bookingType int = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'bookingType')
BEGIN
	SET @bookingType =(Select Value FROM  @ItemsTable WHERE [KEY] = 'bookingType')
END

IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

Select b.*,br.Name as Branch, re.Name as Reasonforenquiry  from Booking b left join Branch br on b.BranchId = br.Id
left join Reasonforenquiry re on b.ReasonforenquiryId = re.Id where b.Status = 1
and 1= Case When ISNULL(@bookingType,0) = 0 then 1
When ISNULL(@bookingType,0)<> 0 and b.BookingType = @bookingType then 1 end

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


