USE [NMoneyBMS]
GO
SET IDENTITY_INSERT [dbo].[AccessRight] ON 

INSERT [dbo].[AccessRight] ([AccessRightsId], [UserId], [EntityType], [KorrectAccessRight], [SecurityPrincipalType], [AllowedRights], [DeniedRights], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [status]) VALUES (1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[AccessRight] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId]) VALUES (1, N'dev', N'dev', N'dev@dev.com', N'QEbdqkVOX0yjRxKbJ4KFYW9MDUhj58JRbhDaF5fcXA171k2GHCvbqAvMKWp0KVCuWwMVK0DCizoHC9mwJ8/5uVhflPdDq+g=', NULL, NULL, NULL, NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[UserAuthToken] ON 

INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1, 1, N'0b9cfbcf-66e6-446d-94e5-1fe7c002808f', CAST(N'2018-08-25T12:13:00.000' AS DateTime), CAST(N'2018-08-25T18:13:19.513' AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserAuthToken] OFF
SET IDENTITY_INSERT [dbo].[UserRole] ON 

INSERT [dbo].[UserRole] ([UserRoleId], [Role], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Admin', 1, 1, CAST(N'2018-08-23T16:38:42.617' AS DateTime), 1, CAST(N'2018-08-23T16:38:42.617' AS DateTime))
SET IDENTITY_INSERT [dbo].[UserRole] OFF
