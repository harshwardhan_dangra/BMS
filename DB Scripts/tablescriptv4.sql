USE [NMoneyBMS]
GO
/****** Object:  Table [dbo].[Appointment]    Script Date: 9/2/2018 1:17:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Appointment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentDate] [date] NULL,
	[AppointmentTime] [time](7) NULL,
	[AppointmentDay] [varchar](max) NULL,
	[AppointmentType] [bigint] NULL,
	[OpeningTimesStart] [time](7) NULL,
	[OpeningTimesEnd] [time](7) NULL,
	[TotalCapacity] [int] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_Appointment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Appointment] ON 
GO
INSERT [dbo].[Appointment] ([Id], [AppointmentDate], [AppointmentTime], [AppointmentDay], [AppointmentType], [OpeningTimesStart], [OpeningTimesEnd], [TotalCapacity], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsActive], [BranchId]) 
VALUES (1, CAST(N'2018-09-02' AS Date), CAST(N'09:00:00' AS Time), N'Sunday', 1, NULL, NULL, 5, 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, 1)

INSERT [dbo].[Appointment] ([Id], [AppointmentDate], [AppointmentTime], [AppointmentDay], [AppointmentType], [OpeningTimesStart], [OpeningTimesEnd], [TotalCapacity], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsActive], [BranchId]) 
VALUES (2, CAST(N'2018-09-02' AS Date), CAST(N'10:00:00' AS Time), N'Monday', 1, NULL, NULL, 5, 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, 1)

INSERT [dbo].[Appointment] ([Id], [AppointmentDate], [AppointmentTime], [AppointmentDay], [AppointmentType], [OpeningTimesStart], [OpeningTimesEnd], [TotalCapacity], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsActive], [BranchId]) 
VALUES (3, CAST(N'2018-09-02' AS Date), CAST(N'11:00:00' AS Time), N'Tuesday', 1, NULL, NULL, 5, 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, 1)

INSERT [dbo].[Appointment] ([Id], [AppointmentDate], [AppointmentTime], [AppointmentDay], [AppointmentType], [OpeningTimesStart], [OpeningTimesEnd], [TotalCapacity], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsActive], [BranchId]) 
VALUES (4, CAST(N'2018-09-02' AS Date), CAST(N'12:00:00' AS Time), N'Wednesday', 1, NULL, NULL, 5, 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, 1)

INSERT [dbo].[Appointment] ([Id], [AppointmentDate], [AppointmentTime], [AppointmentDay], [AppointmentType], [OpeningTimesStart], [OpeningTimesEnd], [TotalCapacity], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsActive], [BranchId]) 
VALUES (5, CAST(N'2018-09-02' AS Date), CAST(N'13:00:00' AS Time), N'Thrusday', 1, NULL, NULL, 5, 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, 1)

INSERT [dbo].[Appointment] ([Id], [AppointmentDate], [AppointmentTime], [AppointmentDay], [AppointmentType], [OpeningTimesStart], [OpeningTimesEnd], [TotalCapacity], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsActive], [BranchId]) 
VALUES (6, CAST(N'2018-09-02' AS Date), CAST(N'14:00:00' AS Time), N'Friday', 1, NULL, NULL, 5, 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, 1)

INSERT [dbo].[Appointment] ([Id], [AppointmentDate], [AppointmentTime], [AppointmentDay], [AppointmentType], [OpeningTimesStart], [OpeningTimesEnd], [TotalCapacity], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsActive], [BranchId]) 
VALUES (7, CAST(N'2018-09-02' AS Date), CAST(N'15:00:00' AS Time), N'Saturday', 1, NULL, NULL, 5, 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, 1)

INSERT [dbo].[Appointment] ([Id], [AppointmentDate], [AppointmentTime], [AppointmentDay], [AppointmentType], [OpeningTimesStart], [OpeningTimesEnd], [TotalCapacity], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsActive], [BranchId]) 
VALUES (8, CAST(N'2018-09-02' AS Date), CAST(N'16:00:00' AS Time), N'Sunday', 1, NULL, NULL, 5, 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, CAST(N'2018-09-02T00:00:00.000' AS DateTime), 1, 1)

GO
SET IDENTITY_INSERT [dbo].[Appointment] OFF
GO


USE [NMoneyBMS]
GO

/****** Object:  Table [dbo].[Settings]    Script Date: 9/2/2018 7:52:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Settings](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SettingKey] [varchar](max) NULL,
	[SettingValue] [varchar](max) NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

