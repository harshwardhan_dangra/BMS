Create Table Booking
(
Id bigint identity(1,1) not null,
Name varchar(max) null,
ContactNumber varchar(max) null,
EmailAddress varchar(max) null,
ReasonforenquiryId bigint,
BranchId bigint,
BookingDateandTime datetime null,
CreatedDate datetime,
CreatedBy bigint,
ModifiedDate datetime,
ModifiedBy bigint,
Status bit default 0
)

Create Table Reasonforenquiry(
Id bigint identity(1,1) not null,
Name varchar(max) null,
CreatedDate datetime,
CreatedBy bigint,
ModifiedDate datetime,
ModifiedBy bigint,
Status bit default 0
)

Create Table Branch(
Id bigint identity(1,1) not null,
Name varchar(max) null,
CreatedDate datetime,
CreatedBy bigint,
ModifiedDate datetime,
ModifiedBy bigint,
Status bit default 0
)