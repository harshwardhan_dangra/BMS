﻿using BMS.Core.Utilities.CustomAttributes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace BMS.Core.Utilities.Extensions
{
    public static class DbCommandExtensions
    {
        public static T FillUpSertParameters<T>(this DbCommand command, Database database, T entity)
            where T : class
        {
            if (database == null)
                throw new ArgumentNullException("database cannot be null");

            if (entity == null)
                throw new ArgumentNullException("entity cannot be null");

            var type = entity.GetType();
            var pkColumnName = Convert.ToString(GetValue(entity, type, "PrimaryKeyColumnName"));
            foreach (var prop in type.GetProperties())
            {
                if (prop.GetCustomAttributes(typeof(IgnoreField), false).Length == 0)
                {
                    var value = GetValue(entity, type, prop.Name);
                    if (prop.Name.ToLowerInvariant() == pkColumnName.ToLowerInvariant())
                    {
                        database.AddInParameter(command, "@Id", DbType.Int64, value);
                        command.Parameters["@Id"].Direction = ParameterDirection.InputOutput;
                    }
                    else if (prop.Name.ToLowerInvariant() != "PrimaryKeyColumnName".ToLowerInvariant())
                    {
                        var dbType = GetDbType(value);
                        if (dbType.HasValue &&
                            (prop.Name.ToLowerInvariant() != "CreatedDate".ToLowerInvariant() && prop.Name.ToLowerInvariant() != "ModifiedDate".ToLowerInvariant()))
                            database.AddInParameter(command, "@" + prop.Name, dbType.Value, value);
                    }
                }
            }

            return entity;
        }

        private static object GetValue<T>(T entity, Type type, string propertyName) where T : class
        {
            var prop = type.GetProperty(propertyName);
            return prop.GetValue(entity);
        }

        private static DbType? GetDbType(object value)
        {
            if (value == null)
                return default(DbType?);

            var type = value.GetType();
            if (value is Int16 || value is Int16?)
                return DbType.Int16;
            else if (value is Int32 || value is Int32?)
                return DbType.Int32;
            else if (value is Int64 || value is Int64?)
                return DbType.Int64;
            else if (value is Decimal || value is Decimal? || value is Double || value is Double?)
                return DbType.Double;
            else if (value is Single || value is Single?)
                return DbType.Single;
            else if (value is DateTime || value is DateTime?)
                return DbType.DateTime;
            else if (value is Boolean || value is Boolean?)
                return DbType.Boolean;
            else if (value is Guid || value is Guid?)
                return DbType.Guid;
            else if (value is string)
                return DbType.String;
            else
                return default(DbType?);
        }
    }
}
