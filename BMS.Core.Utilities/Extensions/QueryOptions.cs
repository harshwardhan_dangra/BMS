﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Utilities.Extensions
{
    public class QueryOptions
    {
        public const string FROMDATE_KEY = "fromdate";
        public const string TODATE_KEY = "todate";
        public const string ROWCOUNT_KEY = "rowcount";
        public const string CURRENCY_KEY = "currencyid";

        public const string MODEL_KEY = "model";
        public const string ENTITYTYPEID_KEY = "entitytypeid"; //NEED to confirm with FE
        public const string ENTITYID_KEY = "entityid";
        public const string INTERVAL_KEY = "interval";

        public const string DATEFORMAT_UTC = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'";

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int RowCount { get; set; }
        public int CurrencyId { get; set; }
        public string Model { get; set; }
        public int? EntityTypeId { get; set; }
        public int? EntityId { get; set; }
        public string Interval { get; set; }

    }
}
