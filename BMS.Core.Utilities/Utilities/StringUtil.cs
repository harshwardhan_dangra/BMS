﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BMS.Core.Utilities
{
    public static class StringUtil
    {
        private static Random random = new Random((int)DateTime.Now.Ticks);

        public static string ReverseString(string Text)
        {
            char[] charArray = Text.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static string RandomCharacter(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }

        public static int GetPageNumber(Dictionary<string, string> searchFields)
        {
            int pageNumber = 1;
            if (searchFields != null && searchFields["pageNumber"] != null)
            {
                int.TryParse(searchFields["pageNumber"], out pageNumber);
            }

            return pageNumber;
        }

        public static int GetPageSize(Dictionary<string, string> searchFields)
        {
            int pageSize = 10;
            if (searchFields != null && searchFields["pageSize"] != null)
            {
                int.TryParse(searchFields["pageSize"], out pageSize);
            }

            return pageSize;
        }
    }
}
