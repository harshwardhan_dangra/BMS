﻿using System.Web;

namespace BMS.Core.Utilities
{
    public static class HtmlUtil
    {
        public static string ConvertStringToHtml(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {

                text = text.Replace("\r\n","<br/>");
                text = text.Replace("\n", "<br/>");
            }

            return text;
        }


        public static string DecodeURIComponent(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                text = HttpContext.Current.Server.UrlDecode(text);
            }

            return text;
        }
    }
}
