﻿using System;
using System.Text.RegularExpressions;

namespace BMS.Core.Utilities.Utilities
{
    public  class CoreValidations
    {
        
        public bool IsNumber(String strNumber)
        {
            Regex objNotNumberPattern = new Regex("[^0-9.-]");
            Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
            Regex objTwoMinusPattern = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
            String strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
            String strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
            Regex objNumberPattern = new Regex("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");
            return !objNotNumberPattern.IsMatch(strNumber) &&
            !objTwoDotPattern.IsMatch(strNumber) &&
            !objTwoMinusPattern.IsMatch(strNumber) &&
            objNumberPattern.IsMatch(strNumber);
        }
        // Function to check for Positive Number both Integer & Real 
        public  bool IsPositiveNumber(String strNumber)
        {
            Regex objNotPositivePattern = new Regex("[^0-9.]");
            Regex objPositivePattern = new Regex("^[.][0-9]+$|[0-9]*[.]*[0-9]+$");
            Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
            return !objNotPositivePattern.IsMatch(strNumber) &&
            objPositivePattern.IsMatch(strNumber) &&
            !objTwoDotPattern.IsMatch(strNumber);
        }

        // Function to check for Positive Integers. 
        public bool IsNaturalNumber(String strNumber)
        {
            Regex objNotNaturalPattern = new Regex("[^0-9]");
            Regex objNaturalPattern = new Regex("0*[1-9][0-9]*");
            return !objNotNaturalPattern.IsMatch(strNumber) &&
            objNaturalPattern.IsMatch(strNumber);
        }
        //// Function to check for Positive Integers with zero inclusive 
        //public  bool IsWholeNumber(String strNumber)
        //{
        //    Regex objNotWholePattern = new Regex("[^0-9]");
        //  
        //    return !objNotWholePattern.IsMatch(strNumber);
        //}
        // Function to check for Integers both Positive & Negative 
        public   bool IsInteger(String strNumber)
        {
            Regex objNotIntPattern = new Regex("[^0-9-]");
            Regex objIntPattern = new Regex("^-[0-9]+$|^[0-9]+$");
            return !objNotIntPattern.IsMatch(strNumber) && objIntPattern.IsMatch(strNumber);
        }
        // Function To check for Alphabets. 
        public bool IsAlpha(String strToCheck)
        {
            Regex objAlphaPattern = new Regex("[^a-zA-Z]");
            return !objAlphaPattern.IsMatch(strToCheck);
        } 
        // Function To check for Alphabets with space. 
        public bool IsAlphaWithSpace(String strToCheck)
        {
            Regex objAlphaWithSpacePattern = new Regex("[^a-zA-Z ]");
            return !objAlphaWithSpacePattern.IsMatch(strToCheck);
        }
        // Function to Check for AlphaNumeric.
        public bool IsAlphaNumeric(String strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex("[^a-zA-Z0-9]");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }
        // Function to Check for AlphaNumeric.
        public bool IsAlphaNumericWithSpace(String strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex("[^a-zA-Z0-9 ]");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }
        public bool IsLifetime(String strToCheck)
        {
           
                DateTime dt;//=new DateTime();
                
                return DateTime.TryParse(strToCheck, out dt);
        }
        //Function to check if it is valid email
        public  bool IsEmail(string strEmail)
        {
            string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                       + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                       + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                       + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                       + @"[a-zA-Z]{2,}))$";
            Regex emailregex = new Regex(patternStrict);

            Match m = emailregex.Match(strEmail);
            if (m.Success)
                return true;
            else
                return false;

        }

        // Function to Check for Telephone Number.
        //public bool IsTel(String strToCheck)
        //{
        //    Regex objTelPattern = new Regex(@"^\(?[+( ]?([0-9]{3})\)?[) ]?([0-9]{3})[- ]?([0-9]{4})$");
        //    return !objTelPattern.IsMatch(strToCheck);
        //}

        //Function to check if the string is empty
        public  bool IsEmpty(String strToCheck)
        {
            String tmpStr = strToCheck.Trim();
            if (tmpStr.Length == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // Size of a string In range
        //Note! it doesn't Trim the string
        public  bool SizeInRange(String strToCheck, int? LowSize, int? HighSize)
        {
            bool isValid = false;
            if ((LowSize != null) && (HighSize != null) && (LowSize != 0) && (HighSize != 0))
            {
                if ((strToCheck.Length >= LowSize) && (strToCheck.Length <= HighSize))
                {
                    isValid = true;
                }
            }
            else
            {
                if ((LowSize != null) && (LowSize!=0))
                {
                    if (strToCheck.Length >= LowSize)
                    {
                        isValid = true;
                    }
                }
                else
                {
                    if ((HighSize != null) && (HighSize != 0))
                    {
                        if (strToCheck.Length <= HighSize)
                        {
                            isValid = true;
                        }
                    }
                    else
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }
        // Size of a string less than
        //Note! it doesn't Trim the string
        public  bool StringSizeLessThan(String strToCheck, int HighSize)
        {
            bool isValid = false;
            if  (strToCheck.Length <= HighSize)
            {
                isValid = true;
            }
            return isValid;
        }
        // Size of a string greater than
        //Note! it doesn't Trim the string
        public  bool StringSizeGreaterThan(String strToCheck, int LowSize)
        {
            bool isValid = false;
            if (strToCheck.Length >= LowSize)
            {
                isValid = true;
            }
            return isValid;
        }

        // Size of a Int In range
        
        public  bool SizeInRange(int intToCheck, int? LowSize, int? HighSize)
        {
            bool isValid = false;
            if ((LowSize != null) && (HighSize != null) && (LowSize != 0) && (HighSize != 0))
            {
                if ((intToCheck >= LowSize) && (intToCheck <= HighSize))
                {
                    isValid = true;
                }
            }    
            else
            {
                if ((LowSize != null) && (LowSize != 0))
                {
                    if (intToCheck >= LowSize)
                    {
                        isValid = true;
                    }
                }
                else
                {
                    if ((HighSize != null) && (HighSize != 0))
                    {
                        if (intToCheck <= HighSize)
                        {
                            isValid = true;
                        }
                    }
                    else
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }
        // Size of a long In range

        //public  bool SizeInRange(long longToCheck, long LowSize, long HighSize)
        //{
        //    bool isValid = false;
        //    if ((longToCheck >= LowSize) && (longToCheck <= HighSize))
        //    {
        //        isValid = true;
        //    }
        //    return isValid;
        //}
        // Size of a decimal In range

        //public bool SizeInRange(decimal decimalToCheck, decimal LowSize, decimal HighSize)
        //{
        //    bool isValid = false;
        //    if ((decimalToCheck >= LowSize) && (decimalToCheck <= HighSize))
        //    {
        //        isValid = true;
        //    }
        //    return isValid;
        //}
        // Size of a double In range

        public  bool SizeInRange(double doubleToCheck, double? LowSize, double? HighSize)
        {
            bool isValid = false;
            if ((LowSize != null) && (HighSize != null) && (LowSize != 0) && (HighSize != 0))
            {
                if ((doubleToCheck >= LowSize) && (doubleToCheck <= HighSize))
                {
                    isValid = true;
                }
            }
            else
            {
                if ((LowSize != null) && (LowSize != 0))
                {
                    if (doubleToCheck >= LowSize)
                    {
                        isValid = true;
                    }
                }
                else
                {
                    if ((HighSize != null) && (HighSize != 0))
                    {
                        if (doubleToCheck <= HighSize)
                        {
                            isValid = true;
                        }
                    }
                    else
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }
        //Function to check if required  string is not empty
        public  bool RequiredValidate(String strToCheck)
        {
            if (strToCheck == null)
            {
                return false;
            }
            String tmpStr = strToCheck.Trim();
            if (tmpStr.Length != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Function to check if required  int is not empty or 0
        public  bool RequiredValidate(int? ValToCheck)
        {
            if (ValToCheck == null)
            {
                return false;
            }
          
            if (ValToCheck != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Function to check if required  long is not empty or 0
        public  bool RequiredValidate(long? ValToCheck)
        {
            if (ValToCheck == null)
            {
                return false;
            }

            if (ValToCheck != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Function to check if required  decimal is not empty or 0
        public  bool RequiredValidate(decimal? ValToCheck)
        {
            if (ValToCheck == null)
            {
                return false;
            }

            if (ValToCheck != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Function to check if required  double is not empty or 0
        public  bool RequiredValidate(double? ValToCheck)
        {
            if (ValToCheck == null)
            {
                return false;
            }

            if (ValToCheck != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

       //VALIDATE DATA TYPE
        public bool ValideDataType(object value, string FieldDataType)
        {
            bool hasFailed = false;
            
            switch (FieldDataType.ToLower())
            {
                case "alpha": hasFailed = !IsAlpha(value.ToString());
                    break;
                case "alphanumeric": hasFailed = !IsAlphaNumeric(value.ToString());
                    break;
                case "alphanumericwithspace": hasFailed = !IsAlphaNumericWithSpace(value.ToString());
                    break;
                case "email": hasFailed = !IsEmail(value.ToString());
                    break;
                case "string": //hasFailed = IsEmpty(value.ToString()); //everything except empty space
                    break;
                case "decimal":
                case "double":
                case "number":
                    hasFailed = !IsNumber(value.ToString());
                    break;
                case "positivedecimal":
                case "positivedouble":
                case "positivenumber":
                    hasFailed = !IsPositiveNumber(value.ToString());
                    break;
                case "int":
                case "integer":
                    hasFailed = !IsInteger(value.ToString());
                    break;     
                case "natural":
                    hasFailed = !IsNaturalNumber(value.ToString());
                    break;

                default: hasFailed=true;
                    break;
            }
            return hasFailed;
        }

        //VALIDATE REQUIRED
        public bool ValideRequiredTotal(object value, string FieldDataType)
        {
            bool hasFailed = false;
            switch (FieldDataType.ToLower())
            {
                case "alpha": 
                case "alphanumeric": 
                case "alphanumericwithspace": 
                case "email":
                case "string": hasFailed = !RequiredValidate(value.ToString()); //everything except empty space
                    break;
                case "decimal":
                case "double":
                case "number":
                case "positivedecimal":
                case "positivedouble":
                case "positivenumber":
                   hasFailed = !RequiredValidate(Convert.ToDouble(value.ToString()));
                    break;
                case "int":
                case "integer":
                case "natural":
                    hasFailed = !RequiredValidate(Convert.ToDouble(value.ToString()));
                    break;

                default: hasFailed = true;
                    break;
            }
            return hasFailed;
        }


        //VALIDATE RANGE
        public bool ValideRange(object value, string FieldDataType, int MinSize, int MaxSize)
        {
            bool hasFailed = false;
              switch (FieldDataType.ToLower())
                {
                    case "alpha":
                    case "alphanumeric":
                    case "alphanumericwithspace":
                    case "email":
                    case "string": hasFailed = !SizeInRange(value.ToString(), MinSize, MaxSize);
                    break;
                    case "decimal":
                    case "double":
                    case "number":
                    case "positivedecimal":
                    case "positivedouble":
                    case "positivenumber":
                    hasFailed = !SizeInRange(Convert.ToDouble(value.ToString()), MinSize, MaxSize);
                        break;
                    case "int":
                    case "integer":
                        hasFailed = !SizeInRange(Convert.ToInt64(value.ToString()), MinSize, MaxSize);
                        break;
                    case "natural":
                        hasFailed = !SizeInRange(Convert.ToInt64(value.ToString()), 1, MaxSize);
                        break;

                    default: hasFailed = true;
                        break;
                }
            
            return hasFailed;
        }
    }
}
