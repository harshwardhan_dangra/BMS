﻿using System;
using System.Configuration;

namespace BMS.Core.Utilities
{
    public static class ApplicationConfig
    {

        public static string APIVersion
        {
            get
            {
                return (string.IsNullOrEmpty(ConfigurationManager.AppSettings["APIVersion"]) ? "" : ConfigurationManager.AppSettings["APIVersion"]);
            }
        }



        #region Mail setting 

        public static string MailFrom { get { return string.IsNullOrEmpty(ConfigurationManager.AppSettings["MailFrom"]) ? "" : ConfigurationManager.AppSettings["MailFrom"]; } }

        public static string MailCC { get { return string.IsNullOrEmpty(ConfigurationManager.AppSettings["MailCC"]) ? "" : ConfigurationManager.AppSettings["MailCC"]; } }

        public static string SmtpServer { get { return string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpServer"]) ? "" : ConfigurationManager.AppSettings["SmtpServer"]; } }

        public static int SmtpPortNumber { get { return string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpPortNumber"]) ? 0 : Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPortNumber"]); } }

        public static string SmtpUserName { get { return string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpUserName"]) ? "" : ConfigurationManager.AppSettings["SmtpUserName"]; } }

        public static string SmtpUserPass { get { return string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpUserPass"]) ? "" : ConfigurationManager.AppSettings["SmtpUserPass"]; } }

        public static bool SmtpEnableSsl { get { return string.IsNullOrEmpty(ConfigurationManager.AppSettings["SmtpEnableSsl"]) ? false : Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSsl"]); } }

        #endregion Mail setting 



        #region Client settings

        public static string ContractRefNumber
        {
            get
            {
                return (string.IsNullOrEmpty(ConfigurationManager.AppSettings["ContractRefNumber"]) ? "" : ConfigurationManager.AppSettings["ContractRefNumber"]);
            }
        }


        public static string Date
        {
            get
            {
                return (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Date"]) ? "dd/MM/yyyy" : ConfigurationManager.AppSettings["Date"]);
            }
        }

        #endregion Client settings


        #region Client settings
        public static string UserName
        {
            get
            {
                return (string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserName"]) ? "ally.meer-hossen@BMS.com" : ConfigurationManager.AppSettings["UserName"]);
            }
        }

        public static string UserPwd
        {
            get
            {
                return (string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserPwd"]) ? "123" : ConfigurationManager.AppSettings["UserPwd"]);
            }
        }


        public static string UserFirstName
        {
            get
            {
                return (string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserFirstName"]) ? "First Name" : ConfigurationManager.AppSettings["UserFirstName"]);
            }
        }

        public static string UserLastName
        {
            get
            {
                return (string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserLastName"]) ? "Last Name" : ConfigurationManager.AppSettings["UserLastName"]);
            }
        }

        #endregion Client settings


        public static double AuthProfileCacheExpirySecs
        {
            get
            {
                double authProfileCacheExpirySecs = 0;
                double.TryParse(ConfigurationManager.AppSettings["AuthProfileCacheExpirySecs"], out authProfileCacheExpirySecs);

                return authProfileCacheExpirySecs;
            }
        }

        public static bool DisableToken { get { return false; } }

        public static string TemplatesPath { get { return (ConfigurationManager.AppSettings["TemplatesPath"]); } }


        public static int LoginTokenExpiryHours
        {
            get
            {
                int loginTokenExpiryHours = 0;

                int.TryParse(ConfigurationManager.AppSettings["LoginTokenExpiryHours"], out loginTokenExpiryHours);

                return loginTokenExpiryHours;
            }
        }

        public static int CommandTimeout
        {
            get
            {
                int CommandTimeout = 1800;

                int.TryParse(ConfigurationManager.AppSettings["CommandTimeout"], out CommandTimeout);

                return CommandTimeout;
            }
        }

        public static string APIRemoteUrl { get { return (ConfigurationManager.AppSettings["APIRemoteUrl"]); } }

        public static string ErrorLogFolderPath { get { return (ConfigurationManager.AppSettings["ErrorLogFolderPath"]); } }

        public static string MailTo { get { return (ConfigurationManager.AppSettings["MailTo"]); } }
        public static string BookingSubject { get { return (ConfigurationManager.AppSettings["BookingSubject"]); } }

        public static string MessageBirdSecretKey { get { return (ConfigurationManager.AppSettings["MessageBirdSecretKey"]); } }

        public static string MessageBirdSMSTitle { get { return (ConfigurationManager.AppSettings["MessageBirdSMSTitle"]); } }

        public static string CountryCode { get { return (ConfigurationManager.AppSettings["CountryCode"]); } }

        public static string ImmediateEmailTempatePath { get { return (ConfigurationManager.AppSettings["ImmediateEmailTempatePath"]); } }
        public static string ScheduledEmailTempatePath { get { return (ConfigurationManager.AppSettings["ScheduledEmailTempatePath"]); } }

        public static string PickupDirectoryLocation { get { return (ConfigurationManager.AppSettings["PickupDirectoryLocation"]); } }

        public static string MailFromName { get { return (ConfigurationManager.AppSettings["MailFromName"]); } }
        public static string ImmediateEmailViewInBrowserDomain { get { return (ConfigurationManager.AppSettings["ImmediateEmailViewInBrowserDomain"]); } }
        public static string ScheduledEmailViewInBrowserDomain { get { return (ConfigurationManager.AppSettings["ScheduledEmailViewInBrowserDomain"]); } }

        public static string AudioBookingSendEmailDisabled { get { return (ConfigurationManager.AppSettings["AudioBookingSendEmailDisabled"]); } }
        public static string VideoBookingSendEmailDisabled { get { return (ConfigurationManager.AppSettings["VideoBookingSendEmailDisabled"]); } }

        public static string AudioBookingSendSMSDisabled { get { return (ConfigurationManager.AppSettings["AudioBookingSendSMSDisabled"]); } }
        public static string VideoBookingSendSMSDisabled { get { return (ConfigurationManager.AppSettings["VideoBookingSendSMSDisabled"]); } }
    }
}
