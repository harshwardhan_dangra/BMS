﻿using BMS.Security.Service.Data;
using BMS.Security.Service.Entities;

namespace BMS.Security.Service.Services
{
    public class UsersRegionService : IUsersRegionService
    {

        private ISecurityDbContext SecurityDbContext { get; set; }


        public UsersRegionService(ISecurityDbContext securityDbContext)
        {
            this.SecurityDbContext = securityDbContext;
        }       

        public UsersRegion InsertUserRegion(UsersRegion user)
        {
           return SecurityDbContext.UsersRegionRepository.Add(user);
        }
    }
}
