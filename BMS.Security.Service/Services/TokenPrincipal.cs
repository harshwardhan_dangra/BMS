﻿using Core.Services;
using Security.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Security.Service.Services
{
    public class TokenPrincipal : ITokenPrincipal
    {

        public User User
        {
            get;
            set;
        }

        public Guid Token
        {
            get;
            set;
        }

        public DateTime ExpiryDate
        {
            get;
            set;
        }

        public IIdentity Identity
        {
            get;
            set;
        }

        public long UserId
        {
            get
            {
                long userId = 0;
                if (User != null)
                {
                    userId = User.UserId;
                }

                return userId;
            }

        }

        public bool IsInRole(string role)
        {
            return false;
        }

        public TokenPrincipal(string email)
        {
            this.Identity = new GenericIdentity(email);
        }
    }
}
