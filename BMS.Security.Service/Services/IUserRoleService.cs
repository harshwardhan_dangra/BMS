﻿using BMS.Core.Services;
using BMS.Security.Service.BusinessObjects;
using System.Collections.Generic;

namespace BMS.Security.Service.Services
{
    public interface IUserRoleService : IService
    {
        List<UserRoleBO> UserRoleSearchList(Dictionary<string, string> searchFields);
    }
}
