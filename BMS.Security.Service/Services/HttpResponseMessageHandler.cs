﻿using System.Net.Http;

namespace BMS.Security.Service.Services
{
    public class HttpResponseMessageHandler 
    {
        public HttpRequestMessage Request;
        public HttpResponseMessage Response;
        public HttpResponseMessageHandler(HttpRequestMessage request, HttpResponseMessage response)
        {
            Request = request;
            Response = response;
        }

        //public HttpResponseMessageHandler()
        //{
        ////    Request = request;
        ////    Response = response;
        //}
    }
}
