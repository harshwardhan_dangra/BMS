﻿using BMS.Core.Services;
using BMS.Security.Service.Entities;

namespace BMS.Security.Service.Services
{
    public interface IUsersRegionService : IService
    {
        UsersRegion InsertUserRegion(UsersRegion user);
    }
}
