﻿using System.Runtime.Serialization;

namespace BMS.Security.Service.BusinessObjects
{
    [DataContract]
    public class LoginRequest
    {
        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}
