﻿using System;
using System.Runtime.Serialization;
using BMS.Security.Service.Entities;

namespace BMS.Security.Service.BusinessObjects
{
    [DataContract]
    public class LoginResponse : User
    {
        //[DataMember(Name = "userId")]
        //public long UserId { get; set; }

        //[DataMember(Name = "firstName")]
        //public string FirstName { get; set; }

        //[DataMember(Name = "lastName")]
        //public string LastName { get; set; }

        //[DataMember(Name = "email")]
        //public string Email { get; set; }

        [DataMember(Name = "Token")]
        public Guid Token { get; set; }

        //public User User { get; set; }

        [DataMember(Name = "userType")]
        public string UserType { get; set; }

    }
}
