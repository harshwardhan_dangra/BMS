﻿using System.Runtime.Serialization;
using BMS.Security.Service.Entities;

namespace BMS.Security.Service.BusinessObjects
{
    [DataContract]
    public class UserBO : User
    {

        [DataMember(Name = "userType")]
        public string UserType { get; set; }

        [DataMember(Name = "fullName")]
        public string FullName { get; set; }


        [DataMember(Name = "Role")]
        public string Role { get; set; }
        
        [DataMember(Name = "totalRows")]
        public int TotalRows { get; set; }

    }


    [DataContract]
    public class UserHomeBO : User
    {

        [DataMember(Name = "AllUsersCount")]
        public int AllUsersCount { get; set; }

        [DataMember(Name = "DisabledUsersCount")]
        public int DisabledUsersCount { get; set; }


        [DataMember(Name = "EmployeesCount")]
        public int EmployeesCount { get; set; }

        [DataMember(Name = "ContractorsCount")]
        public int ContractorsCount { get; set; }

        [DataMember(Name = "AllocatedPCCounts")]
        public int AllocatedPCCounts { get; set; }

        [DataMember(Name = "PCNotReturnedCounts")]
        public int PCNotReturnedCounts { get; set; }

    }
}
