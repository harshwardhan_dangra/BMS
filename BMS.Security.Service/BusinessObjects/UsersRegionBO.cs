﻿using System.Runtime.Serialization;
using BMS.Security.Service.Entities;

namespace BMS.Security.Service.BusinessObjects
{
    [DataContract]
    public class UsersRegionBO : UsersRegion
    {


        [DataMember(Name = "totalRows")]
        public int TotalRows { get; set; }

    }
}
