﻿using BMS.Security.Service.Entities;
using System.Runtime.Serialization;

namespace BMS.Security.Service.BusinessObjects
{
    [DataContract]
    public class UserRoleBO : UserRole
    {       

        [DataMember(Name = "totalRows")]
        public int TotalRows { get; set; }

    }
}
