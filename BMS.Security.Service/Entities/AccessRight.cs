﻿using BMS.Core.Entities;
using BMS.Security.Service.Constants;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace BMS.Security.Service.Entities
{
    [DataContract]
    public class AccessRight : BaseEntity
    {
        [DataMember(Name = "accessRightsId")]
        public long AccessRightsId { get; set; }

        [DataMember(Name = "userId"), Required(ErrorMessage ="userId required")]        
        public long UserId { get; set; }

        //[DataMember(Name = "entityId"), Required(ErrorMessage = "entityId required")]  
        //public long? EntityId { get; set; }

        [DataMember(Name = "entityType"), Required(ErrorMessage = "entityType required")]  
        public EntityType? EntityType { get; set; }

        private KorrectAccessRight korrectAccessRight;

        [DataMember(Name = "korrectAccessRight"), Required(ErrorMessage = "korrectAccessRight required")]  
        public KorrectAccessRight KorrectAccessRight
        {
            get
            {
                return korrectAccessRight;
            }
            set
            {
                korrectAccessRight = value;
                switch (korrectAccessRight)
                {
                    //case KorrectAccessRight.ManageUsers :
                        
                    //    SecurityPrincipal = SecurityPrincipalType.ManageUsers;
                    //    AllowedRights = Right.Create | Right.Read | Right.Update | Right.Delete;
                    //    break;
                        

                    //case KorrectAccessRight.Edit:
                    //    SecurityPrincipal = SecurityPrincipalType.GeneralAccess;
                    //    AllowedRights = Right.Create | Right.Read | Right.Update | Right.Delete;
                    //    break;

                    //case KorrectAccessRight.ReadAndAnalyze:
                    //    SecurityPrincipal = SecurityPrincipalType.GeneralAccess;
                    //    AllowedRights = Right.Read;
                    //    break;

                    //case KorrectAccessRight.ManageUsers | KorrectAccessRight.Edit :
                    //case KorrectAccessRight.Edit | KorrectAccessRight.ReadAndAnalyze:
                    //case KorrectAccessRight.ManageUsers | KorrectAccessRight.Edit | KorrectAccessRight.ReadAndAnalyze:

                    //    SecurityPrincipal = SecurityPrincipalType.GeneralAccess;
                    //    AllowedRights = Right.Create | Right.Read | Right.Update | Right.Delete;
                    //    break;
                }
            }
        }

        public SecurityPrincipalType SecurityPrincipal { get; set; }

        public Right AllowedRights { get; set; }

        public Right DeniedRights { get; set; }

        public AccessRight()
            : base("AccessRightsId")
        {}

        public override bool Equals(object accessRight)
        {
            // If parameter is null return false.
            if (accessRight == null)
            {
                return false;
            }

            // If parameter cannot be cast to AccessRight return false.
            AccessRight a = accessRight as AccessRight;
            if ((System.Object)a == null)
            {
                return false;
            }

            return (UserId == a.UserId)
                //&& (EntityId == a.EntityId)
                && (EntityType == a.EntityType)
                && (KorrectAccessRight == a.KorrectAccessRight)
                && (SecurityPrincipal == a.SecurityPrincipal)
                && (AllowedRights == a.AllowedRights)
                && (DeniedRights == a.DeniedRights);
        }
    }
}