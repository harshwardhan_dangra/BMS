﻿using BMS.Core.Entities;
using System;
using System.Runtime.Serialization;

namespace BMS.Security.Service.Entities
{
    public class UserAuthToken : BaseEntity
    {

        [DataMember(Name = "userAuthTokenId")]
        public long UserAuthTokenId { get; set; }

        [DataMember(Name = "userId")]
        public long UserId { get; set; }

        [DataMember(Name = "tokenKey")]
        public Guid TokenKey { get; set; }

        [DataMember(Name = "loginDate")]
        public DateTime LoginDate { get; set; }

        [DataMember(Name = "expiryDate")]
        public DateTime ExpiryDate { get; set; }

        public int  ExpiryHours { get; set; }

        public UserAuthToken()
            : base("UserAuthTokenId") { }
    }
}
