﻿using BMS.Core.Data;
using BMS.Security.Service.Entities;
namespace BMS.Security.Service.Data
{
    public interface IUsersRegionRepository : IRepository<UsersRegion>
    {        

    }
}
