﻿using BMS.Core.Data;
using BMS.Security.Service.BusinessObjects;
using BMS.Security.Service.Entities;
using System.Collections.Generic;
namespace BMS.Security.Service.Data
{
    public interface IUserRepository : IRepository<User>
    {

        List<AccessRight> GetUserEntityAccessRights(long? userId);
        List<UserBO> UserList(Dictionary<string, string> searchFields);
        List<UserHomeBO> UsersHomeCount(Dictionary<string, string> searchFields);

        List<UserBO> UserSearchList(Dictionary<string, string> searchFields);
        UserBO GetUserById(long id);
    }
}
