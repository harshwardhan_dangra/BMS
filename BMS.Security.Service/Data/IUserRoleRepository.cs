﻿using BMS.Core.Data;
using BMS.Security.Service.BusinessObjects;
using BMS.Security.Service.Entities;
using System.Collections.Generic;
namespace BMS.Security.Service.Data
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {

        List<UserRoleBO> UserRoleSearchList(Dictionary<string, string> searchFields);

    }
}
