﻿using BMS.Core.Data;
using BMS.Security.Service.BusinessObjects;
using BMS.Security.Service.Entities;
using System;
namespace BMS.Security.Service.Data
{
    public interface IUserAuthTokenRepository : IRepository<UserAuthToken>
    {
        void Logout(Guid token);
        TokenPrincipal GetUserFromTokenOrEmail(Guid token, string userEmail);
    }
}
