﻿using BMS.Core.Data;
using BMS.Core.Services;
using BMS.Core.Utilities;
using BMS.Security.Service.Services;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BMS.Security.Service.Handlers
{
    /**
     * This handler rewrites everything using a supported type.
     */
    public class ResponseHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {

            return SendAsyncHandler(request, cancellationToken).ContinueWith<HttpResponseMessage>((responseToCompleteTask) =>
            {
                HttpResponseMessage response = responseToCompleteTask.Result.Response;
                response.Headers.Add("Version", ApplicationConfig.APIVersion);
                CommonAppLog commonAppLog = null;

                TaskScheduler syncContextTaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();

                if (!response.IsSuccessStatusCode)
                {
                    HttpError error = null;
                    BusinessError businessError = null;
                    
                    if (response.TryGetContentValue<HttpError>(out error))
                    {
                        //CommonLogAppRepository
                        commonAppLog = new CommonAppLog();
                        commonAppLog.StackTrace = string.IsNullOrEmpty(error.StackTrace) == false ?  error.StackTrace : "";
                        commonAppLog.Message = error.GetPropertyValue<string>("ExceptionMessage") != null
                            ? error.GetPropertyValue<string>("ExceptionMessage")
                            : error.GetPropertyValue<string>("Message") != null ? error.GetPropertyValue<string>("Message") : "";
                        commonAppLog.LogType = LogType.Error;
                        
                    }
                    else if (response.TryGetContentValue<BusinessError>(out businessError))
                    {
                        //Filter appropriate attributes to return 
                        commonAppLog = new CommonAppLog();

                        if (businessError.RawException != null){
                        commonAppLog.StackTrace = businessError.RawException.StackTrace;
                        commonAppLog.Message = businessError.RawException.Message;
                        }

                        commonAppLog.ApplicationName = businessError.BusinessModule.ToString();
                        commonAppLog.ComponentName = businessError.ReasonPhrase;
                        //commonLogApp.MethodName = businessError.Message;

                        commonAppLog.LogType = LogType.BusinessError;
                    }
                }
                else
                {
                    //log sucessful response
                    commonAppLog = new CommonAppLog();
                    commonAppLog.LogType = LogType.Information;
                }

                if (commonAppLog != null)
                {
                    //User ID
                    if (ProfileContextHelper.CurrentTokenPrincipal != null)
                    {
                        commonAppLog.UserId = ProfileContextHelper.CurrentTokenPrincipal.User.UserId;
                    }

                    //Request content
                    if (responseToCompleteTask.Result.Request.Content != null)
                    {
                        commonAppLog.GatewayRequest = UTF8Encoding.UTF8.GetString(responseToCompleteTask.Result.Request.Content.ReadAsByteArrayAsync().Result);
                    }

                    //Response content
                    if (response.Content != null)
                    {
                        commonAppLog.GatewayResponse = UTF8Encoding.UTF8.GetString(response.Content.ReadAsByteArrayAsync().Result);
                    }

                    if (responseToCompleteTask.Result.Request != null)
                    {
                        HttpContextWrapper context = (HttpContextWrapper)request.Properties["MS_HttpContext"];

                        if (context != null){
                            commonAppLog.MachineIp = context.Request.ServerVariables["REMOTE_ADDR"];
                        }
                        //Properties["MS_HttpActionDescriptor"]
                        if (responseToCompleteTask.Result.Request.GetActionDescriptor() != null)
                        {
                            commonAppLog.MethodName = responseToCompleteTask.Result.Request.GetActionDescriptor().ActionName;
                        }
                    }

                    CoreDBContext coreDBContext = new CoreDBContext();
                    //coreDBContext.CommonAppLogRepository.Add(commonAppLog);
                }

                return response;
            }
            , TaskScheduler.FromCurrentSynchronizationContext()
            );
        }

        protected async Task<HttpResponseMessageHandler> SendAsyncHandler(HttpRequestMessage request, CancellationToken cancellationToken)
        {
             var response = await base.SendAsync(request, cancellationToken);
            HttpResponseMessageHandler customResponse = new HttpResponseMessageHandler(request, response);

            return customResponse;
        }
    }
}