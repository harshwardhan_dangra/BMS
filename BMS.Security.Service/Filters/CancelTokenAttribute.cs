﻿using System;

namespace BMS.Security.Service.Filters
{
    /// <summary>
    /// Attribute to cancel the global action of Token Attribute
    /// </summary>
    public class CancelTokenAttribute : Attribute
    {

    }
}
