﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Services;
using BMS.Core.Utilities;
using MessageBird;
using MessageBird.Objects;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BMSScheduler
{
    class Program
    {
        private static Database database;

        static void Main(string[] args)
        {
            Synchronize synchronize = new Synchronize();
            synchronize.Sync();
        }
    }

    public class Synchronize
    {
        #region DB contexts
        private ICMDBContext CMDBContext { get; set; }
        #endregion

        #region Services
        private ISettingsService _settingsService { get; set; }
        private IBookingService _bookingService { get; set; }
        #endregion Services       

        public Synchronize()
        {
            var database = new DatabaseProviderFactory().CreateDefault();
            this.CMDBContext = new CMDBContext(database);
            this._settingsService = new SettingsService(this.CMDBContext);
            this._bookingService = new BookingService(this.CMDBContext);
        }

        public void Sync()
        {
            try
            {
                List<SettingsBO> settingsBO = _settingsService.List();
                SettingsBO emailsettings = settingsBO.Where(x => x.SettingKey == "SendEmailRemider").FirstOrDefault();
                SettingsBO smssettings = settingsBO.Where(x => x.SettingKey == "SendSMSRemider").FirstOrDefault();
                if(Convert.ToInt32(emailsettings.SettingValue) > 0)
                {
                    emailsettings.SettingValue = "-" + emailsettings.SettingValue;
                }
                if (Convert.ToInt32(smssettings.SettingValue) > 0)
                {
                    smssettings.SettingValue = "-" + smssettings.SettingValue;
                }
                var emailfiltereddate = DateTime.Now.AddDays(Convert.ToInt32(emailsettings.SettingValue));
                var smsfiltereddate = DateTime.Now.AddDays(Convert.ToInt32(smssettings.SettingValue));
                Dictionary<string, string> emailkeyValuePairs = new Dictionary<string, string>();
                emailkeyValuePairs.Add("bookingStartDate", emailfiltereddate.ToShortDateString());
                emailkeyValuePairs.Add("bookingEndDate", emailfiltereddate.ToShortDateString());
                List<BookingBO> emailsbookinglist = _bookingService.List(emailkeyValuePairs);
                foreach (BookingBO booking in emailsbookinglist)
                {
                    string mailTo = ApplicationConfig.MailTo;
                    string mailFrom = ApplicationConfig.MailFrom;
                    string mailFromName = ApplicationConfig.MailFromName;
                    int Port = ApplicationConfig.SmtpPortNumber;
                    string Host = ApplicationConfig.SmtpServer;
                    bool EnableSsl = ApplicationConfig.SmtpEnableSsl;
                    int timeout = ApplicationConfig.CommandTimeout;
                    string sendermail = ApplicationConfig.SmtpUserName;
                    string senderpassword = ApplicationConfig.SmtpUserPass;
                    string Subject = ApplicationConfig.BookingSubject; ;
                    string PickupDirectoryLocation = ApplicationConfig.PickupDirectoryLocation;
                    SettingsBO settings = settingsBO.Where(x => x.SettingKey == "emailSettings").FirstOrDefault();
                    string EmailSettings = settings != null ? settings.SettingValue : string.Empty;
                    string emailtemplate = ApplicationConfig.ImmediateEmailTempatePath;
                    byte[] bookingidbyte = Encoding.UTF8.GetBytes(Convert.ToString(booking.Id));
                    string ScheduledEmailViewInBrowserDomain = string.Concat(ApplicationConfig.ScheduledEmailViewInBrowserDomain, WebUtility.UrlEncode(Convert.ToBase64String(bookingidbyte)));
                    string line = File.ReadAllText(emailtemplate).Replace("{ScheduledEmailViewInBrowserDomain}", ScheduledEmailViewInBrowserDomain).Replace("{Name}", booking.FirstName + " " + booking.LastName).Replace("{BookingDate}", booking.BookingDateandTime.Value.ToLongDateString()).Replace("{BookingTime}", booking.BookingDateandTime.Value.ToLongTimeString()).Replace("{Location}", booking.Location).Replace("{CallType}", booking.BookingType == 2 ? "Video" : "Audio").Replace("{MortgageType}", booking.Reasonforenquiry).Replace("{AccessCode}", booking.AccessCode);
                    string Body = line;
                    CommonFunction.SendMail(mailTo, mailFrom, mailFromName, Port, Host, EnableSsl, timeout, sendermail, senderpassword, Subject, Body, PickupDirectoryLocation, true);
                    CommonFunction.SendMail(booking.EmailAddress, mailFrom, mailFromName, Port, Host, EnableSsl, timeout, sendermail, senderpassword, Subject, Body, PickupDirectoryLocation, true);
                    var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "Log_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                    var fileExists = File.Exists(filepath);
                    using (var writer = new StreamWriter(filepath, true))
                    {
                        if (!fileExists)
                        {
                            writer.WriteLine("Start Error Log for today");
                        }
                        writer.WriteLine("Send Mail for the Email " + booking.EmailAddress + " at " + DateTime.Now + "Successfully");
                    }
                }
                Dictionary<string, string> smskeyValuePairs = new Dictionary<string, string>();
                smskeyValuePairs.Add("bookingStartDate", smsfiltereddate.ToShortDateString());
                smskeyValuePairs.Add("bookingEndDate", smsfiltereddate.ToShortDateString());
                List<BookingBO> smsbookinglist = _bookingService.List(smskeyValuePairs);
                foreach (BookingBO booking in smsbookinglist)
                {
                    smssettings = settingsBO.Where(x => x.SettingKey == "smsSettings").FirstOrDefault();
                    string SMSBody = smssettings != null ? smssettings.SettingValue.Replace("{AccessCode}", booking.AccessCode) : string.Empty;
                    long Msisdn = Convert.ToInt64(ApplicationConfig.CountryCode + booking.ContactNumber);
                    Client client = Client.CreateDefault(ApplicationConfig.MessageBirdSecretKey);
                    Balance balance = client.Balance();
                    client.SendMessage(ApplicationConfig.MessageBirdSMSTitle, SMSBody, new[] { Msisdn });


                    var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "Log_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                    var fileExists = File.Exists(filepath);
                    using (var writer = new StreamWriter(filepath, true))
                    {
                        if (!fileExists)
                        {
                            writer.WriteLine("Start Error Log for today");
                        }
                        writer.WriteLine("Send Message for the Number " + Msisdn + " at " + DateTime.Now + "Successfully");
                    }
                }
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "Log_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
            }
        }
    }
}
