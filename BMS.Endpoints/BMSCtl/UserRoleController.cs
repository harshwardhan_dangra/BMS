﻿using BMS.CMDB.Service.Data;
using BMS.Core.Controllers;
using BMS.Security.Service.BusinessObjects;
using BMS.Security.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BMS.Endpoints.API.BMSCtl
{
    [RoutePrefix("bms/userrole")]
    public class UserRoleController : BaseApiController
    {
        #region DB contexts
        private ICMDBContext CMDBContext;
        #endregion

        #region Services
        private IUserRoleService _userRoleService;
        #endregion Services

        #region Constructor
        public UserRoleController(ICMDBContext CMDBContext, IUserService userService, IUserRoleService userRoleService) : base(null, null, userService)
        {
            this.CMDBContext = CMDBContext;
            this._userRoleService = userRoleService;
        }
        #endregion Constructor

        [Route("list")]
        [HttpGet]
        public List<UserRoleBO> List()
        {
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            return _userRoleService.UserRoleSearchList(searchFields);
        }

    }
}
