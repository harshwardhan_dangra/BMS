﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Entities;
using BMS.CMDB.Service.Services;
using BMS.Core.Controllers;
using BMS.Core.Utilities;
using BMS.Security.Service.Filters;
using BMS.Security.Service.Services;
using MessageBird;
using MessageBird.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;


namespace BMS.Endpoints.API.BMSCtl
{
    [RoutePrefix("bms/booking")]
    public class BookingController : BaseApiController
    {
        #region DB contexts
        private ICMDBContext CMDBContext;
        #endregion

        #region Services
        private IBookingService _bookingService;
        private ISettingsService _settingService;
        #endregion Services

        #region Constructor
        public BookingController(ICMDBContext CMDBContext, IUserService userService, IBookingService bookingService, ISettingsService settingService) : base(null, null, userService)
        {
            this.CMDBContext = CMDBContext;
            this._bookingService = bookingService;
            this._settingService = settingService;
        }
        #endregion Constructor


        [Route("save")]
        [HttpPost]
        [CancelToken]
        public Booking Save(Booking booking)
        {

            if (booking.BookingType == 1)
            {
                if (!booking.BookingDateandTime.HasValue)
                    booking.BookingDateandTime = DateTime.Now;
            }
            _bookingService.Save(booking);
            BookingBO bookingBO = _bookingService.GetBookingBOById(booking.Id);
            SalesforceModel salesforceModel = new SalesforceModel
            {
                LeadID = Guid.NewGuid().ToString(),
                Forename = bookingBO.FirstName,
                Surname = bookingBO.LastName,
                Address = bookingBO.Location,
                Email = bookingBO.EmailAddress,
                TelephoneNumber = bookingBO.ContactNumber,
                Branch = bookingBO.Branch,
                PreferredDay = bookingBO.BookingType == 2 ? bookingBO.BookingDateandTime.HasValue ? bookingBO.BookingDateandTime.Value.ToShortDateString() : "" : null,
                PreferredTime = bookingBO.BookingType == 2 ? bookingBO.BookingDateandTime.HasValue ? bookingBO.BookingDateandTime.Value.ToShortTimeString() : "" : null,
                Reasonforenquiry = bookingBO.Reasonforenquiry,
                RequestFromPhoneApp = "Mobile",
                Comments = bookingBO.Comments,
                Referrer = bookingBO.Referrer,
                BranchLocation = bookingBO.BranchLocation
            };
            string sFmethod = string.Empty;
            if (bookingBO.BookingType == 1)
            {
                sFmethod = "NMMoneyBranchMortgageLeadPhone";
            }
            else //(bookingBO.BookingType == 2)
            {
                sFmethod = "NMMoneyBranchMortgageLeadVPod";
            }
            try
            {
                SalesforceManager.AddFormEntry(salesforceModel, sFmethod);
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
            }
            if (booking.AccessCodeChanged == true)
            {
                string mailTo = ApplicationConfig.MailTo;
                string mailFrom = ApplicationConfig.MailFrom;
                string mailFromName = ApplicationConfig.MailFromName;
                int Port = ApplicationConfig.SmtpPortNumber;
                string Host = ApplicationConfig.SmtpServer;
                bool EnableSsl = ApplicationConfig.SmtpEnableSsl;
                int timeout = ApplicationConfig.CommandTimeout;
                string sendermail = ApplicationConfig.SmtpUserName;
                string senderpassword = ApplicationConfig.SmtpUserPass;
                string Subject = ApplicationConfig.BookingSubject;
                string PickupDirectoryLocation = ApplicationConfig.PickupDirectoryLocation;
                List<SettingsBO> settingsBO = _settingService.List();
                SettingsBO settings = settingsBO.Where(x => x.SettingKey == "emailSettings").FirstOrDefault();
                string EmailSettings = settings != null ? settings.SettingValue : string.Empty;
                string emailtemplate = ApplicationConfig.ImmediateEmailTempatePath;
                byte[] bookingidbyte = Encoding.UTF8.GetBytes(Convert.ToString(booking.Id));
                string ImmediateEmailViewInBrowserDomain = string.Concat(ApplicationConfig.ImmediateEmailViewInBrowserDomain, WebUtility.HtmlEncode(Convert.ToBase64String(bookingidbyte)));
                string line = File.ReadAllText(emailtemplate).Replace("{ImmediateEmailViewInBrowserDomain}", ImmediateEmailViewInBrowserDomain).Replace("{Name}", bookingBO.FirstName + " " + bookingBO.LastName).Replace("{BookingDate}", bookingBO.BookingDateandTime.Value.ToLongDateString()).Replace("{BookingTime}", bookingBO.BookingDateandTime.Value.ToLongTimeString()).Replace("{Location}", bookingBO.Location).Replace("{CallType}", bookingBO.BookingType == 2 ? "Video" : "Audio").Replace("{MortgageType}", bookingBO.Reasonforenquiry).Replace("{AccessCode}", bookingBO.AccessCode);
                string Body = line;
                if (Convert.ToBoolean(ApplicationConfig.AudioBookingSendEmailDisabled))
                {
                    CommonFunction.SendMail(mailTo, mailFrom, mailFromName, Port, Host, EnableSsl, timeout, sendermail, senderpassword, Subject, Body, PickupDirectoryLocation, true);
                    CommonFunction.SendMail(booking.EmailAddress, mailFrom, mailFromName, Port, Host, EnableSsl, timeout, sendermail, senderpassword, Subject, Body, PickupDirectoryLocation, true);
                }
                if (Convert.ToBoolean(ApplicationConfig.VideoBookingSendEmailDisabled))
                {
                    CommonFunction.SendMail(mailTo, mailFrom, mailFromName, Port, Host, EnableSsl, timeout, sendermail, senderpassword, Subject, Body, PickupDirectoryLocation, true);
                    CommonFunction.SendMail(booking.EmailAddress, mailFrom, mailFromName, Port, Host, EnableSsl, timeout, sendermail, senderpassword, Subject, Body, PickupDirectoryLocation, true);
                }
                SettingsBO smssettings = settingsBO.Where(x => x.SettingKey == "smsSettings").FirstOrDefault();
                string SMSBody = smssettings != null ? smssettings.SettingValue.Replace("{AccessCode}", bookingBO.AccessCode) : string.Empty;
                long Msisdn = Convert.ToInt64(ApplicationConfig.CountryCode + booking.ContactNumber);
                if (Convert.ToBoolean(ApplicationConfig.VideoBookingSendSMSDisabled))
                {
                    Client client = Client.CreateDefault(ApplicationConfig.MessageBirdSecretKey);
                    Balance balance = client.Balance();
                    client.SendMessage(ApplicationConfig.MessageBirdSMSTitle, SMSBody, new[] { Msisdn });
                }
                if (Convert.ToBoolean(ApplicationConfig.AudioBookingSendSMSDisabled))
                {
                    Client client = Client.CreateDefault(ApplicationConfig.MessageBirdSecretKey);
                    Balance balance = client.Balance();
                    client.SendMessage(ApplicationConfig.MessageBirdSMSTitle, SMSBody, new[] { Msisdn });
                }
            }
            return booking;
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public bool Delete(long id)
        {
            return _bookingService.Delete(id);
        }

        [Route("archive/{id}")]
        [HttpGet]
        public bool Archive(long id)
        {
            return _bookingService.Archive(id);
        }


        [Route("unarchive/{id}")]
        [HttpGet]
        public bool UnArchive(long id)
        {
            return _bookingService.UnArchive(id);
        }

        [Route("getbyId/{id}")]
        [HttpGet]
        [CancelToken]
        public BookingBO GetBookingById(long id)
        {
            return _bookingService.GetBookingBOById(id);
        }

        [Route("list")]
        [HttpGet]
        public List<BookingBO> List()
        {
            //SalesforceManager.SendSMSAuth();
            //SalesforceManager.SendSMS("");
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);

            return _bookingService.List(searchFields);
        }
    }
}
