﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Entities;
using BMS.CMDB.Service.Services;
using BMS.Core.Controllers;
using BMS.Security.Service.Filters;
using BMS.Security.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace BMS.Endpoints.API.BMSCtl
{
    [RoutePrefix("bms/appointment")]
    public class AppointmentController : BaseApiController
    {
        #region DB contexts
        private ICMDBContext CMDBContext;
        #endregion

        #region Services
        private IAppointmentService _appointmentService;
        #endregion Services

        #region Constructor
        public AppointmentController(ICMDBContext CMDBContext, IUserService userService, IAppointmentService appointmentService) : base(null, null, userService)
        {
            this.CMDBContext = CMDBContext;
            this._appointmentService = appointmentService;
        }
        #endregion Constructor

        [Route("allList")]
        [HttpGet]
        [CancelToken]
        public List<AppointmentBO> GetAllList()
        {
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            return _appointmentService.GetAllList(searchFields);
        }

        [Route("list")]
        [HttpGet]
        [CancelToken]
        public List<AppointmentBO> List()
        {
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            return _appointmentService.List(searchFields);
        }


        [Route("appointmentlist")]
        [HttpGet]
        [CancelToken]
        public List<AppointmentDatesBI> AppointmentList()
        {
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            var listOfAppoints = _appointmentService.List(searchFields);
            List<AppointmentDatesBI> model = new List<AppointmentDatesBI>();

            var distinctDates = listOfAppoints.Select(x => x.AppointmentDate).Distinct().ToList();

            for (int i = 0; i < distinctDates.Count; i++)
            {
                var dateObject = listOfAppoints.FirstOrDefault(x => x.AppointmentDate == distinctDates[i]);
                var listOfTimes = listOfAppoints.Where(x => x.AppointmentDate == distinctDates[i]).ToList();
                model.Add(new AppointmentDatesBI
                {
                    ApIndex = i,
                    ApDates = dateObject,
                    ApTimes = listOfTimes
                });

            }
            return model;
        }


        [Route("save")]
        [HttpPost]
        public bool Save(AppointmentSaveBO appointmentSaveBO)
        {

            return _appointmentService.Save(appointmentSaveBO);
        }

        [Route("appointmenttimeslotslist")]
        [HttpGet]
        public List<AppointmentBO> AppointmentTimeSlotsist()
        {
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            return _appointmentService.TimeSlotsList(searchFields);
        }


        [Route("allappointmenttimeslotslist")]
        [HttpGet]
        public List<AppointmentBO> Allappointmenttimeslotslist()
        {
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            return _appointmentService.Allappointmenttimeslotslist(searchFields);
        }

    }
}
