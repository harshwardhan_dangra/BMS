﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Entities;
using BMS.CMDB.Service.Services;
using BMS.Core.Controllers;
using BMS.Security.Service.Filters;
using BMS.Security.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace BMS.Endpoints.API.BMSCtl
{
    [RoutePrefix("bms/branch")]
    public class BranchController : BaseApiController
    {
        #region DB contexts
        private ICMDBContext CMDBContext;
        #endregion

        #region Services
        private IBranchService _branchService;
        #endregion Services

        #region Constructor
        public BranchController(ICMDBContext CMDBContext, IUserService userService, IBranchService branchService) : base(null, null, userService)
        {
            this.CMDBContext = CMDBContext;
            this._branchService = branchService;
        }
        #endregion Constructor

        [Route("save")]
        [HttpPost]
        public Branch Save(Branch branch)
        {
            return _branchService.Save(branch);
        }


        [Route("delete/{id}")]
        [HttpDelete]
        public bool Delete(long id)
        {
            return _branchService.Delete(id);
        }

        [Route("list")]
        [HttpGet]
        [CancelToken]
        public List<BranchBO> List()
        {
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            return _branchService.List(searchFields);
        }


        [Route("getbyId/{id}")]
        [HttpGet]
        public Branch GetBranchbyId(long id)
        {
            return _branchService.GetBranchById(id);
        }


        [Route("allBranchTotalCapacity")]
        [HttpGet]
        public int AllBranchTotalCapacity()
        {
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            return _branchService.AllBranchTotalCapacity(searchFields);
        }

        [Route("archive/{id}")]
        [HttpGet]
        public bool Archive(long id)
        {
            return _branchService.Archive(id);
        }

        [Route("unarchive/{id}")]
        [HttpGet]
        public bool UnArchive(long id)
        {
            return _branchService.UnArchive(id);
        }

        [Route("headbranchcapacity")]
        [HttpGet]
        public int HeadBranchCapacity()
        {
            return _branchService.HeadBranchCapacity();
        }
    }
}
