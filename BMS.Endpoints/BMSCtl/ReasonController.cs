﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Services;
using BMS.Core.Controllers;
using BMS.Security.Service.Filters;
using BMS.Security.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace BMS.Endpoints.API.BMSCtl
{
    [RoutePrefix("bms/reason")]
    public class ReasonController : BaseApiController
    {
        #region DB contexts
        private ICMDBContext CMDBContext;
        #endregion

        #region Services
        private IReasonService _reasonService;
        #endregion Services

        #region Constructor
        public ReasonController(ICMDBContext CMDBContext, IUserService userService, IReasonService reasonService) : base(null, null, userService)
        {
            this.CMDBContext = CMDBContext;
            this._reasonService = reasonService;
        }
        #endregion Constructor

        [Route("list")]
        [HttpGet]
        [CancelToken]
        public List<ReasonBO> List()
        {
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
            return _reasonService.List(searchFields);
        }
    }
}
