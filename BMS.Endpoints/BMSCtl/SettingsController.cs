﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Entities;
using BMS.CMDB.Service.Services;
using BMS.Core.Controllers;
using BMS.Security.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BMS.Endpoints.API.BMSCtl
{
    [RoutePrefix("bms/settings")]
    public class SettingsController : BaseApiController
    {

        #region DB contexts
        private ICMDBContext CMDBContext;
        #endregion

        #region Services
        private ISettingsService _settingsService;
        #endregion Services

        #region Constructor
        public SettingsController(ICMDBContext CMDBContext, IUserService userService, ISettingsService settingsService) : base(null, null, userService)
        {
            this.CMDBContext = CMDBContext;
            this._settingsService = settingsService;
        }
        #endregion Constructor

        [Route("save")]
        [HttpPost]
        public Settings Save(Settings settings)
        {
            return _settingsService.Save(settings);
        }


        [Route("list")]
        [HttpGet]
        public List<SettingsBO> List()
        {
            return _settingsService.List();
        }
    }
}
