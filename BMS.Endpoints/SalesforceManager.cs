﻿using BMS.Core.Utilities;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
namespace BMS.Endpoints.API
{
    public class SalesforceManager
    {
        private const string SalesforceUrl = "https://final-bestdeal.cs13.force.com/CustomerPortal/";
        //NMMoneyBranchMortgageLeadVPod
        //NMMoneyBranchMortgageLeadPhone
        public static void AddFormEntry(SalesforceModel model, string sFMethod)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["SalesforceUrl"].ToString() + sFMethod) ;
           
            var postData = "lead_id=" + model.LeadID;
            postData += "&forename=" + model.Forename;
            postData += "&surname=" + model.Surname;
            postData += "&email=" + model.Email;
            postData += "&telephone_number=" + model.TelephoneNumber;
            postData += "&campaign=" + model.Branch;
            postData += "&preferred_day=" + model.PreferredDay;
            postData += "&preferred_time=" + model.PreferredTime;
            postData += "&session_id=" + model.Address;
            postData += "&mortgage_purpose=" + model.Reasonforenquiry.ToString().Replace('_', ' ');
            postData += "&tracking_id=" + model.RequestFromPhoneApp;
            postData += "&referrer=" + model.Referrer;
            postData += "&BranchLocation=" + model.BranchLocation;
            postData += "&comments=" + model.Comments;

            var data = Encoding.ASCII.GetBytes(postData);

            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.ContentLength = data.Length;
            
            using (var stream = httpWebRequest.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            bool responseResult = false;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                responseResult = GetResult(streamReader.ReadToEnd());
            }

            if (responseResult)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Success Message in  Occured at-- " + DateTime.Now + " : " + postData.ToString());
                }
                // Log.Write("Successfully submitted form entry on: " + DateTime.Now, ConfigurationPolicy.Trace);
            }
            else
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + postData.ToString());
                }
                // Log.Write("Failed to submit entry!", ConfigurationPolicy.Trace);
            }
        }

        // <summary>
        /// Gets result of Salesforce request.
        /// </summary>
        /// <param name="resultXml">The result XML.</param>
        /// <returns></returns>
        private static bool GetResult(string resultXml)
        {
            //Handle result
            // “OK-xxxxxxxxxxxxx” (where xxxx is the Lead ID) if the lead was successfully processed or “FAILED – “ followed by an error message.
            bool result = false;

            if (resultXml.Contains("OK"))
            {
                result = true;
            }
            else if (resultXml.Contains("FAILED"))
            {
                result = false;
            }

            return result;
        }
    }
}