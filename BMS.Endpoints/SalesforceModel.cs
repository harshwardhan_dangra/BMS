﻿using System;

namespace BMS.Endpoints.API
{
    public class SalesforceModel
    {
        public string LeadID { get; set; }
        public int MortgageAmount { get; set; }
        public int IncomeAmount { get; set; }
        public int PropertyValue { get; set; }
        public string PreferredDay { get; set; }
        public string PreferredTime { get; set; }
        public string Reasonforenquiry { get; set; }
        public int Term { get; set; }
        public string Title { get; set; } // optional
        public string Forename { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; } //optional
        public bool JointApplication { get; set; }
        public string PartnerTitle { get; set; } // optional
        public string PartnerForename { get; set; } //optional
        public string PartnerSurname { get; set; } //optional
        public DateTime PartnerDateOfBirth { get; set; } //optional
        public string HouseNameNumber { get; set; } //optional
        public string Postcode { get; set; }
        public string Email { get; set; }
        public string TelephoneNumber { get; set; }
        public string MobileNumber { get; set; } //optional
        public string Branch { get; set; }
        public string Address { get; set; }
        public string RequestFromPhoneApp { get; set; }
        public string Referrer { get; set; }
        public string Comments { get; set; }
        public string BranchLocation { get; set; }
    }


    public enum MortgagePurposeEnum
    {
        Mortgage,
        Remortgage,
        Buy_To_Let,
        First_Time_Buyer,
        Moving_Home,
        Call_Back
    }
}