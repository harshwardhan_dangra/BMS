﻿using BMS.CMDB.Service.Data;
using BMS.Core.Controllers;
using BMS.Security.Service.Services;
using System.Web.Http;
using BMS.Service.Services;

namespace BMS.Endpoints.API.Controllers.ApplicationCtl
{

    [RoutePrefix("common")]
    public partial class CommonController : BaseApiController
    {

        #region DB contexts
        private ICMDBContext CMDBContext;
        #endregion


        #region Services
        private IAlHudaWanNoorService AlHudaWanNoorService;
        #endregion Services


        #region Constructor

        public CommonController(ICMDBContext CMDBContext, IAlHudaWanNoorService alHudaWanNoorService, IUserService userService)
            : base(null, null, userService)
        {

            this.CMDBContext = CMDBContext;
            this.AlHudaWanNoorService = alHudaWanNoorService;

        }

        #endregion Constructor


    }

}