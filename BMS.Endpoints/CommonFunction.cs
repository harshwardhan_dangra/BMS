﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace BMS.Endpoints.API
{
    public static class CommonFunction
    {

        public static void SendMail(string mailTo, string mailFrom, string mailFromName, int Port, string Host, bool EnableSsl, int timeout, string sendermail, string senderpassword, string Subject, string Body, string PickupDirectoryLocation, bool IsBodyHtml = false)
        {
            //SmtpClient client = new SmtpClient();
            //client.Port = Port;
            //client.Host = Host;
            //client.EnableSsl = EnableSsl;
            //client.Timeout = timeout;
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //client.UseDefaultCredentials = false;
            //client.Credentials = new System.Net.NetworkCredential(sendermail, senderpassword);

            //MailMessage mm = new MailMessage(mailFrom, mailTo);
            //mm.Body = Body;
            //mm.Subject = Subject;
            //mm.IsBodyHtml = IsBodyHtml;
            //mm.BodyEncoding = UTF8Encoding.UTF8;

            //client.Send(mm);

            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
            string FromEmail = mailFrom;
            mailMessage.From = new MailAddress(FromEmail, mailFromName);
            mailMessage.To.Add(mailTo);
            //mailMessage.Bcc.Add("manish@eurochange.co.uk");
            mailMessage.Subject = Subject;
            mailMessage.Body = Body;
            mailMessage.IsBodyHtml = IsBodyHtml;

            try
            {
                SmtpClient smtp = new SmtpClient();
                string ServerName = (HttpContext.Current.Request.ServerVariables["Server_Name"]).ToString().ToLower();

                if (!string.IsNullOrEmpty(Host))
                {
                    smtp.Port = Port;
                    smtp.Host = Host;
                    smtp.EnableSsl = EnableSsl;
                    smtp.Timeout = timeout;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential(sendermail, senderpassword);
                }
                else
                {
                    smtp.PickupDirectoryLocation = PickupDirectoryLocation;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                }

                smtp.Timeout = 15000;
                smtp.Send(mailMessage);

                mailMessage.Dispose();
            }
            catch (Exception ex)
            {
            }
        }
    }
}