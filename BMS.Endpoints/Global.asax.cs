﻿using System.Web.Http;
using Microsoft.Practices.Unity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Web.Mvc;
using BMS.Core.Service.Services;
using BMS.Security.Service.Handlers;
using BMS.Security.Service.Filters;
using BMS.Security.Service.Data;
using BMS.Security.Service.Services;
using System.Web.Http.Cors;
using BMS.Common.Data;
using BMS.Common.Services;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Services;

namespace BMS.Endpoints.API
{
    public class WebApiApplication : BaseWebApiApplication
    {

        protected override void SetHttpControllerTypeResolver(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute(origins: "*", headers: "*", methods: "*");
            config.EnableCors(cors);
            //config.Services.Replace(typeof(IHttpControllerTypeResolver), new CustomHttpControllerTypeResolver(new[] { "Contract" }));
        }


        protected override void RegisterServices(IUnityContainer container)
        {
            var database = new DatabaseProviderFactory().CreateDefault();
            //var Database2 = new DatabaseProviderFactory().Create("Connection2");

            container.RegisterType<ISecurityDbContext, SecurityDbContext>(new InjectionConstructor(database));

            container.RegisterType<ICMDBContext, CMDBContext>(new InjectionConstructor(database));
            container.RegisterType<ICommonDbContext, CommonDbContext>(new InjectionConstructor(database));

            container.RegisterType<IUserService, UserService>();
            container.RegisterType<ICommonService, CommonService>();
            container.RegisterType<ITranslationService, TranslationService>();
            container.RegisterType<IBookingService, BookingService>();
            container.RegisterType<IReasonService, ReasonService>();
            container.RegisterType<IBranchService, BranchService>();
            container.RegisterType<IAppointmentService, AppointmentService>();
            container.RegisterType<ISettingsService, SettingsService>();
            container.RegisterType<IUserRoleService, UserRoleService>();

        }


        protected override void RegisterFilters(GlobalFilterCollection filters)
        {
            base.RegisterFilters(filters);
        }


        protected override void Configure(HttpConfiguration config)
        {
            //config.Filters.Add(new ValidateModelAttribute());
            config.Filters.Add(new TokenAttribute());
            config.MessageHandlers.Add(new ResponseHandler());
            config.MessageHandlers.Add(new HTTPSGuard());
            base.Configure(config);
        }

    }
}