﻿using BMS.CMDB.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.BusinessObjects
{
    [DataContract]
    public class AppointmentBO : Appointment
    {
        [DataMember(Name = "AppointmentAvailable")]
        public bool AppointmentAvailable { get; set; }

        //[DataMember(Name = "Status")]
        //public bool Status { get; set; }

        [DataMember(Name = "BookedAppointment")]
        public Int64 BookedAppointment { get; set; }

        [DataMember(Name = "SortOrder")]
        public Int64 SortOrder { get; set; }

        [DataMember(Name = "AppointmentTimeString")]
        public String AppointmentTimeString { get; set; }

        [DataMember(Name = "Branch")]
        public string Branch { get; set; }
        
    }

    [DataContract]
    public class AppointmentDatesBI
    {
        [DataMember(Name = "ApIndex")]
        public int ApIndex { get; set; }

        [DataMember(Name = "ApDates")]
        public AppointmentBO ApDates { get; set; }

        [DataMember(Name = "ApTimes")]
        public List<AppointmentBO> ApTimes { get; set; }

    }


}
