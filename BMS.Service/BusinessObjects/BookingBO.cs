﻿using BMS.CMDB.Service.Entities;
using System.Runtime.Serialization;

namespace BMS.CMDB.Service.BusinessObjects
{
    [DataContract]
    public class BookingBO : Booking
    {
        [DataMember(Name = "Reasonforenquiry")]
        public string Reasonforenquiry { get; set; }

        [DataMember(Name = "Branch")]
        public string Branch { get; set; }

        [DataMember(Name = "AssignedUser")]
        public string AssignedUser { get; set; }
    }
}
