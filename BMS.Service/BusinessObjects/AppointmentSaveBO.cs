﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.BusinessObjects
{
    public class AppointmentSaveBO
    {
        public Int64 BranchId { get; set; }
        public Int64 Type { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public List<Slot> Slots { get; set; }
    }

    public class Slot
    {
        public DateTime? Time { get; set; }
        public Int64 Count { get; set; }
        public Boolean IsAvailabe { get; set; }
    }

}
