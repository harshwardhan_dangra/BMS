﻿using BMS.Core.Entities;
using System;
using System.Runtime.Serialization;

namespace BMS.CMDB.Service.Entities
{
    [DataContract]
    public class Booking : BaseEntity
    {
        [DataMember(Name = "Id")]
        public Int64 Id { get; set; }

        [DataMember(Name = "FirstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "LastName")]
        public string LastName { get; set; }

        [DataMember(Name = "Latitude")]
        public string Latitude { get; set; }

        [DataMember(Name = "BookingType")]
        public Int64 BookingType { get; set; }

        [DataMember(Name = "Longitude")]
        public string Longitude { get; set; }

        [DataMember(Name = "Location")]
        public string Location { get; set; }

        [DataMember(Name = "ContactNumber")]
        public string ContactNumber { get; set; }

        [DataMember(Name = "EmailAddress")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "ReasonforenquiryId")]
        public Int64 ReasonforenquiryId { get; set; }

        [DataMember(Name = "BranchId")]
        public Int64 BranchId { get; set; }

        [DataMember(Name = "BookingDateandTime")]
        public DateTime? BookingDateandTime { get; set; }

        [DataMember(Name = "Comment")]
        public string Comments { get; set; }

        [DataMember(Name = "AssignedTo")]
        public Int64 AssignedTo { get; set; }

        [DataMember(Name = "AccessCode")]
        public string AccessCode { get; set; }

        [DataMember(Name = "Referrer")]
        public string Referrer { get; set; }

        [DataMember(Name = "BranchLocation")]
        public string BranchLocation { get; set; }

        [DataMember(Name = "AccessCodeChanged", IsRequired = false)]
        public bool AccessCodeChanged { get; set; } = true;


        public Booking() : base("Id")
        {
        }
    }
}
