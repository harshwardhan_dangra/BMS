﻿using BMS.Core.Entities;
using System;
using System.Runtime.Serialization;

namespace BMS.CMDB.Service.Entities
{
    [DataContract]
    public class Branch : BaseEntity
    {

        [DataMember(Name = "Id")]
        public Int64 Id { get; set; }

        [DataMember(Name = "Name")]
        public string Name { get; set; }

        [DataMember(Name = "Address")]
        public string Address { get; set; }

        [DataMember(Name = "VideoCapacity")]
        public Int64 VideoCapacity { get; set; }

        [DataMember(Name = "BookingNotes")]
        public string BookingNotes { get; set; }

        [DataMember(Name = "MondayOpeningHours")]
        public string MondayOpeningHours { get; set; }

        [DataMember(Name = "MondayClosingHours")]
        public string MondayClosingHours { get; set; }

        [DataMember(Name = "TuesdayOpeningHours")]
        public string TuesdayOpeningHours { get; set; }

        [DataMember(Name = "TuesdayClosingHours")]
        public string TuesdayClosingHours { get; set; }

        [DataMember(Name = "WednesdayOpeningHours")]
        public string WednesdayOpeningHours { get; set; }

        [DataMember(Name = "WednesdayClosingHours")]
        public string WednesdayClosingHours { get; set; }

        [DataMember(Name = "ThursdayOpeningHours")]
        public string ThursdayOpeningHours { get; set; }

        [DataMember(Name = "ThursdayClosingHours")]
        public string ThursdayClosingHours { get; set; }

        [DataMember(Name = "FridayOpeningHours")]
        public string FridayOpeningHours { get; set; }

        [DataMember(Name = "FridayClosingHours")]
        public string FridayClosingHours { get; set; }

        [DataMember(Name = "SaturdayOpeningHours")]
        public string SaturdayOpeningHours { get; set; }

        [DataMember(Name = "SaturdayClosingHours")]
        public string SaturdayClosingHours { get; set; }

        [DataMember(Name = "SundayOpeningHours")]
        public string SundayOpeningHours { get; set; }

        [DataMember(Name = "SundayClosingHours")]   
        public string SundayClosingHours { get; set; }

        public Branch() : base("Id")
        {
        }
    }
}
