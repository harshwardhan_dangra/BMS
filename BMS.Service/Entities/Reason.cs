﻿using BMS.Core.Entities;
using System;
using System.Runtime.Serialization;

namespace BMS.CMDB.Service.Entities
{
    [DataContract]
    public class Reason : BaseEntity
    {
        [DataMember(Name = "Id")]
        public Int64 Id { get; set; }

        [DataMember(Name = "Name")]
        public string Name { get; set; }

        public Reason() : base("Id")
        {
        }
    }
}
