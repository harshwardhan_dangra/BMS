﻿using BMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.Entities
{
    [DataContract]
    public class Settings : BaseEntity
    {

        [DataMember(Name = "Id")]
        public Int64 Id { get; set; }

        [DataMember(Name = "SettingKey")]
        public string SettingKey { get; set; }

        [DataMember(Name = "SettingValue")]
        public string SettingValue { get; set; }

        public Settings() : base("Id")
        {
        }
    }
}
