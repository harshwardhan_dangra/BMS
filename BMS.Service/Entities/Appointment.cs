﻿using BMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.Entities
{
    [DataContract]
    public class Appointment : BaseEntity
    {
        [DataMember(Name = "Id")]
        public Int64 Id { get; set; }

        [DataMember(Name = "AppointmentDate")]
        public DateTime? AppointmentDate { get; set; }

        [DataMember(Name = "AppointmentTime")]
        public DateTime? AppointmentTime { get; set; }

        [DataMember(Name = "AppointmentDay")]
        public string AppointmentDay { get; set; }

        [DataMember(Name = "AppointmentType")]
        public Int64 AppointmentType { get; set; }

        [DataMember(Name = "OpeningTimesStart")]
        public DateTime? OpeningTimesStart { get; set; }

        [DataMember(Name = "OpeningTimesEnd")]
        public DateTime? OpeningTimesEnd { get; set; }

        [DataMember(Name = "TotalCapacity")]
        public Int64 TotalCapacity { get; set; }

        [DataMember(Name = "BranchId")]
        public Int64 BranchId { get; set; }

        public Appointment() : base("Id")
        {
        }
    }
}
