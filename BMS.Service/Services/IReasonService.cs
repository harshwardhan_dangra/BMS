﻿using System.Collections.Generic;
using BMS.CMDB.Service.BusinessObjects;

namespace BMS.CMDB.Service.Services
{
    public interface IReasonService
    {
        List<ReasonBO> List(Dictionary<string, string> searchFields);
    }
}
