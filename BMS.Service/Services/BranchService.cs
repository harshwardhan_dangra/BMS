﻿using System.Collections.Generic;
using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Entities;

namespace BMS.CMDB.Service.Services
{
    public class BranchService : IBranchService
    {
        #region Init

        public ICMDBContext CMDBContext;

        public BranchService(ICMDBContext CMDBContext)
        {

            this.CMDBContext = CMDBContext;

        }

        public bool Delete(long id)
        {
            return CMDBContext.BranchRepository.Delete(id);
        }

        #endregion Init
        public List<BranchBO> List(Dictionary<string, string> searchFields)
        {
            return CMDBContext.BranchRepository.List(searchFields);
        }

        public Branch Save(Branch branch)
        {
            return CMDBContext.BranchRepository.Add(branch);
        }

        public Branch GetBranchById(long id)
        {
            return CMDBContext.BranchRepository.GetById(id);
        }

        public int AllBranchTotalCapacity(Dictionary<string, string> searchFields)
        {
            return CMDBContext.BranchRepository.AllBranchTotalCapacity(searchFields);
        }

        public bool Archive(long id)
        {
            return CMDBContext.BranchRepository.Archive(id);
        }

        public bool UnArchive(long id)
        {
            return CMDBContext.BranchRepository.UnArchive(id);
        }

        public int HeadBranchCapacity()
        {
            return CMDBContext.BranchRepository.HeadBranchCapacity();
        }
    }
}
