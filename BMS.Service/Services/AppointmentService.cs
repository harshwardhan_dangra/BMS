﻿using System;
using System.Collections.Generic;
using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Entities;

namespace BMS.CMDB.Service.Services
{
    public class AppointmentService : IAppointmentService
    {
        #region Init

        public ICMDBContext CMDBContext;

        public AppointmentService(ICMDBContext CMDBContext)
        {

            this.CMDBContext = CMDBContext;

        }

        public List<AppointmentBO> Allappointmenttimeslotslist(Dictionary<string, string> searchFields)
        {
            return CMDBContext.AppointmentRepository.Allappointmenttimeslotslist(searchFields);
        }

        #endregion Init
        public List<AppointmentBO> GetAllList(Dictionary<string, string> searchFields)
        {
            return CMDBContext.AppointmentRepository.GetAllList(searchFields);
        }
        public List<AppointmentBO> List(Dictionary<string, string> searchFields)
        {
            return CMDBContext.AppointmentRepository.List(searchFields);
        }

        public bool Save(AppointmentSaveBO appointmentSaveBO)
        {
            try
            {
                List<Int64> addedappointmentlist = new List<Int64>();
                appointmentSaveBO.Startdate = appointmentSaveBO.Startdate.HasValue ? appointmentSaveBO.Startdate.Value.AddDays(1) : appointmentSaveBO.Startdate;
                appointmentSaveBO.Enddate = appointmentSaveBO.Enddate.HasValue ? appointmentSaveBO.Enddate.Value.AddDays(1) : appointmentSaveBO.Enddate;
                for (DateTime date = appointmentSaveBO.Startdate.Value; date.Date <= (appointmentSaveBO.Enddate.HasValue ? appointmentSaveBO.Enddate.Value.Date : appointmentSaveBO.Startdate.Value.Date); date = date.AddDays(1))
                {
                    CMDBContext.AppointmentRepository.DeleteByDate(date, appointmentSaveBO.Type, appointmentSaveBO.BranchId);
                    foreach (Slot slot in appointmentSaveBO.Slots)
                    {
                        var appointment = new Appointment
                        {
                            AppointmentDate = date,
                            AppointmentTime = slot.Time,
                            AppointmentDay = date.DayOfWeek.ToString(),
                            AppointmentType = appointmentSaveBO.Type,
                            TotalCapacity = slot.Count,
                            BranchId = appointmentSaveBO.BranchId,
                            Status = slot.IsAvailabe ? 1 : 0
                        };
                        var addedappointment = CMDBContext.AppointmentRepository.Add(appointment);
                        addedappointmentlist.Add(addedappointment.Id);
                    }
                }
                if (addedappointmentlist.Count != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<AppointmentBO> TimeSlotsList(Dictionary<string, string> searchFields)
        {
            return CMDBContext.AppointmentRepository.TimeSlotsList(searchFields);
        }
    }
}
