﻿using System.Collections.Generic;
using BMS.CMDB.Service.Entities;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.BusinessObjects;

namespace BMS.CMDB.Service.Services
{
    public class BookingService : IBookingService
    {
        #region Init

        public ICMDBContext CMDBContext;

        public BookingService(ICMDBContext CMDBContext)
        {

            this.CMDBContext = CMDBContext;

        }

        #endregion Init
        public Booking Save(Booking booking)
        {
            return CMDBContext.BookingRepository.Add(booking);
        }

        public bool Delete(long id)
        {
            return CMDBContext.BookingRepository.Delete(id);
        }
        public Booking GetBookingById(long id)
        {
            return CMDBContext.BookingRepository.GetById(id);
        }

        public List<BookingBO> List(Dictionary<string, string> searchFields)
        {
            return CMDBContext.BookingRepository.List(searchFields);
        }

        public BookingBO GetBookingBOById(long id)
        {
            return CMDBContext.BookingRepository.GetBookingBOById(id);
        }

        public bool Archive(long id)
        {
            return CMDBContext.BookingRepository.Archive(id);
        }

        public bool UnArchive(long id)
        {
            return CMDBContext.BookingRepository.UnArchive(id);
        }
    }
}
