﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.Services
{
    public interface ISettingsService
    {
        Settings Save(Settings settings);
        List<SettingsBO> List();
    }
}
