﻿using System.Collections.Generic;
using BMS.CMDB.Service.BusinessObjects;

namespace BMS.CMDB.Service.Services
{
    public interface IAppointmentService
    {
        List<AppointmentBO> GetAllList(Dictionary<string, string> searchFields);
        List<AppointmentBO> List(Dictionary<string, string> searchFields);
        bool Save(AppointmentSaveBO appointmentSaveBO);
        List<AppointmentBO> TimeSlotsList(Dictionary<string, string> searchFields);
        List<AppointmentBO> Allappointmenttimeslotslist(Dictionary<string, string> searchFields);
    }
}
