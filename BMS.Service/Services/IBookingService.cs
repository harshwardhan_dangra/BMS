﻿using System.Collections.Generic;
using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;

namespace BMS.CMDB.Service.Services
{
    public interface IBookingService
    {
        Booking Save(Booking booking);
        bool Delete(long id);
        List<BookingBO> List(Dictionary<string, string> searchFields);

        Booking GetBookingById(long id);
        BookingBO GetBookingBOById(long id);
        bool Archive(long id);
        bool UnArchive(long id);
    }
}
