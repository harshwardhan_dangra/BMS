﻿using System.Collections.Generic;
using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;

namespace BMS.CMDB.Service.Services
{
    public interface IBranchService
    {
        List<BranchBO> List(Dictionary<string, string> searchFields);
        Branch Save(Branch branch);
        bool Delete(long id);
        Branch GetBranchById(long id);
        int AllBranchTotalCapacity(Dictionary<string, string> searchFields);
        bool Archive(long id);
        bool UnArchive(long id);
        int HeadBranchCapacity();
    }
}
