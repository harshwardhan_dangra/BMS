﻿using System.Collections.Generic;
using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;

namespace BMS.CMDB.Service.Services
{
    public class ReasonService : IReasonService
    {

        #region Init

        public ICMDBContext CMDBContext;

        public ReasonService(ICMDBContext CMDBContext)
        {

            this.CMDBContext = CMDBContext;

        }

        #endregion Init

        public List<ReasonBO> List(Dictionary<string, string> searchFields)
        {
            return CMDBContext.ReasonRepository.List(searchFields);
        }
    }
}
