﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Data;
using BMS.CMDB.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.Services
{
    public class SettingsService : ISettingsService
    {
        #region Init

        public ICMDBContext CMDBContext;

        public SettingsService(ICMDBContext CMDBContext)
        {

            this.CMDBContext = CMDBContext;

        }

        public List<SettingsBO> List()
        {
            return CMDBContext.SettingsRepository.List();
        }

        public Settings Save(Settings settings)
        {
            return CMDBContext.SettingsRepository.Add(settings);
        }

        #endregion Init
    }
}
