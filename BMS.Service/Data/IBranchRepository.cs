﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using System.Collections.Generic;

namespace BMS.CMDB.Service.Data
{
    public interface IBranchRepository : IRepository<Branch>
    {
        List<BranchBO> List(Dictionary<string, string> searchFields);
        int AllBranchTotalCapacity(Dictionary<string, string> searchFields);
        bool Archive(long id);
        bool UnArchive(long id);
        int HeadBranchCapacity();
    }
}
