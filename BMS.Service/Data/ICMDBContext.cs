﻿using BMS.Core.Data;

namespace BMS.CMDB.Service.Data
{
    public interface ICMDBContext : IDbContext
    {
        IBookingRepository BookingRepository { get; }
        IReasonRepository ReasonRepository { get; }
        IBranchRepository BranchRepository { get; }
        IAppointmentRepository AppointmentRepository { get; }
        ISettingsRepository SettingsRepository { get; }
    }
}
