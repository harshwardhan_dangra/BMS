﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using BMS.Core.Utilities;
using BMS.Core.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace BMS.CMDB.Service.Data
{
    public class BranchRepository : BaseRepository<Branch>, IBranchRepository
    {
        private const string GetByIdStoredProcName = "Branch_SelectById";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "Branch_UpSert";
        private const string DeleteStoredProcName = "Branch_DeleteById";

        protected override string GetByIdStoredProcedureName { get { return BranchRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return BranchRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return BranchRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return BranchRepository.DeleteStoredProcName; } }

        public BranchRepository(IDbContext dbContext)
           : base(dbContext)
        {

        }

        public List<BranchBO> List(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<BranchBO> branchBOList = new List<BranchBO>();
            BranchBO branch;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Branch_Specific_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {

                            branch = new BranchBO();
                            DataReaderExtensions.FillEntity<BranchBO>(reader, branch);
                            branchBOList.Add(branch);
                        }
                    }
                }

            }

            return branchBOList;
        }

        public int AllBranchTotalCapacity(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            int capacity = 0;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("AllBranchTotalCapacity", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            capacity = Convert.ToInt32(reader["AppointmentAvailable"]);
                        }
                    }
                }

            }

            return capacity;
        }

        public bool Archive(long id)
        {
            var rowsAffected = 0;
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Branch_Archive_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.BigInt;

                    rowsAffected = database.ExecuteNonQuery(sqlCmd);
                }

            }
            return rowsAffected > 0;
        }

        public bool UnArchive(long id)
        {
            var rowsAffected = 0;
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Branch_UnArchive_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.BigInt;

                    rowsAffected = database.ExecuteNonQuery(sqlCmd);
                }

            }
            return rowsAffected > 0;
        }

        public int HeadBranchCapacity()
        {
            int TotalCapacity = 0;
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Branch_HeadBranch_Capacity", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            TotalCapacity = Convert.ToInt32(reader["VideoCapacity"]);
                        }
                    }
                }

            }
            return TotalCapacity;
        }
    }
}
