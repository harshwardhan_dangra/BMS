﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.Data
{
    public interface ISettingsRepository : IRepository<Settings>
    {
        List<SettingsBO> List();
    }
}
