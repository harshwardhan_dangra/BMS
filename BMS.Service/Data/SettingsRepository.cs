﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using BMS.Core.Utilities;
using BMS.Core.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.Data
{
    public class SettingsRepository : BaseRepository<Settings>, ISettingsRepository
    {
        private const string GetByIdStoredProcName = "Settings_SelectById";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "Settings_UpSert";
        private const string DeleteStoredProcName = "Settings_DeleteById";

        protected override string GetByIdStoredProcedureName { get { return SettingsRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return SettingsRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return SettingsRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return SettingsRepository.DeleteStoredProcName; } }

        public SettingsRepository(IDbContext dbContext)
           : base(dbContext)
        {

        }

        public List<SettingsBO> List()
        {
             List<SettingsBO> settingsBOList = new List<SettingsBO>();
            SettingsBO settingsBO;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Settings_Specific_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                
                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {

                            settingsBO = new SettingsBO();
                            DataReaderExtensions.FillEntity<SettingsBO>(reader, settingsBO);
                            settingsBOList.Add(settingsBO);
                        }
                    }
                }

            }

            return settingsBOList;
        }
    }
}
