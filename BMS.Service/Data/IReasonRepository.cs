﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using System.Collections.Generic;

namespace BMS.CMDB.Service.Data
{
    public interface IReasonRepository : IRepository<Reason>
    {
        List<ReasonBO> List(Dictionary<string, string> searchFields);
    }
}
