﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using BMS.Core.Utilities;
using BMS.Core.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.Data
{
    public class AppointmentRepository : BaseRepository<Appointment>, IAppointmentRepository
    {
        private const string GetByIdStoredProcName = "Appointment_SelectById";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "Appointment_UpSert";
        private const string DeleteStoredProcName = "Appointment_DeleteById";

        protected override string GetByIdStoredProcedureName { get { return AppointmentRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return AppointmentRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return AppointmentRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return AppointmentRepository.DeleteStoredProcName; } }

        public AppointmentRepository(IDbContext dbContext)
           : base(dbContext)
        {

        }

        public List<AppointmentBO> GetAllList(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<AppointmentBO> appointmentBOList = new List<AppointmentBO>();
            AppointmentBO appointment;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Appointment_All_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {

                            appointment = new AppointmentBO();
                            DataReaderExtensions.FillEntity<AppointmentBO>(reader, appointment);
                            appointmentBOList.Add(appointment);
                        }
                    }
                }

            }

            return appointmentBOList;
        }

        public List<AppointmentBO> List(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<AppointmentBO> appointmentBOList = new List<AppointmentBO>();
            AppointmentBO appointment;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Appointment_Specific_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {

                            appointment = new AppointmentBO();
                            DataReaderExtensions.FillEntity<AppointmentBO>(reader, appointment);
                            appointmentBOList.Add(appointment);
                        }
                    }
                }

            }

            return appointmentBOList;
        }

        public List<AppointmentBO> TimeSlotsList(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<AppointmentBO> appointmentBOList = new List<AppointmentBO>();
            AppointmentBO appointment;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Appointment_TimeSlots_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {

                            appointment = new AppointmentBO();
                            DataReaderExtensions.FillEntity<AppointmentBO>(reader, appointment);
                            appointmentBOList.Add(appointment);
                        }
                    }
                }

            }

            return appointmentBOList;
        }

        public void DeleteByDate(DateTime date, long type, long branchId)
        {
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Appointment_DeleteByDate", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@AppointmentDate", date); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Date;

                    SqlParameter tvpParam1 = sqlCmd.Parameters.AddWithValue("@AppointmentType", type); //Needed TVP
                    tvpParam1.SqlDbType = SqlDbType.BigInt;

                    SqlParameter tvpParam2 = sqlCmd.Parameters.AddWithValue("@BranchId", branchId); //Needed TVP
                    tvpParam2.SqlDbType = SqlDbType.BigInt;

                    database.ExecuteNonQuery(sqlCmd);

                }

            }
        }

        public List<AppointmentBO> Allappointmenttimeslotslist(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<AppointmentBO> appointmentBOList = new List<AppointmentBO>();
            AppointmentBO appointment;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Appointment_AllTimeSlots_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {

                            appointment = new AppointmentBO();
                            DataReaderExtensions.FillEntity<AppointmentBO>(reader, appointment);
                            appointmentBOList.Add(appointment);
                        }
                    }
                }

            }

            return appointmentBOList;
        }
    }
}
