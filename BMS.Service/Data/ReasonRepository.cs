﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using BMS.Core.Utilities;
using BMS.Core.Utilities.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace BMS.CMDB.Service.Data
{
    public class ReasonRepository : BaseRepository<Reason>, IReasonRepository
    {
        private const string GetByIdStoredProcName = "Reason_SelectById";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "Reason_UpSert";
        private const string DeleteStoredProcName = "Reason_DeleteById";
        protected override string GetByIdStoredProcedureName { get { return ReasonRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return ReasonRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return ReasonRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return ReasonRepository.DeleteStoredProcName; } }

        public ReasonRepository(IDbContext dbContext)
           : base(dbContext)
        {

        }

        public List<ReasonBO> List(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<ReasonBO> reasonBOList = new List<ReasonBO>();
            ReasonBO reason;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Reason_Specific_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {

                            reason = new ReasonBO();
                            DataReaderExtensions.FillEntity<ReasonBO>(reader, reason);
                            reasonBOList.Add(reason);
                        }
                    }
                }

            }

            return reasonBOList;
        }
    }
}
