﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using BMS.Core.Utilities;
using BMS.Core.Utilities.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace BMS.CMDB.Service.Data
{
    public class BookingRepository : BaseRepository<Booking>, IBookingRepository
    {
        private const string GetByIdStoredProcName = "Booking_SelectById";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "Booking_UpSert";
        private const string DeleteStoredProcName = "Booking_DeleteById";
        protected override string GetByIdStoredProcedureName { get { return BookingRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return BookingRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return BookingRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return BookingRepository.DeleteStoredProcName; } }

        public BookingRepository(IDbContext dbContext)
           : base(dbContext)
        {

        }

        public List<BookingBO> List(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<BookingBO> bookingBOList = new List<BookingBO>();
            BookingBO booking;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Booking_Specific_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {

                            booking = new BookingBO();
                            DataReaderExtensions.FillEntity<BookingBO>(reader, booking);
                            bookingBOList.Add(booking);
                        }
                    }
                }

            }

            return bookingBOList;
        }

        public BookingBO GetBookingBOById(long id)
        {
            BookingBO booking = new BookingBO();

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Booking_Search_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.BigInt;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            DataReaderExtensions.FillEntity<BookingBO>(reader, booking);
                        }
                    }
                }

            }
            return booking;
        }

        public bool Archive(long id)
        {
            var rowsAffected = 0;
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Booking_Archive_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.BigInt;

                    rowsAffected = database.ExecuteNonQuery(sqlCmd);
                }

            }
            return rowsAffected > 0;
        }

        public bool UnArchive(long id)
        {
            var rowsAffected = 0;
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("Booking_UnArchive_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.BigInt;

                    rowsAffected = database.ExecuteNonQuery(sqlCmd);
                }

            }
            return rowsAffected > 0;
        }
    }
}
