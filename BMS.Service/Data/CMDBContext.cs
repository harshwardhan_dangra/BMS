﻿using BMS.Core.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;

namespace BMS.CMDB.Service.Data
{
    public class CMDBContext : BaseDbContext, ICMDBContext, IDisposable
    {



        public CMDBContext(Database database)
            : base(database)
        {

        }


        private bool disposed = false;

        public IBookingRepository BookingRepository { get { return new BookingRepository(this); } }

        public IReasonRepository ReasonRepository { get { return new ReasonRepository(this); } }

        public IBranchRepository BranchRepository { get { return new BranchRepository(this); } }

        public IAppointmentRepository AppointmentRepository { get { return new AppointmentRepository(this); } }

        public ISettingsRepository SettingsRepository { get { return new SettingsRepository(this); } }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //Free any other managed objects here. 
                this.Dispose();
            }

            // Free any unmanaged objects here. 
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



    }
}
