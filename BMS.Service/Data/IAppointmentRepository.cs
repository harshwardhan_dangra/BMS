﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.CMDB.Service.Data
{
    public interface IAppointmentRepository : IRepository<Appointment>
    {
        List<AppointmentBO> GetAllList(Dictionary<string, string> searchFields);
        List<AppointmentBO> List(Dictionary<string, string> searchFields);
        List<AppointmentBO> TimeSlotsList(Dictionary<string, string> searchFields);
        void DeleteByDate(DateTime date, long type, long branchId);
        List<AppointmentBO> Allappointmenttimeslotslist(Dictionary<string, string> searchFields);
    }
}
