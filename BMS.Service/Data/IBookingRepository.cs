﻿using BMS.CMDB.Service.BusinessObjects;
using BMS.CMDB.Service.Entities;
using BMS.Core.Data;
using System.Collections.Generic;

namespace BMS.CMDB.Service.Data
{
    public interface IBookingRepository : IRepository<Booking>
    {
        List<BookingBO> List(Dictionary<string, string> searchFields);
        BookingBO GetBookingBOById(long id);
        bool Archive(long id);
        bool UnArchive(long id);
    }
}
