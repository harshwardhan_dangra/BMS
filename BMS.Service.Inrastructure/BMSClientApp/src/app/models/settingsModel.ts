export class Settings {
    Id: number;
    SettingKey: string;
    SettingValue:string;

    constructor(id: number, settingKey: string,SettingValue:string) {
        this.Id = id;
        this.SettingKey = settingKey;
        this.SettingValue = SettingValue;
    }
}