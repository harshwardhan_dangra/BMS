export class BookingList {
    Id: number;
    FirstName:string;
    LastName:string;
    ContactNumber: String;
    EmailAddress: String;
    ReasonforenquiryId: number;
    BranchId: number;
    BookingDateandTime:Date;   
    Reason: String;
    Branch: String;
    CreatedBy:number;
    Reasonforenquiry:String;
    BookingType:number;
    Comment:string;
    AssignedTo:number;
    AssignedUser:string;
AccessCode:string;
Longitude:string;
Latitude:string;
Location:string;
Referrer:string;
}