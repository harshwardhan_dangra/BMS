export class Branch {
    Id: number;
    Name: string;
    VideoCapacity: number;
    Address:string;
    BookingNotes:string;
    MondayOpeningHours:string;
    MondayClosingHours:string;
    TuesdayOpeningHours:string;
    TuesdayClosingHours:string;
    WednesdayOpeningHours:string;
    WednesdayClosingHours:string;
    ThursdayOpeningHours:string;
    ThursdayClosingHours:string;
    FridayOpeningHours:string;
    FridayClosingHours:string;
    SaturdayOpeningHours:string;
    SaturdayClosingHours:string;
    SundayOpeningHours:string;
    SundayClosingHours:string;

    constructor(id: number, name: string,address:string, videocapacity: number,BookingNotes:string,
        MondayOpeningHours:string,
        MondayClosingHours:string,
        TuesdayOpeningHours:string,
        TuesdayClosingHours:string,
        WednesdayOpeningHours:string,
        WednesdayClosingHours:string,
        ThursdayOpeningHours:string,
        ThursdayClosingHours:string,
        FridayOpeningHours:string,
        FridayClosingHours:string,
        SaturdayOpeningHours:string,
        SaturdayClosingHours:string,
        SundayOpeningHours:string,
        SundayClosingHours:string,
    ) {
        this.Id = id;
        this.Name = name;
        this.Address = address;
        this.VideoCapacity = videocapacity;
        this.BookingNotes = BookingNotes;
        this.MondayOpeningHours = MondayOpeningHours;
        this.MondayClosingHours = MondayClosingHours;
        this.TuesdayOpeningHours = TuesdayOpeningHours;
        this.TuesdayClosingHours= TuesdayClosingHours;
        this.WednesdayOpeningHours = WednesdayOpeningHours;
        this.WednesdayClosingHours = WednesdayClosingHours;
        this.ThursdayOpeningHours = ThursdayOpeningHours;
        this.ThursdayClosingHours = ThursdayClosingHours;
        this.FridayOpeningHours = FridayOpeningHours;
        this.FridayClosingHours = FridayClosingHours;
        this.SaturdayOpeningHours = SaturdayOpeningHours;
        this.SaturdayClosingHours = SaturdayClosingHours;
        this.SundayOpeningHours = SundayOpeningHours;
        this.SundayClosingHours = SundayClosingHours;
    }
}