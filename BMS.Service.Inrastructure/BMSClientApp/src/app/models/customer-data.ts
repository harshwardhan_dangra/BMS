export class AppUser {
    UserId: String;
    userType: String;
    Token: String;
    FirstName: String;
    LastName: String;
    Password: String;
    Email: String;
    message: String;
    RoleId: string;
    constructor(userid: string, email: string, password: string, FirstName: string, LastName: string, userType: string, RoleId: string) {
        this.UserId = userid;
        this.Email = email;
        this.Password = password;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.userType = userType;
        this.RoleId= RoleId;
    }
}


export class Booking {
    Id: Number;
    FirstName: String;
    LastName: String;
    ContactNumber: String;
    EmailAddress: String;
    ReasonforenquiryId: number;
    BranchId: number;
    BookingDateandTime: Date;
    BookingType:number;
    Comment:string;
    AssignedTo:number;
    Longitude:string;
    Latitude:string;
    Location:string;
    AccessCodeChanged:boolean;
    Referrer:string;
    
AccessCode:string;
    constructor(Id: number, FirstName: string,LastName:string, ContactNumber: string, EmailAddress: string, 
        ReasonforenquiryId: number, BranchId: number,bookingdatetime:Date,BooingType:number,Comment:string,AsTo:number,accessCode:string,
        Longitude:string,
        Latitude:string,
        Location:string,
        AccessCodeChanged:boolean,Referrer:string) {
        this.Id = Id;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.ContactNumber = ContactNumber;
        this.EmailAddress = EmailAddress;
        this.ReasonforenquiryId = ReasonforenquiryId;
        this.BranchId = BranchId;
        this.BookingDateandTime =  bookingdatetime;
        this.BookingType = BooingType;
        this.Comment = Comment;        
        this.AssignedTo=AsTo;
        this.AccessCode = accessCode;
        this.Location = Location;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
        this.AccessCodeChanged =AccessCodeChanged;
        this.Referrer = Referrer;
    }
}

