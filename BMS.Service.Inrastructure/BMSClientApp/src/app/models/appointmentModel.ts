export class BranchAppointment{
    AppointmentAvailable:number;
    BookedAppointment:number;
    SortOrder:number;
    AppointmentTimeString:string;
    TotalCapacity:number;
    Status:boolean;
  isActive: any;
    constructor(AppointmentAvailable:number,BookedAppointment: number, SortOrder: number, AppointmentTimeString: string,TotalCapacity:number, Status: boolean) {
        this.AppointmentAvailable = AppointmentAvailable;
        this.BookedAppointment = BookedAppointment;
        this.SortOrder = SortOrder;
        this.AppointmentTimeString = AppointmentTimeString;
        this.TotalCapacity = TotalCapacity;
        this.Status = Status;
    }
}







export class AppointmentSlotsModel {
    Type: number;
    Startdate: Date;
    Enddate: Date;
    Slots: any;
    BranchId:number;
    constructor(branchId:number,editDay: number, startDate: Date, endDate: Date, timeslots: any) {
        this.Type = editDay;
        this.Startdate = startDate;
        this.Enddate = endDate;
        this.Slots = timeslots;
        this.BranchId = branchId;
    }
}


export  class   timeSetting{
id:number;
time:string;
sortorder:number

constructor(sid:number,stime:string,psortorder:number){
this.id=sid;
this.time = stime;
this.sortorder=psortorder;
}
}



export class AppointmentModel {

    AppointmentAvailable: boolean;
    BookedAppointment: number;
    Id: number;
    AppointmentDate: Date;
    AppointmentTime: Date;
    AppointmentDay: string;
    AppointmentType: number;
    OpeningTimesStart: Date;
    OpeningTimesEnd: Date;
    TotalCapacity: number;
    BranchId: number;

}


export class ApDateTimeModel {
    ApIndex: number;
    ApDates: AppointmentModel;
    ApTimes: AppointmentModel[];
}


export class AppointmentDateModel {
    AppointmentDate: Date;
    AppointmentDateNumber: number;
    AppointmentDay: string;
    AppointmentMonth: string;
    AppointmentFullDate: Date;
    AppointmentMonthNumber:string;
    AppointmentYear: number;


    constructor(appointmentDate: Date) {
        this.AppointmentDate = new Date(appointmentDate);
        this.AppointmentMonth = this.getMonthNameFromDate();
        this.AppointmentDay = this.getDayNameFromDate();
        this.AppointmentDateNumber = this.getDayFromDate();
        this.AppointmentYear = this.getYearFromDate();
        this.AppointmentMonthNumber = this.AppointmentDate.getMonth()>0&& this.AppointmentDate.getMonth()<10?"0"+ (this.AppointmentDate.getMonth()+1).toString():this.AppointmentDate.getMonth().toString();
    }

    getDayNameFromDate() {
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        return days[(this.AppointmentDate.getDay())];
    }

    getMonthNameFromDate(): string {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        return monthNames[this.AppointmentDate.getMonth()];
    }

    getDayFromDate(): number {
        return (this.AppointmentDate.getDate());
    }

    getYearFromDate(): number {
        return (this.AppointmentDate.getFullYear());
    }
}