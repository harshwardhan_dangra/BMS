import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from '../../services/account/login-service.service';
import { Router } from '@angular/router';
import { AppUser } from '../../models/customer-data';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('empForm') empForm: NgForm;
  appUser: AppUser = null;
  Email:string;
  Password:string;
  IsLoggedIn = false;
  errorMessagedisplay: boolean;
  errorMessage: String;
  constructor(private _accountService: LoginService, private router: Router) { }

  ngOnInit() {
    //this.CheckAlreadyLoggedIn();
  }
  CheckAlreadyLoggedIn(): any {
    this._accountService.CheckLoggedIn().subscribe(res=>{
      if(res!=null && res!=undefined){
        this.IsLoggedIn = false;  
         this.router.navigate(['/admin/dashboard']);
      }
    });
  }
  ClickLogin() {    
    if (this.empForm.valid) {   
      this._accountService.userLoginPost(new AppUser('',this.empForm.value.Email, this.empForm.value.Password,'','','','')).subscribe(res => {
        console.log(res);
        this.appUser = res;    
        if (this.appUser != null && this.appUser.UserId >= '0') {          
          this._accountService.SaveUserToSession(this.appUser);
         this.IsLoggedIn = false;  
         this.router.navigate(['/admin/dashboard']);
        } else {
          this.errorMessagedisplay = true;
          this.errorMessage=this.appUser.message;
         this.IsLoggedIn = true;
        }
      });
    }
  }

}
