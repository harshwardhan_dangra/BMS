import { Component, OnInit } from '@angular/core';
import { BookingService } from '../../services/booking/booking.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BranchService } from '../../services/miscservices/branch.service';
import { BookingList } from '../../models/bookingModel';
@Component({
  selector: 'app-viewbooking',
  templateUrl: './viewbooking.component.html',
  styleUrls: ['./viewbooking.component.css'],
  providers:[BookingService]
})
export class ViewbookingComponent implements OnInit {
 
  FirstName: String;
  LastName: String;
  ContactNumber: String;
  EmailAddress: String;
  Reason: String;
  Branch: String;
  BookingDateandTime: Date;
  actionName: string = '';
  BooingType: number;
  Comment:string;
  AssignedTo:string;
  AccessCode:string;
  updateBranchId: number;
  customerToUpdate: BookingList;
  bookingType: number;

  constructor(private _bookingService: BookingService,
    private route: ActivatedRoute) {

       var paramId = this.route.snapshot.params["id"];
    this.bookingType = this.route.snapshot.params["type"];

    this.updateBranchId = paramId;
    
    this.FillCustomerForm(paramId);
  }
  ngOnInit() {
    
  }

  
  FillCustomerForm(custId: number) {
    this._bookingService.getBookingByID(custId).subscribe(res => {
      this.customerToUpdate = res;
      if (this.customerToUpdate != null && this.customerToUpdate.Id > 0) {

        this.FirstName = this.customerToUpdate.FirstName;
        this.LastName = this.customerToUpdate.LastName;
        this.ContactNumber = this.customerToUpdate.ContactNumber;
        this.Comment = this.customerToUpdate.Comment;
        this.EmailAddress = this.customerToUpdate.EmailAddress;
        this.Reason = this.customerToUpdate.Reasonforenquiry;
        this.Branch = this.customerToUpdate.Branch;
        this.BookingDateandTime = this.customerToUpdate.BookingDateandTime;
        this.BooingType = this.customerToUpdate.BookingType;
        this.AssignedTo = this.customerToUpdate.AssignedUser;
        this.AccessCode = this.customerToUpdate.AccessCode;
        
      }
    });

  }

}
