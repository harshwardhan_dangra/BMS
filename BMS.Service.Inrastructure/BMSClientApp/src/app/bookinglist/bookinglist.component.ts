import { Component, OnInit } from '@angular/core';
import { BookingService } from '../services/booking/booking.service';
import { BookingList } from '../models/bookingModel';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Router, ActivatedRoute } from '@angular/router';
import { BranchService } from '../services/miscservices/branch.service';
import { Branch } from '../models/branch.model';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { UUID } from 'angular2-uuid';
import { CommonService } from '../services/common/common.service';
@Component({
  selector: 'app-bookinglist',
  templateUrl: './bookinglist.component.html',
  styleUrls: ['./bookinglist.component.css'],
  providers:[BookingService,BranchService]
})
export class BookinglistComponent implements OnInit {

  booklist: BookingList[] = null;
  returnedbooklist: BookingList[] = null;
  title: String = '';
  bookingType: number ;
  status:number;  
  branch: Branch=null;  
  branchId:number= 0;
  branchList: Branch[];
  bookingStartDate:string = '';
  bookingEndDate:string = '';
  filename: string;
  searchText:string;
  bsValue = Date;
  RoleId: string;
  constructor(private _bookingservice: BookingService,
    private route: ActivatedRoute,private router: Router,
    private _branchservice: BranchService, private _commonService:CommonService,) {

      var paramId = this.route.snapshot.params["type"];
      var status = this.route.snapshot.params["status"];  
      var branchId = this.route.snapshot.params["branchId"];      
      this.branchId = branchId!=undefined? branchId:0;
      this.bookingType = paramId;
      if (paramId==2) {
        this.title = 'Video Appointments';
      } 
      if (paramId==1) { 
        this.title = 'Call Back Appointments';
      }
      if(status == "0")
      {
        this.status = status;
        if (paramId==2) {
          this.title = 'Archived Video Appointments';
        } 
        if (paramId==1) { 
          this.title = 'Archived Call Back Appointments';
        }
      }else{
        this.status = 1;
      }
  }

  ngOnInit() {
        this.loadBookingList();        
        this.getBranchList();
        if(this.branchId!=null&&this.branchId!=0)
        {
          this.getBranchById(this.branchId);
        }

        this.getuserRoleId();
  }
  getuserRoleId()
  {
    this.RoleId = this._commonService.getuserRoleId();
    }

  getBranchList(): any {
   this._branchservice.getBranchList().subscribe(res=>{
this.branchList = res;
   });
  }


  loadBookingListByBranch(event):void{
    this.branchId = event.target.value;
    this.loadBookingList();
  }
  loadBookingList(){
    var bookingSearch = {bookingType:this.bookingType,status:this.status,
      branchId:this.branchId,
      bookingStartDate:this.bookingStartDate!=null && this.bookingStartDate != "NaN-NaN-NaN"?this.bookingStartDate:'',
      bookingEndDate:this.bookingEndDate!=null && this.bookingEndDate !=  "NaN-NaN-NaN"?this.bookingEndDate:''};
    this._bookingservice.getBookingList(bookingSearch).subscribe(res => {
      this.booklist = res;
      this.returnedbooklist = this.booklist.slice(0, 10);      
    });
  }

  loadBookingListByDate(event):void{
    if(event!=null)
    {var startDate = new Date(event[0]);
    var endDate = new Date(event[1]);
    var startDateString = startDate.getFullYear()+'-'+ (startDate.getMonth()+1)+'-'+startDate.getDate();
    var endDateString = endDate.getFullYear()+'-'+ (endDate.getMonth()+1)+'-'+endDate.getDate();
    this.bookingStartDate = startDateString;
    this.bookingEndDate = endDateString;}
    this.loadBookingList();
  }

  getBranchById(branchId:number){
    this._branchservice.getBranchByID(branchId).subscribe(res=>{
      this.branch = res;
      if (this.branch != null && this.branch.Id > 0) {        
        this.title = this.branch.Name + ' Appointments';    
      }
    });
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedbooklist = this.booklist.slice(startItem, endItem);
  }

isDeleteSuccess:boolean;
  DeleteCustomer(id: number) {
    if (confirm("Are you sure to delete this record?")) {
      this._bookingservice.deleteBooking(id)
        .subscribe(res => {
          this.isDeleteSuccess = res;
          if (this.isDeleteSuccess) {
            this.loadBookingList();
            alert("Deleted Successfully");
            
          }
        })
    }
  }

  isArchiveSuccess:boolean;
  ArchiveCustomer(id: number) {
    if (confirm("Are you sure to archive this record?")) {
      this._bookingservice.archiveBooking(id)
        .subscribe(res => {
          this.isArchiveSuccess = res;
          if (this.isArchiveSuccess) {
            this.loadBookingList();
            alert("Archived Successfully");
            
          }
        })
    }
  }

  UnArchiveCustomer(id: number) {
    if (confirm("Are you sure to unarchive this record?")) {
      this._bookingservice.unarchiveBooking(id)
        .subscribe(res => {
          this.isArchiveSuccess = res;
          if (this.isArchiveSuccess) {
            this.loadBookingList();
            alert("UnArchived Successfully");
            
          }
        })
    }
  }

  ExportBooking(){
    var options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: false, 
      showTitle: false,
      useBom: false,
      noDownload: false,
      headers: ["Id", "Date", "Time","Branch","Customer Name","Mortgage Type","Assigned","Access Code"]
    };
    var returnedbooklist = [];
    this.returnedbooklist.forEach(element=>{
      var fDate = new Date(element.BookingDateandTime);
      var bookingdate= fDate.getDate() + '/'+(fDate.getMonth()+1) + '/'+fDate.getFullYear();
      var bookingTime = fDate.getHours() +':'+fDate.getMinutes()
returnedbooklist.push({
Id:element.Id==null?'':element.Id,
Date:element.BookingDateandTime==null?'':bookingdate,
Time:element.BookingDateandTime==null?'':bookingTime,
Branch:element.Branch==null?'':element.Branch,
CustomerName:element.FirstName==null?'':element.FirstName + ' '+ element.LastName==null?'':element.LastName,
MortgageTyoe:element.Reasonforenquiry==null?'':element.Reasonforenquiry,
Assigned:element.AssignedUser==null?'':element.AssignedUser,
AccessCode:element.AccessCode==null?'':element.AccessCode
});
    });
    if (this.bookingType==2) {
      this.filename = 'Video Appointments' + UUID.UUID();
    } 
    if (this.bookingType==1) { 
      this.filename = 'Call Back Appointments' + UUID.UUID();
    }
    if(this.status == 0)
    {
      if (this.bookingType==2) {
        this.filename = 'Archived Video Appointments' + UUID.UUID();
      } 
      if (this.bookingType==1) { 
        this.filename = 'Archived Call Back Appointments' + UUID.UUID();
      }
    
  }
  if(this.branchId!=0){
    this.filename = this.branch.Name + ' Appointments' + UUID.UUID();
  }
  new Angular5Csv(returnedbooklist, this.filename, options);
}

}
