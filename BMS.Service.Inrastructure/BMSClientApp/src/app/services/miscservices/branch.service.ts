import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from '../common/common.service';

import { Branch } from '../../models/branch.model';

@Injectable()
export class BranchService {
  
  
  constructor(private _commonService:CommonService) {
  }

  
  getArchiveBranchList(model:any): Observable<Branch[]> {
    return this._commonService.get('bms/branch/list?'+this._commonService.buildQueryString(model));
  }


  getBranchList():Observable<Branch[]>{
    return this._commonService.get('bms/branch/list');
  }

  createBranch(branch: Branch): Observable<Branch> {    
    return this._commonService.post('bms/branch/save', branch);      
  }

  getBranchByID(id:number):Observable<Branch>{
    return this._commonService.get('bms/branch/getbyId/'+id);
  }

  deleteBranch(id: number): Observable<boolean> {    
    return this._commonService.delete('bms/branch/delete/'+ id);                  
  }

  validateBranchTotalCapacity(model:any):Observable<number>{
    return this._commonService.get('bms/branch/allBranchTotalCapacity?'+this._commonService.buildQueryString(model));
  }

  archiveBranch(id: number): any {
    return this._commonService.get('bms/branch/archive/'+ id);                  
  }  

  unarchiveBranch(id: number): any {
    return this._commonService.get('bms/branch/unarchive/'+ id);                  
  }
  getHeadBranchCapacity(): any {
    return this._commonService.get('bms/branch/headbranchcapacity/');                  
  }

}
