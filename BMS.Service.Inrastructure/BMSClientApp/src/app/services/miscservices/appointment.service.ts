import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from '../common/common.service';
import { AppointmentModel, ApDateTimeModel, AppointmentSlotsModel, BranchAppointment } from '../../models/appointmentModel';

@Injectable()
export class AppointmentService {  

  constructor(private _commonService:CommonService) {
  }
  getAppointmentList(model:any):Observable<ApDateTimeModel[]>{
    return this._commonService.get('bms/appointment/appointmentlist?'+this._commonService.buildQueryString(model));
  }  
  getadminAppointmentList(model:any):Observable<AppointmentModel[]>{
    return this._commonService.get('bms/appointment/allList?'+this._commonService.buildQueryString(model));
  }  


  createAppointmentSlots(_slots: AppointmentSlotsModel): Observable<boolean> {        
    return this._commonService.post('bms/appointment/save', _slots);      
  }

  getbranchAppointmentList(model:any):Observable<BranchAppointment[]>{
    return this._commonService.get('bms/appointment/appointmenttimeslotslist?'+this._commonService.buildQueryString(model));
  } 
  
  getAllTimeSlots(model:any):Observable<BranchAppointment[]>{
    return this._commonService.get('bms/appointment/allappointmenttimeslotslist?'+this._commonService.buildQueryString(model));
  } 
  
}
