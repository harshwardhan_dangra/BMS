import { Injectable } from "@angular/core";
import { Settings } from "../../models/settingsModel";
import { Observable } from "rxjs";
import { Http } from "@angular/http";
import { CommonService } from "../common/common.service";

@Injectable()
export class SettingsService {
  
    
  public _settings: Settings = null; 
    constructor(private _http: Http,private _commonService:CommonService) {
    }
  AddSettings(_settings: Settings): Observable<Settings> {
    return this._commonService.post('bms/settings/save', _settings);      
  }  
  getSettings(): Observable<Settings[]> {
    return this._commonService.get('bms/settings/list');      
  }

}