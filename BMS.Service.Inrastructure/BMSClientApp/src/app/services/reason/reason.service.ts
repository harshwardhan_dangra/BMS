import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from '../common/common.service';
import { Reason } from '../../models/reasonModel';

@Injectable()
export class ReasonService {
  public _booking: Reason = null; 
  constructor(private _commonService:CommonService) {
  }


  getReasonList():Observable<Reason[]>{
    return this._commonService.get('bms/reason/list');
  }
}
