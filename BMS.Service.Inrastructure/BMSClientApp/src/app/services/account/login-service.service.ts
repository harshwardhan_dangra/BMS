import { Injectable, EventEmitter } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { AppUser } from '../../models/customer-data';
import { Observable } from 'rxjs';
import { GlobalConfig } from '../../globalconfig';
import { CommonService } from '../common/common.service';

//import {resolve,reject} from 'q';

@Injectable()
export class LoginService {
  CheckLoggedIn(): any {
    return this._commonService.getToken();
  }

  public _appUser: AppUser = null;
  IsAuthenticated = false;  
  constructor(private _http: Http,private _commonService:CommonService) {
  }

  createUser(appuser: AppUser): Observable<number> {    
    return this._commonService.post('user', appuser)
      .pipe(map((response: any) => response.json()));
  }


  userLoginPost(user: AppUser): Observable<AppUser> {
    return this._commonService.post('login',user);
    //return this._http.post(this.baseUrl + 'login', user).pipe(map((res: Response) => <AppUser>res.json()));
  }


  SaveUserToSession(aU: AppUser) {
    localStorage.setItem('loggedInUser', JSON.stringify(aU));
  }
  RemoveUserFromSession() {
    this._appUser = null;
    localStorage.removeItem('loggedInUser');
  }

  IsUserAuthenticated(): boolean {

    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    this._appUser = jsonObject;
    if (this._appUser != null) {
      return true;
    }
    return false;
  }

  getLoggedInUser() {
    return this._appUser;
  }


  getAuthToken():string {
    var user = this.getLoggedInUser();
    if (user != null && user.Token.length > 0) {
      return user.Token as string;
    }
  }


  IsAuthorized() {
    // this.IsAuthenticated = this.IsUserAuthenticated();
    const promise = new Promise(
      (resolve, reject) => {
        resolve(this.IsUserAuthenticated());
      }
    );
    //return this.IsUserAuthenticated()
    return promise;
  }

  getuserRole():string{
    return this._commonService.getuserRole();
  }
}
