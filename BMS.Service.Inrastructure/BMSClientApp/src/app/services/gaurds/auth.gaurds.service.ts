import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { LoginService } from '../account/login-service.service';
import { Injectable } from "@angular/core";
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { CommonService } from "../common/common.service";



@Injectable()
export class AuthGaurd implements CanActivate {
    userRoleId: string;
    ArrayPathValue: any[] = [{ RoleId: 1, Path: 'branches' },
    { RoleId: 1, Path: 'customers' },
    { RoleId: 0, Path: 'bookinglist' },
    { RoleId: 1, Path: 'availability' },
    { RoleId: 0, Path: 'home' },
    { RoleId: 0, Path: 'viewbooking' },
    { RoleId: 1, Path: 'archivebooking' },
    { RoleId: 1, Path: 'branchbookinglist' },
    { RoleId: 0, Path: 'dashboard' },
    { RoleId: 1, Path: 'archivebranches' },
    { RoleId: 1, Path: 'headbranchavailability' },
    { RoleId: 1, Path: 'addbranch' },
    { RoleId: 1, Path: 'viewcustomers' },
    { RoleId: 1, Path: 'addcustomers' },
    { RoleId: 1, Path: 'addavailablity' },
    { RoleId: 1, Path: 'userlist' },
    { RoleId: 1, Path: 'adduser' },
    { RoleId: 0, Path: 'profile' },
    { RoleId: 1, Path: 'userdetail' },
    { RoleId: 1, Path: 'messagesettings' }
    ];


    constructor(private authService: LoginService, private _commonService: CommonService, private route: Router) {
    }
    /**
     *
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<boolean> | Promise<boolean> | boolean {
        return this.authService.IsAuthorized().then(
            (auth: boolean) => {
                if (auth) {
                    this.userRoleId = this._commonService.getuserRoleId();
                    if (this.userRoleId != "1") {
                        this.userRoleId  = "0";
                        var item = this.ArrayPathValue.filter(x => x.RoleId == this.userRoleId && state.url.includes(x.Path))
                        if (item.length != 0) {
                            return true;
                        }
                        else {
                            this.route.navigate(['no-auth']);
                        }
                    } else {
                        return true;
                    }

                    // return true;
                } else {
                    this.route.navigate(['']);
                }
            }
        );
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<boolean> | Promise<boolean> | boolean {
        return this.canActivate(route, state);
    }

}



