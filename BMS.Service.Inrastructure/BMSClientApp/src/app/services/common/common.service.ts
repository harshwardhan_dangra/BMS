import { Injectable, EventEmitter } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { Http, Response, Headers, URLSearchParams, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { Observable, Subscriber } from 'rxjs';
import { GlobalConfig } from '../../globalconfig';
import { AppUser } from '../../models/customer-data';
import { Router } from '@angular/router';

@Injectable()
export class CommonService {
  private baseUrl = GlobalConfig.ApiUrl + '/';
  private accessToken: string = '';

  constructor(private _http: Http,private router: Router) {

  }

  getHeader(): RequestOptions {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      this.accessToken = jsonObject.Token as string;
    }
    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
    cpHeaders.append('X-AuthToken', this.accessToken);
    let options = new RequestOptions({ headers: cpHeaders });
    return options;
  }


  getToken(): string {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      return jsonObject.Token as string;
    }
    return '';
  }

  
  getuserRole(): string {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      return jsonObject.userType as string;
    }
    return '';
  }
  getuserRoleId(): string {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      return jsonObject.RoleId as string;
    }
    return '';
  }
  

  post(url: string, model: any): Observable<any> {
    var response = this._http.post(this.baseUrl + url, model, this.getHeader())
    .pipe(map((response: any) => response.json()),
    catchError(this.handleError('getData')));
    return response;
  }


  get(url: string): Observable<any> {    
    var response = this._http.get(this.baseUrl + url, this.getHeader()).pipe(map(data => <any>data.json()),
    catchError(this.handleError('getData')));  
    return response;
  }

  

  getwithParams(url: string,model:any): Observable<any> {
    var response = this._http.get(this.baseUrl + url, this.getHeader()).pipe(map(data => <any>data.json()),
    catchError(this.handleError('getData')));
    return response;
  }

  delete(url: string): Observable<boolean> {
    var response = this._http.delete(this.baseUrl + url, this.getHeader()).pipe(map((response: Response) => response.json()),
    catchError(this.handleError('getData'))    
    );
    return response;
  }

  buildQueryString = function ($params) {


    var queryParams = [];
  
    for (var k in $params) {
        if ($params.hasOwnProperty(k)) {
            queryParams.push(encodeURIComponent(k) + "=" + encodeURIComponent($params[k]));
            //queryParams.push(k + "=" + $params[k]);
        }
    }
  
    return queryParams.join("&");
  }


  private handleError(operation: String) {
    return (err: any) => {
        let errMsg = `error in`;       
        if(err.status==401){
            this.router.navigate(['/admin']);
        }
        if(err.status==400){
          alert('User name or password is incorrect, please enter valid credentials!');
      }
        return Observable.throw(errMsg);
    }
}
}
