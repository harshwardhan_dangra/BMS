import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { CommonService } from '../common/common.service';
import { Observable } from 'rxjs';
import { UserRole } from '../../models/userrole-data';

//import {resolve,reject} from 'q';

@Injectable()
export class UserRoleService {
  constructor(private _http: Http,private _commonService:CommonService) {
  }


  
  getUserRoles(userRoleSearch:any): Observable<UserRole> {
    return this._commonService.get('bms/userrole/list?'+this._commonService.buildQueryString(userRoleSearch));
  }
}
