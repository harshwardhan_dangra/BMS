import { Injectable, EventEmitter } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { AppUser, Booking } from '../../models/customer-data';
import { Observable } from 'rxjs';
import { CommonService } from '../common/common.service';
import { BookingList } from '../../models/bookingModel';

//import {resolve,reject} from 'q';

@Injectable()
export class BookingService {
 
  public _booking: Booking = null; 
  constructor(private _http: Http,private _commonService:CommonService) {
  }

  createBooking(_booking: Booking): Observable<Booking> {    
    return this._commonService.post('bms/booking/save', _booking);      
  }

  deleteBooking(id: number): Observable<boolean> {    
    return this._commonService.delete('bms/booking/delete/'+ id);                  
  }


  archiveBooking(id: number): Observable<boolean> {    
    return this._commonService.get('bms/booking/archive/'+ id);                  
  }
  unarchiveBooking(id: number): any {
    return this._commonService.get('bms/booking/unarchive/'+ id);                  
  }
  getBookingList(bookingSearch:any):Observable<BookingList[]>{
    return this._commonService.get('bms/booking/list?'+this._commonService.buildQueryString(bookingSearch));
  }

  getBookingByID(id:number):Observable<BookingList>{
    return this._commonService.get('bms/booking/getbyId/'+id);
  }
}
