import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Booking } from '../models/customer-data';
import { BookingService } from '../services/booking/booking.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingList } from '../models/bookingModel';
import { ReasonService } from '../services/reason/reason.service';
import { Reason } from '../models/reasonModel';
import { BranchService } from '../services/miscservices/branch.service';
import { Branch } from '../models/branch.model';
import { AppointmentService } from '../services/miscservices/appointment.service';
import { AppointmentModel, AppointmentDateModel, ApDateTimeModel } from '../models/appointmentModel';
import { UsersService } from '../services/miscservices/users.service';
import { Users } from '../models/usersModel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [BookingService, BranchService, AppointmentService,UsersService]
})
export class HomeComponent implements OnInit {
  @ViewChild('custForm') custForm: NgForm;
  activeSlideIndex:number=4;
  FirstName: String;
  LastName: String;
  ContactNumber: String;
  EmailAddress: String;
  ReasonforenquiryId: number=0;
  BranchId: number=0;
  BookingDateandTime: Date;
  actionName: string = '';
  BooingType: number;
  Comment:string;
  AssignedTo:number=0;
  AccessCode:string;
  AccessCodeChanged:boolean = false;

  appointmentList: ApDateTimeModel[] = null;
  reasonList: Reason[];
  branchList: Branch[];
  customerToUpdate: BookingList = null;
  datesList: AppointmentDateModel[] = null;

  onlyDateList: AppointmentModel[] = [];
  onlyTimeList: AppointmentModel[] = [];
  userList:Users[];

  selectedDateIndex: number;

  bookingType: number;

  isDateSelected: boolean = false;
  showError: boolean;
  updateBranchId: number;
  CurrentBookingDateandTime: Date;
  PreviousSelectedBookingDateandTime: Date;
  PreviousAccessCode: string = "";  
  emailInvalid: boolean;
  contactnumberInvalid: boolean;
  WeekDays:any=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
  Referrer:string;
  constructor(private _bookingService: BookingService,
    private route: ActivatedRoute, private router: Router
    , private _reasonService: ReasonService
    , private _branchService: BranchService
    , private _appointmentService: AppointmentService
    ,private _userService:UsersService) {

    var paramId = this.route.snapshot.params["id"];
    this.bookingType = this.route.snapshot.params["type"];

    this.updateBranchId = paramId;   


  }
  bookingmodel: Booking = null;
  ngOnInit() {
    this.getReasonList();
    this.getBranchList();
      this.getUserList();
      if (this.updateBranchId > 0) {
        this.actionName = 'Update';
        this.FillCustomerForm(this.updateBranchId);
      } else {
        this.actionName = 'Book';
      }
  }

  getUserList(){

    this._userService.getUsersList().subscribe(res=>{

      this.userList = res;      

    });

  }




  getReasonList() {
    this._reasonService.getReasonList().subscribe(res => {
      this.reasonList = res;
    });
  }

  getBranchList() {

    if (this.bookingType != 1) {
      this.custForm.value.BookingDateandTime = null;
      this._branchService.getBranchList().subscribe(res => {
        this.branchList = res;
        //   this.BranchId = 0;
        // this.custForm.value.BranchId = this.BranchId;
        this.SelectAppointmentDates();       
      }
      );
    }else{
      this.BranchId = 0;
      this.custForm.value.BranchId = this.BranchId;      
      this.SelectAppointmentDates();
    
    }

  }



  onSelectDateTime(time: AppointmentModel) {
    if (this.datesList[this.selectedDateIndex] != null) {
      var selectedDate = this.appointmentList[this.selectedDateIndex].ApDates;
      var fDate = this.mergeDateAndTime(selectedDate.AppointmentDate, time.AppointmentTime, );
      this.custForm.value.BookingDateandTime = fDate;
      this.BookingDateandTime =  fDate;
      this.CurrentBookingDateandTime = time.AppointmentTime;
      this.isDateSelected = true;
    }
  }


  mergeDateAndTime(ssDate: Date, ssTime: Date): Date {
    var sDate = new Date(ssDate);
    var sTime = new Date(ssTime)
    var dateStringFromDP = sDate.getFullYear() + '-' + (sDate.getMonth()+1)  + '-' + sDate.getDate();
    var timeStringFromDP = sTime.getHours() + ':' + sTime.getMinutes() + ':00';
    return new Date(dateStringFromDP + ' ' + timeStringFromDP + ' ' + 'GMT');
  }


  SelectAppointmentDates() {
    this.BranchId = this.custForm.value.BranchId;
    var currentDate = new Date();
    var date = currentDate.getDate();
    var month = currentDate.getMonth(); //Be careful! January is 0 not 1
    var year = currentDate.getFullYear();
    var hour = currentDate.getHours();
    var min = currentDate.getMinutes();
    var second = currentDate.getSeconds();
    var milisecond = currentDate.getMilliseconds();
    var dateString =year+  "-" + (month + 1)+ "-" + date  + " " + hour + ":" + min + ":"+ second+ ":"+milisecond;
    var searchParams = { 'branchId': this.custForm.value.BranchId, 'appointmentType': this.bookingType, 'currentDateTime': dateString ,
    'selectedDay': this.WeekDays[currentDate.getDay()]};
    this.datesList = [];
    this.onlyTimeList = [];
    this.onlyDateList = [];
    this._appointmentService.getAppointmentList(searchParams).subscribe(res => {
      this.appointmentList = res;
      this.appointmentList.forEach(element => {
        this.onlyDateList.push(element.ApDates);
      });
        this.datesList = this.getAllDays(this.onlyDateList);  
        if(this.appointmentList.length!=0)
     { this.onlyTimeList = this.appointmentList[0].ApTimes;}
    });
  }

  getAllDays(model: AppointmentModel[]): AppointmentDateModel[] {
    if(model.length!=0)
   { var localList: AppointmentDateModel[] = [];
    var distinct = [];
    this.activeSlideIndex=0;
    model.forEach(element => {
      if (distinct.indexOf(element.AppointmentDate) == -1) {
        distinct.push(element.AppointmentDate);
      }
    });    
    var selectedIndex = 0;
    distinct.forEach((element,index) => {
      var a = new AppointmentDateModel(element);
      localList.push(a);    
      var sourceDate = new Date(this.PreviousSelectedBookingDateandTime);
      var destinationDate = new Date(element);          
      if(sourceDate.getFullYear() == destinationDate.getFullYear() && sourceDate.getDate()==destinationDate.getDate() && sourceDate.getMonth()==destinationDate.getMonth()){                
        selectedIndex = index;
      }
    });    
    
    setTimeout(()=>{
      this.activeSlideIndex = selectedIndex;
    },1000);
    
    return localList;
  }
  }

  FillTheTimes(event: number) {
    this.selectedDateIndex = event;
    if (this.datesList[event] != null) {
      this.onlyTimeList = this.appointmentList[event].ApTimes;
    }
  }
  FillCustomerForm(custId: number) {
    this._bookingService.getBookingByID(custId).subscribe(res => {
      this.customerToUpdate = res;
      if (this.customerToUpdate != null && this.customerToUpdate.Id > 0) {        
        this.BranchId = this.customerToUpdate.BranchId;
        this.FirstName = this.customerToUpdate.FirstName;
        this.LastName = this.customerToUpdate.LastName;
        this.ContactNumber = this.customerToUpdate.ContactNumber;
        this.Comment = this.customerToUpdate.Comment;
        this.EmailAddress = this.customerToUpdate.EmailAddress;
        this.ReasonforenquiryId = this.customerToUpdate.ReasonforenquiryId;        
        this.BookingDateandTime = this.customerToUpdate.BookingDateandTime;
        this.BooingType = this.customerToUpdate.BookingType;
        this.AssignedTo = this.customerToUpdate.AssignedTo;
        this.AccessCode = this.customerToUpdate.AccessCode;
        this.PreviousSelectedBookingDateandTime=  this.customerToUpdate.BookingDateandTime;
        this.PreviousAccessCode = this.customerToUpdate.AccessCode;    
        this.Referrer = this.customerToUpdate.Referrer;
      //   setTimeout(()=>{            
      //     this.activeSlideIndex =5;
      // }, 2000);
       
      }
    });

  }



  BookCustomer() {
if(this.updateBranchId==0 && this.bookingType == 2 )
    {if (!this.isDateSelected) {
      //this.showError = true;
      alert("Please Select Appointment Date and Time");
      return false;
    }
  }
    if (this.custForm.valid) {
      
      this.CreateBooking();
    }
  }

  CreateBooking() {
    var bid: number = 0;
    if(this.bookingType==1){
      this.BookingDateandTime = new Date();
    }
    if (this.actionName == "Update") {
      bid = this.customerToUpdate.Id;
    }
    
if(this.PreviousAccessCode != this.AccessCode){
  this.AccessCodeChanged = true;
}
    var newbooking = new Booking(bid, this.custForm.value.FirstName, this.custForm.value.LastName, this.custForm.value.ContactNumber,
      this.custForm.value.EmailAddress, this.custForm.value.ReasonforenquiryId, this.custForm.value.BranchId,
      this.BookingDateandTime, this.bookingType, this.custForm.value.Comment,this.custForm.value.AssignedTo,this.custForm.value.AccessCode
      ,this.customerToUpdate==null?'':this.customerToUpdate.Longitude
      ,this.customerToUpdate==null?'':this.customerToUpdate.Latitude
      ,this.customerToUpdate==null?'':this.customerToUpdate.Location
      ,this.AccessCodeChanged,this.custForm.value.Referrer  
      );

    this._bookingService.createBooking(newbooking).subscribe(res => {

      this.bookingmodel = res;
      if (this.bookingmodel != null && this.bookingmodel.Id >= 0) {

        alert('Booking is saved successfully');

        this.router.navigate(['/admin/bookinglist/' + this.bookingType]);
        this.bookingmodel = null;
      } else {
      }
    });

    // this._customerservice.createCustomer().subscribe(res => {
    //   this.ServiceResult = res;
    //   if (this.ServiceResult == 201) {
    //     this.router.navigate(['/admin/customer']);
    //   }
    // });
  }

  isNumberKeyy(event) {
    var textInput = event.target.value;
    textInput = textInput.replace(/[^0-9]/g, "");
    event.target.value = textInput;
  }
  paste(){
    return false;
  }
  validateEmail(event){
    var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;    
    this.emailInvalid=emailpattern.test(event.target.value);
  }
  setCustomValidity(event){
    var contactnumber = /[01|02|03|05|07|]{2}[0-9]{9}/;    
    this.contactnumberInvalid=contactnumber.test(event.target.value);
  }
}
