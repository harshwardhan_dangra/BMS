import { Component, OnInit } from '@angular/core';
import { BookingService } from '../services/booking/booking.service';
import { ActivatedRoute } from '@angular/router';
import { BookingList } from '../models/bookingModel';

@Component({
  selector: 'app-thank',
  templateUrl: './thank.component.html',
  styleUrls: ['./thank.component.css'],
  providers:[BookingService]
})
export class ThankComponent implements OnInit {
    customerToUpdate: BookingList;
    Reason: String;
    EmailAddress: String;
    Branch: String;
    BookingDateandTime: Date;

  constructor(private _bookingService: BookingService,
    private route: ActivatedRoute) {

    var paramId = this.route.snapshot.params["id"];
    this.FillCustomerForm(paramId);
   }

  ngOnInit() {
  }
  
  FillCustomerForm(custId: number) {
    this._bookingService.getBookingByID(custId).subscribe(res => {
      this.customerToUpdate = res;
      if (this.customerToUpdate != null && this.customerToUpdate.Id > 0) {

        this.EmailAddress = this.customerToUpdate.EmailAddress;
        this.Reason = this.customerToUpdate.Reasonforenquiry;
        this.Branch = this.customerToUpdate.Branch;
        this.BookingDateandTime = this.customerToUpdate.BookingDateandTime;
       
      }
    });

  }
}
