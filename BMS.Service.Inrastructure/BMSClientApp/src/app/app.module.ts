import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { LoginComponent } from './account/login/login.component';
import { RegistrationComponent } from './account/registration/registration.component';
import { NotAuthorizedComponent } from './not-authorized/not-authorized.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { MainappComponent } from './mainapp/mainapp.component';
import { HomeComponent } from './home/home.component';
import { HttpModule } from '@angular/http';
import { AppRoutingModuel } from './app.routing';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginService } from './services/account/login-service.service';
import { AuthGaurd } from './services/gaurds/auth.gaurds.service';
import { CommonService } from './services/common/common.service';
import { BookinglistComponent } from './bookinglist/bookinglist.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { DlDateTimePickerDateModule } from 'angular-bootstrap-datetimepicker';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AdmindashboardComponent } from './admin/admindashboard/admindashboard.component';
import { BranchesComponent } from './admin/branches/branches.component';
import { CustomersComponent } from './admin/customers/customers.component';
import { AvailabilityComponent } from './admin/availability/availability.component';
import { AddComponent } from './admin/branches/add/add.component';
import { AddcustomersComponent } from './admin/customers/addcustomers/addcustomers.component';
import { AddavailabilityComponent } from './admin/availability/addavailability/addavailability.component';
import { ReasonService } from './services/reason/reason.service';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { UsersComponent } from './admin/users/users.component';
import { AdduserComponent } from './admin/users/adduser/adduser.component';
import { UserdetailComponent } from './admin/users/userdetail/userdetail.component';
import { SettingsComponent } from './admin/settings/settings.component';
import { ViewbookingComponent } from './bookinglist/view/viewbooking.component';
import { ViewCustomersComponent } from './admin/customers/view/viewcustomers.component';
import { GuestUserComponent } from './guestuser/guest.component';
import { GuestBookingComponent } from './guestbooking/guestbooking.component';
import { GuestHeaderComponent } from './guestheader/guestheader.component';
import { ThankComponent } from './thank/thank.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { immediatemailComponent } from './emailtemplates/immediatemail/immediatemail.component';
import { scheduledmailComponent } from './emailtemplates/scheduledmail/scheduledmail.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    NotAuthorizedComponent,
    PagenotfoundComponent,
    MainappComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    BookinglistComponent,
    AdmindashboardComponent,
    BranchesComponent,
    CustomersComponent,
    AvailabilityComponent,
    AddComponent,
    AddcustomersComponent,
    AddavailabilityComponent,
    UsersComponent,
    AdduserComponent,
    UserdetailComponent,
    SettingsComponent,
    ViewbookingComponent,
    ViewCustomersComponent,
    GuestUserComponent,
    GuestBookingComponent,
    GuestHeaderComponent,
    ThankComponent,
    immediatemailComponent,
    scheduledmailComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,    
    AppRoutingModuel,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    DlDateTimePickerDateModule,
    PaginationModule.forRoot(),
    CarouselModule.forRoot(),
    Ng2SearchPipeModule
  ],
  providers: [CommonService,LoginService,AuthGaurd,ReasonService],
  bootstrap: [AppComponent]
})


export class AppModule { }
