import { Component, OnInit } from '@angular/core';
import { AppUser } from '../models/customer-data';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../services/account/login-service.service';
import { CommonService } from '../services/common/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers:[LoginService]
})
export class HeaderComponent implements OnInit {
  UserId: String;
  userrole: string;
  RoleId:string;

  constructor(private route: ActivatedRoute, private router: Router, private _loginservice:LoginService,private _commonService:CommonService) { }

  ngOnInit() {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      this.UserId = jsonObject.UserId;
    }
    this.getuserRole();
    
    this.getuserRoleId();
  }
  
  getuserRoleId()
  {
    this.RoleId = this._commonService.getuserRoleId();
    }
  getuserRole() {
    this.userrole = this._loginservice.getuserRole();
   
  }
  Logout(){
    localStorage.removeItem('loggedInUser');
    this.router.navigate(['/admin']);
  }

}
