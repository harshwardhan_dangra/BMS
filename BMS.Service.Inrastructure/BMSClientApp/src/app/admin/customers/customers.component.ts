import { Component, OnInit } from '@angular/core';
import { BookingList } from '../../models/bookingModel';
import { BookingService } from '../../services/booking/booking.service';
export interface PageChangedEvent {
  itemsPerPage: number;
  page: number;
}
@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css'],
  providers:[BookingService]
})
export class CustomersComponent implements OnInit {
  booklist: BookingList[] = null;
  returnedbooklist: BookingList[] = null;
  constructor(private _bookingservice: BookingService) { }
  searchText:string;
  
  ngOnInit() {
    this.loadBookingList();
}


loadBookingList(){
var bookingSearch = {'status':1}
this._bookingservice.getBookingList(bookingSearch).subscribe(res => {
  this.booklist = res;
  this.returnedbooklist = this.booklist.slice(0, 10);
});
}
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedbooklist = this.booklist.slice(startItem, endItem);
  }
 
isDeleteSuccess:boolean;
DeleteCustomer(id: number) {
  if (confirm("Are you sure to delete this record?")) {
    this._bookingservice.deleteBooking(id)
      .subscribe(res => {
        this.isDeleteSuccess = res;
        if (this.isDeleteSuccess) {
          this.loadBookingList();
          alert("Deleted Successfully");
          
        }
      })
  }
}
}
