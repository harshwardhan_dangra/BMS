import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent implements OnInit {
  RoleId: any;
  constructor(private _commonService:CommonService, private router: Router) { }

  ngOnInit() {

    this.getuserRoleId();
  }

  getuserRoleId()
  {
    this.RoleId = this._commonService.getuserRoleId();
    }
}
