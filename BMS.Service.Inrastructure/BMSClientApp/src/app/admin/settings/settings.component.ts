import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Settings } from '../../models/settingsModel';
import { SettingsService } from '../../services/miscservices/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
  providers:[SettingsService]
})
export class SettingsComponent implements OnInit {
  @ViewChild('custForm') custForm: NgForm;
  smsSettings:string;
  SendSMSRemider:string;
  emailSettings:string;
  SendEmailRemider:string;
  success:boolean;
  settingslist:Settings;
  constructor(private _settingsService:SettingsService) { }

  ngOnInit() {
      this.FillSettings();
  }

  FillSettings(){
this._settingsService.getSettings().subscribe(res=>{
  this.smsSettings=res.find(x=>x.SettingKey=="smsSettings").SettingValue;
  this.SendSMSRemider=res.find(x=>x.SettingKey=="SendSMSRemider").SettingValue;
  this.emailSettings=res.find(x=>x.SettingKey=="emailSettings").SettingValue;
  this.SendEmailRemider=res.find(x=>x.SettingKey=="SendEmailRemider").SettingValue;
});
  }

  AddSettings(){
        this.CreateSettings();   
  }
  CreateSettings(){
    var settingsarr = [[{'id':0},{'settingKey':'smsSettings'},{'SettingValue':this.custForm.value.smsSettings}],
    [{'id':0},{'settingKey':'SendSMSRemider'},{'SettingValue':this.custForm.value.SendSMSRemider}],
    [{'id':0},{'settingKey':'emailSettings'},{'SettingValue':this.custForm.value.emailSettings}],
    [{'id':0},{'settingKey':'SendEmailRemider'},{'SettingValue':this.custForm.value.SendEmailRemider}]
    ]

    settingsarr.forEach(element=>{
        var settings = new Settings(element[0].id,element[1].settingKey,element[2].SettingValue);
      this._settingsService.AddSettings(settings).subscribe(res => {
        if(res.Id!=0){
this.success = true;
        }
      });
    });
    if(this.success==true){
      alert("successful");
    }
  }
}
