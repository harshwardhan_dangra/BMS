import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/miscservices/users.service';
import { Users } from '../../models/usersModel';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers:[UsersService]
})
export class UsersComponent implements OnInit {


  userlist: Users[];
  returneduserlist:Users[];
  constructor(private _userService:UsersService) { }

  ngOnInit() {
    this.loadUsers();
  }


  loadUsers(){    
    this._userService.getUsersList().subscribe(res => {
      this.userlist = res;     
      this.returneduserlist = this.userlist.slice(0, 10);
    });
  }

  isDeleteSuccess:boolean;
  DeleteUsers(id: number) {
    if (confirm("Are you sure to delete this record?")) {
      this._userService.deleteUser(id)
        .subscribe(res => {
          this.isDeleteSuccess = res;
          if (this.isDeleteSuccess) {
            this.loadUsers();
            alert("Deleted Successfully");
            
          }
        })
    }
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returneduserlist = this.userlist.slice(startItem, endItem);
  }
}
