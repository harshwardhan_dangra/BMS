import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UsersService } from '../../../services/miscservices/users.service';
import { Users } from '../../../models/usersModel';
import { UserRoleService } from '../../../services/userrole/userrole-service.service';
import { UserRole } from '../../../models/userrole-data';
import { LoginService } from '../../../services/account/login-service.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css'],
  providers:[UsersService,UserRoleService,LoginService]
})
export class AdduserComponent implements OnInit {

  @ViewChild('custForm') custForm: NgForm;
  actionName: string = '';
  showPasswordSetting:boolean=false;
  updateparamId:number=0;

  pUser : Users;


  UserId: number;
  RoleId: number;
  firstName: string;
  lastName: string;
  email: string;
  Password: string;
  userRoleList:UserRole;
  ContactNumber: string;
  UserRole: number = 0;
  redirectType:number;
  ConfirmPassword:string;
  userrole: string;
  constructor( private _userService:UsersService, private route: ActivatedRoute,private router: Router,
    private _userRoleService:UserRoleService, private _loginservice:LoginService) { 

    var paramId = this.route.snapshot.params["id"];
    var type = this.route.snapshot.params["type"];
    this.redirectType = type;
    if (paramId > 0) {
      this.updateparamId = paramId;
      this.actionName = 'Update';
      this.FillCustomerForm(paramId);
    } else {
      this.showPasswordSetting=true;
      this.actionName = 'Save';
    }

  }




  ngOnInit() {
    this.getUserRoleList();
    this.getUserRole();
  }
  getUserRole(): any {
    this.userrole =this._loginservice.getuserRole();
  }

  getUserRoleList(){
    var userRoleSearch = {}
    this._userRoleService.getUserRoles(userRoleSearch).subscribe(res=>{
      this.userRoleList = res;
    });
  }
  FillCustomerForm(custId: number) {
    this._userService.getUserByID(custId).subscribe(res => {
      this.pUser = res;
      if (this.pUser != null && this.pUser.UserId > 0) {        
        this.firstName = this.pUser.firstName;
        this.lastName = this.pUser.lastName;
        this.email = this.pUser.email;      
        this.ContactNumber = this.pUser.ContactNumber;   
        this.UserRole = this.pUser.RoleId;      
      }
    });

  }


  AddUsers (){
    if (this.custForm.valid) {
      if(this.custForm.value.Password == this.custForm.value.ConfirmPassword)
      {
        this.CreateUser();
      }else{
        this.custForm.value.ConfirmPassword.error=false;
      }
    }
  }



  CreateUser() {
    var bid: number = 0;
    if (this.actionName == "Update") {
      bid = this.pUser.UserId;
    }    
    var newUser = new Users(bid, 
      this.custForm.value.UserRole!=null && this.custForm.value.UserRole!=undefined ?this.custForm.value.UserRole:this.UserRole,
      this.custForm.value.firstName,
      this.custForm.value.lastName,
      this.custForm.value.email,
      this.custForm.value.Password,
      this.custForm.value.ContactNumber);
    this._userService.createUser(newUser).subscribe(res => {
      this.pUser = res;
      if (this.pUser != null && this.pUser.UserId >= 0) {
        alert('successful');
        if(this.redirectType!=0){
          this.router.navigate(['/admin/userlist']);
          this.pUser = null;
        }
      } 
      if (this.pUser != null && this.pUser.UserId == -100) {
        alert(this.pUser.firstName);
      } 
    });
  }
}
