import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, PreloadingStrategy } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AppointmentSlotsModel, timeSetting } from '../../../models/appointmentModel';
import { AppointmentService } from '../../../services/miscservices/appointment.service';
import { BranchService } from '../../../services/miscservices/branch.service';
import { Branch } from '../../../models/branch.model';
import { PreviousRouteService } from '../../../services/miscservices/previousroute.service';

@Component({
  selector: 'app-addavailability',
  templateUrl: './addavailability.component.html',
  styleUrls: ['./addavailability.component.css'],
  providers: [AppointmentService, BranchService, PreviousRouteService]
})
export class AddavailabilityComponent implements OnInit {
  @ViewChild('custForm') custForm: NgForm;
  bookingType: number;
  actionName: string;
  returnedappointmenttimelist: any;
  editdays = 0;
  Name: string;
  times: number[] = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
  newTimeHour: number;
  newTimeMin: number;
  HeadOfficeTotalCapacity: number = 0;
  alltimes: any = [{ id: 1, time: '8:00', sortorder: 1, bookedCapacity: 0, bookingAvailable: true },
  { id: 2, time: '9:00', sortorder: 2, bookedCapacity: 0, bookingAvailable: true },
  { id: 3, time: '10:00', sortorder: 3, bookedCapacity: 0, bookingAvailable: true },
  { id: 4, time: '11:00', sortorder: 4, bookedCapacity: 0, bookingAvailable: true },
  { id: 5, time: '12:00', sortorder: 5, bookedCapacity: 0, bookingAvailable: true },
  { id: 6, time: '13:00', sortorder: 6, bookedCapacity: 0, bookingAvailable: true },
  { id: 7, time: '14:00', sortorder: 7, bookedCapacity: 0, bookingAvailable: true },
  { id: 8, time: '15:00', sortorder: 8, bookedCapacity: 0, bookingAvailable: true },
  { id: 9, time: '16:00', sortorder: 9, bookedCapacity: 0, bookingAvailable: true },
  { id: 10, time: '17:00', sortorder: 10, bookedCapacity: 0, bookingAvailable: true },
  { id: 11, time: '18:00', sortorder: 11, bookedCapacity: 0, bookingAvailable: true },
  { id: 12, time: '19:00', sortorder: 12, bookedCapacity: 0, bookingAvailable: true }
  ];
  //alltimes: any = [];
  WeekDays: any = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  showEndDate: boolean = false;
  isSucccess: boolean;
  branchList: Branch[];
  branchVideoCapacity: number;
  StartDate: Date;
  allSlot: number;
  allCheckBox: boolean = false;
  newTime: Date;
  seletectDateFromInput: Date;
  result: number;
  validatetimeslots: boolean = true;
  hourvalidator: boolean;
  minvalidator: boolean;
  checkmultipleappointmentslots: boolean = false;
  BranchId: number = 0;
  EndDate: Date;
  bsValue: string;
  endDateValue: string;
  branchnotselected: boolean = false;
  constructor(private route: ActivatedRoute, private router: Router,
    private _appointmentService: AppointmentService
    , private _branchService: BranchService,
    private _previousRouteService: PreviousRouteService) {
    this.addValuesInTimeSlots(0, false);
    this.getBranchList();
    this.bookingType = this.route.snapshot.params["type"];
    this.BranchId = this.route.snapshot.params["id"];
    if (this.BranchId == undefined) {
      this.BranchId = 0;
    }
    this.actionName = 'Save';
  }


  ngOnInit() {

    if (this.BranchId != 3) {
      this.getHeadBranchCapacity();
    }
    this.loadDefaultDateSlots();
  }
  loadDefaultDateSlots(): any {
    this.alltimes = [{ id: 1, time: '8:00', sortorder: 1, bookedCapacity: 0, bookingAvailable: true },
    { id: 2, time: '9:00', sortorder: 2, bookedCapacity: 0, bookingAvailable: true },
    { id: 3, time: '10:00', sortorder: 3, bookedCapacity: 0, bookingAvailable: true },
    { id: 4, time: '11:00', sortorder: 4, bookedCapacity: 0, bookingAvailable: true },
    { id: 5, time: '12:00', sortorder: 5, bookedCapacity: 0, bookingAvailable: true },
    { id: 6, time: '13:00', sortorder: 6, bookedCapacity: 0, bookingAvailable: true },
    { id: 7, time: '14:00', sortorder: 7, bookedCapacity: 0, bookingAvailable: true },
    { id: 8, time: '15:00', sortorder: 8, bookedCapacity: 0, bookingAvailable: true },
    { id: 9, time: '16:00', sortorder: 9, bookedCapacity: 0, bookingAvailable: true },
    { id: 10, time: '17:00', sortorder: 10, bookedCapacity: 0, bookingAvailable: true },
    { id: 11, time: '18:00', sortorder: 11, bookedCapacity: 0, bookingAvailable: true },
    { id: 12, time: '19:00', sortorder: 12, bookedCapacity: 0, bookingAvailable: true }
    ];
    this.addValuesInTimeSlots(0, false);
  }
  getHeadBranchCapacity() {
    this._branchService.getHeadBranchCapacity().subscribe(res => {
      this.HeadOfficeTotalCapacity = res;
    });
  }

  onStartDateSelect(value: Date): void {
    if (value != null) {
      if (this.bookingType == 2) {
        if (this.BranchId == 0) {
          this.branchnotselected = true;
          this.bsValue = '';
          alert('Please Select Branch');
        }
        else {
          this.seletectDateFromInput = new Date(value);
          var currentDate = new Date(value);
          var date = currentDate.getDate();
          var month = currentDate.getMonth(); //Be careful! January is 0 not 1
          var year = currentDate.getFullYear();
          var dateString = year + "-" + (month + 1) + "-" + date;

          if (year > 1980) {
            var searchParams = {
              'branchId': this.BranchId == 3 ? this.BranchId : this.custForm.value.BranchId, 'appointmentType': this.bookingType, 'selectedStartDate': dateString,
              'selectedDay': this.WeekDays[currentDate.getDay()]
            };
            this._appointmentService.getbranchAppointmentList(searchParams).subscribe(res => {

              this.alltimes = [];
              if (res != null && res.length != 0) {
                res.forEach(element => {
                  this.alltimes.push({
                    id: element.SortOrder,
                    time: element.AppointmentTimeString, sortorder: element.SortOrder,
                    bookedCapacity: element.TotalCapacity,
                    bookingAvailable: element.isActive
                  });
                });
                this.addValuesInTimeSlots(0, false);
              } else {
                this.alltimes = [{ id: 1, time: '8:00', sortorder: 1, bookedCapacity: 0, bookingAvailable: true },
                { id: 2, time: '9:00', sortorder: 2, bookedCapacity: 0, bookingAvailable: true },
                { id: 3, time: '10:00', sortorder: 3, bookedCapacity: 0, bookingAvailable: true },
                { id: 4, time: '11:00', sortorder: 4, bookedCapacity: 0, bookingAvailable: true },
                { id: 5, time: '12:00', sortorder: 5, bookedCapacity: 0, bookingAvailable: true },
                { id: 6, time: '13:00', sortorder: 6, bookedCapacity: 0, bookingAvailable: true },
                { id: 7, time: '14:00', sortorder: 7, bookedCapacity: 0, bookingAvailable: true },
                { id: 8, time: '15:00', sortorder: 8, bookedCapacity: 0, bookingAvailable: true },
                { id: 9, time: '16:00', sortorder: 9, bookedCapacity: 0, bookingAvailable: true },
                { id: 10, time: '17:00', sortorder: 10, bookedCapacity: 0, bookingAvailable: true },
                { id: 11, time: '18:00', sortorder: 11, bookedCapacity: 0, bookingAvailable: true },
                { id: 12, time: '19:00', sortorder: 12, bookedCapacity: 0, bookingAvailable: true }
                ];
                this.addValuesInTimeSlots(0, false);
              }
            });
          }
        }
      } else {
        this.seletectDateFromInput = new Date(value);
        var currentDate = new Date(value);
        var date = currentDate.getDate();
        var month = currentDate.getMonth(); //Be careful! January is 0 not 1
        var year = currentDate.getFullYear();
        var dateString = year + "-" + (month + 1) + "-" + date;

        if (year > 1980) {
          var searchParams = {
            'branchId': this.BranchId == 3 ? this.BranchId : this.custForm.value.BranchId != undefined ? this.custForm.value.BranchId : 0, 'appointmentType': this.bookingType, 'selectedStartDate': dateString,              
            //'branchId': this.custForm.value.BranchId != undefined ? this.custForm.value.BranchId : 0, 
            //'appointmentType': this.bookingType, 'selectedStartDate': dateString,
            'selectedDay': this.WeekDays[currentDate.getDay()]
          }; this._appointmentService.getbranchAppointmentList(searchParams).subscribe(res => {

            this.alltimes = [];
            if (res != null && res.length != 0) {
              res.forEach(element => {
                this.alltimes.push({
                  id: element.SortOrder,
                  time: element.AppointmentTimeString, sortorder: element.SortOrder,
                  bookedCapacity: element.TotalCapacity,
                  bookingAvailable: element.Status
                });
              });
              this.addValuesInTimeSlots(0, false);
            } else {
              this.alltimes = [{ id: 1, time: '8:00', sortorder: 1, bookedCapacity: 0, bookingAvailable: true },
              { id: 2, time: '9:00', sortorder: 2, bookedCapacity: 0, bookingAvailable: true },
              { id: 3, time: '10:00', sortorder: 3, bookedCapacity: 0, bookingAvailable: true },
              { id: 4, time: '11:00', sortorder: 4, bookedCapacity: 0, bookingAvailable: true },
              { id: 5, time: '12:00', sortorder: 5, bookedCapacity: 0, bookingAvailable: true },
              { id: 6, time: '13:00', sortorder: 6, bookedCapacity: 0, bookingAvailable: true },
              { id: 7, time: '14:00', sortorder: 7, bookedCapacity: 0, bookingAvailable: true },
              { id: 8, time: '15:00', sortorder: 8, bookedCapacity: 0, bookingAvailable: true },
              { id: 9, time: '16:00', sortorder: 9, bookedCapacity: 0, bookingAvailable: true },
              { id: 10, time: '17:00', sortorder: 10, bookedCapacity: 0, bookingAvailable: true },
              { id: 11, time: '18:00', sortorder: 11, bookedCapacity: 0, bookingAvailable: true },
              { id: 12, time: '19:00', sortorder: 12, bookedCapacity: 0, bookingAvailable: true }
              ];
              this.addValuesInTimeSlots(0, false);
            }
          });
        }
      }

    }
  }

  onEndDateSelect(value: Date): void {
    if (value != null) {
      if (new Date(value) < new Date(this.custForm.value.StartDate)) {
        this.endDateValue = '';
        alert('Please Select End Date is greater than Start Date');
      }
    }
  }


  getBranchList() {
    if (this.bookingType != 1) {
      this.custForm != null ? this.custForm.value.BookingDateandTime : null;
      this._branchService.getBranchList().subscribe(res => {
        this.branchList = res;
      }
      );
    }

  }

  checkDates() {
    if (this.custForm.value.editdays == 1) {
      this.showEndDate = true;
    } else {
      this.showEndDate = false;
    }

  }

  private addValuesInTimeSlots(capacity: number, status: boolean) {
    var returnedappointmenttimelist = [];
    this.alltimes.forEach((element, idx) => {
      let newappointmenttime = {
        Appointmenthour: element.time.split(':')[0],
        Appointmentmin: element.time.split(':')[1],
        TotalCapacity: element.bookedCapacity,
        Status: element.bookingAvailable,
        index: idx
      }
      if (returnedappointmenttimelist.findIndex(x => (x.Appointmenthour + ':' + x.Appointmentmin) == element.time) == -1) {
        returnedappointmenttimelist.push(newappointmenttime);
      }
    });

    this.returnedappointmenttimelist = returnedappointmenttimelist;
  }




  validateslots(event, Appointmenthour, Appointmentmin) {
    var currentDate = new Date(this.custForm.value.StartDate);
    var date = currentDate.getDate();
    var month = currentDate.getMonth(); //Be careful! January is 0 not 1
    var year = currentDate.getFullYear();
    var dateString = year + "-" + (month + 1) + "-" + date;
    var timeString = dateString + " " + Appointmenthour + ":" + Appointmentmin;


    if (this.bookingType == 2) {
      var checkparams = { 'appointmentType': this.bookingType, 'appointmentDate': dateString, 'appointmentTime': timeString, "currentslotavailable": event.target.value, 'branchId': this.BranchId };
      this._branchService.validateBranchTotalCapacity(checkparams).subscribe(res => {
        this.result = res;
        if (this.result != 1) {
          this.validatetimeslots = false;
          if (this.checkmultipleappointmentslots != true) {
            alert('Please add Total Capacity below branch screens');
          }
          this.returnedappointmenttimelist.forEach((element, idx) => {
            element.TotalCapacity = 0;
          });
          event.target.value = 0;
        } else {
          this.validatetimeslots = true;
          if (this.bookingType == 2) {
            if (this.branchnotselected == true) {
              alert('Please Select Branch First')
            }
          }
        }

      });
    }
  }
  setAllSlots(event) {
    if (this.bookingType == 2) {
      if (this.branchnotselected == false) {
        this.checkmultipleappointmentslots = true;
        this.returnedappointmenttimelist.forEach((element, idx) => {
          this.validateslots(event, element.Appointmenthour, element.Appointmentmin);
        });

        setTimeout(() => {
          if (this.validatetimeslots == true) {
            this.returnedappointmenttimelist.forEach((element, idx) => {
              element.TotalCapacity = event.target.value;
            });
          } else {
            this.returnedappointmenttimelist.forEach((element, idx) => {
              element.TotalCapacity = 0;
            });
            alert('Please add Total Capacity below branch screens');
          }
        }, 500);
      }
      else {
        alert('Please Select Branch First');
      }
    } else {
      if (this.branchnotselected == false) {
        this.checkmultipleappointmentslots = true;
        this.returnedappointmenttimelist.forEach((element, idx) => {
          this.validateslots(event, element.Appointmenthour, element.Appointmentmin);
        });

        setTimeout(() => {
          if (this.validatetimeslots == true) {
            this.returnedappointmenttimelist.forEach((element, idx) => {
              element.TotalCapacity = event.target.value;
            });
          } else {
            this.returnedappointmenttimelist.forEach((element, idx) => {
              element.TotalCapacity = 0;
            });
            alert('Please add Total Capacity below branch screens');
          }
        }, 5000);
      }
    }
  }

  setAllSlotsCheck(event) {
    this.returnedappointmenttimelist.forEach((element, idx) => {
      element.Status = event.target.checked;
    });
  }



  AddNewTime() {
    if (this.custForm.value.newTimeHour != null) {
      if (this.custForm.value.newTimeHour < 0 || this.custForm.value.newTimeHour >= 24) {
        this.hourvalidator = true;
      } else if (this.custForm.value.newTimeMin < 0 || this.custForm.value.newTimeMin >= 59) {
        this.minvalidator = true;
      }
      else {
        this.hourvalidator = false;
        this.minvalidator = false;
        this.alltimes.push({ id: this.alltimes.length + 1, time: this.custForm.value.newTimeHour + ':' + (this.custForm.value.newTimeMin == 0 ? "00" : this.custForm.value.newTimeMin), sortorder: this.alltimes.length + 1 });
        this.addValuesInTimeSlots(0, false);
      }
    }
  }

  AddAvailability() {
    if (this.custForm.valid) {
      if (this.bookingType == 2) {
        if (this.branchnotselected == true) {
          alert('Please Select Branch First')
        } else {
          if (this.showEndDate) {
            if (new Date(this.custForm.value.EndDate) > new Date(this.custForm.value.StartDate)) {
              if (this.BranchId != 3) {
                if (this.result == 1 || this.result == undefined) {
                  this.CreateAvailability();
                }
                else {
                  alert('Please add Total Capacity below branch screens');
                }
              }
              else {
                this.CreateAvailability();
              }
            }
            else {
              alert('Please Select End Date is greater than Start Date');
            }
          }
          else {
            if (this.BranchId != 3) {
              if (this.result == 1 || this.result == undefined) {
                this.CreateAvailability();
              }
              else {
                alert('Please add Total Capacity below branch screens');
              }
            } else {
              this.CreateAvailability();
            }
          }
        }
      } else {
        if (this.showEndDate) {
          if (new Date(this.custForm.value.EndDate) > new Date(this.custForm.value.StartDate)) {
            if (this.BranchId != 3) {
              if (this.result == 1 || this.result == undefined) {
                this.CreateAvailability();
              }
              else {
                alert('Please add Total Capacity below branch screens');
              }
            }
            else {
              this.CreateAvailability();
            }
          }
          else {
            alert('Please Select End Date is greater than Start Date');
          }
        }
        else {
          if (this.BranchId != 3) {
            if (this.result == 1 || this.result == undefined) {
              this.CreateAvailability();
            }
            else {
              alert('Please add Total Capacity below branch screens');
            }
          } else {
            this.CreateAvailability();
          }
        }
      }
    }
  }

  CreateAvailability() {
    var bid: number = 0;
    if (this.actionName == "Update") {
      //bid = this.customerToUpdate.Id;
    }

    var availablityType = this.bookingType;
    var newStartDate = new Date(this.custForm.value.StartDate);
    var startDate = newStartDate.getFullYear() + "/" + (newStartDate.getMonth() + 1) + "/" + newStartDate.getDate();
    var newEndDate = new Date(this.custForm.value.EndDate);
    var endDate = newEndDate.getFullYear() + "/" + (newEndDate.getMonth() + 1) + "/" + newEndDate.getDate();

    var timeSlots = [];
    this.alltimes.forEach((element, index) => {
      let newappointmenttime = {
        Time: element.time,
        Count: this.custForm.value['availability_' + (index)],
        IsAvailabe: this.custForm.value['checkbox_' + (index)]
      };
      timeSlots.push(newappointmenttime);
    });

    // for (let i = this.times[0]; i <= this.times[this.times.length - 1]; i++) {
    //   let newappointmenttime = {
    //     Time: i + ":00",
    //     Count: this.custForm.value['availability_' + i],
    //     IsAvailabe: this.custForm.value['checkbox_' + i]
    //   };
    //   timeSlots.push(newappointmenttime);
    // }

    var newSlot = new AppointmentSlotsModel(this.BranchId, availablityType, new Date(startDate), new Date(endDate), timeSlots);
    this._appointmentService.createAppointmentSlots(newSlot).subscribe(res => {

      this.isSucccess = res;
      if (this.isSucccess != null && this.isSucccess == true) {
        alert('Slots are saved successfully');
        if (this.route.snapshot.params["id"] != undefined && this.route.snapshot.params["id"] != 3) {
          this.router.navigate(['admin/branches']);
        } else {
          this.router.navigate(['admin/availability/' + this.bookingType]);
        }
      } else {
      }
    });
  }

  getcapacity(event): void {
    this.branchnotselected = false;
    var newVal = event.target.value;
    this.BranchId = event.target.value;
    var branch = this.branchList.filter(x => x.Id == newVal);
    if (branch != null && branch.length != 0) {
      this.branchVideoCapacity = branch[0].VideoCapacity;
    }
  }


  removeThisSlot(idex) {
    this.alltimes.splice(idex, 1);
    this.addValuesInTimeSlots(0, false);
  }
  paste() {
    return false;
  }

}
