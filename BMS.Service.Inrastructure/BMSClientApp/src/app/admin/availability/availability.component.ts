import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppointmentService } from '../../services/miscservices/appointment.service';
import { AppointmentModel } from '../../models/appointmentModel';
import { BranchService } from '../../services/miscservices/branch.service';
import { Branch } from '../../models/branch.model';
export interface PageChangedEvent {
  itemsPerPage: number;
  page: number;
}

@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.css'],
  providers:[AppointmentService,BranchService]
})
export class AvailabilityComponent implements OnInit {


  appointmentlist: AppointmentModel[] = null;
  returnedappointmentlist: AppointmentModel[] = null;
  bookingType: number;
  returnedappointmenttimelist: any;
  AppointmentTime:number;
  bsValue:any;
  branchId: number=0;
  branchList:Branch[];
  searchdateString: string = '';
  searchtimeString: string= '';

  constructor(private route: ActivatedRoute, private router: Router,
     private _appointmentService: AppointmentService,
     private _branchservice: BranchService) {    
    this.bookingType = this.route.snapshot.params["type"];
    this.branchId = this.route.snapshot.params["branchId"];
    if(this.branchId==undefined){
      this.branchId = 0
    }
    if(this.branchId==3){
      this.searchHeadBranch();
    }
  }

  ngOnInit() {
    if(this.branchId!=3){
    this.loadAppointments();
    }
    this.getBranchList();
    this.timelist();
    this.AppointmentTime = 0;
  }


  loadAppointments(){
    var searchParams = { 'appointmentType': this.bookingType,'sortOrder':'desc','branchId':this.branchId };
    this._appointmentService.getadminAppointmentList(searchParams).subscribe(res => {
      this.appointmentlist = res;     
      this.returnedappointmentlist = this.appointmentlist.slice(0, 10);
    });
  }

  searchAppointments(currentDate: Date): void {
    if(currentDate!=null)
    {
      var date = currentDate.getDate();
    var month = currentDate.getMonth(); //Be careful! January is 0 not 1
    var year = currentDate.getFullYear();
    var dateString = year+  "-" + (month + 1)+ "-" + date;
this.searchdateString = dateString;
    var searchParams = { 'branchId': this.branchId, 'sortOrder':'desc','appointmentType': this.bookingType, 'selectedDate': dateString, 'selectedTime': this.searchtimeString };
    this._appointmentService.getadminAppointmentList(searchParams).subscribe(res => {
      this.appointmentlist = res;     
      this.returnedappointmentlist = this.appointmentlist.slice(0, 10);
    });
  }else{
    var searchParams2 = { 'branchId': 0, 'appointmentType': this.bookingType };
    this._appointmentService.getadminAppointmentList(searchParams2).subscribe(res => {
      this.appointmentlist = res;     
      this.returnedappointmentlist = this.appointmentlist.slice(0, 10);
    });
  }
  }
  searchAppointmentsTime(event): void {  // event will give you full breif of action
    var newVal = event.target.value;
    var timeString = newVal + ":00:00";
    this.searchtimeString = timeString;
    var searchParams = { 'branchId': this.branchId, 'sortOrder':'desc','appointmentType': this.bookingType, 'selectedDate': this.searchdateString, 'selectedTime': timeString };
    this._appointmentService.getadminAppointmentList(searchParams).subscribe(res => {
      this.appointmentlist = res;     
      this.returnedappointmentlist = this.appointmentlist.slice(0, 10);
    });
  }

  toTime(timeString){
    var timeTokens = timeString.split(':');
    return new Date(1970,0,1, timeTokens[0], timeTokens[1], timeTokens[2]).toLocaleTimeString("en-US",{hour:"numeric",minute:"numeric"});
}

  timelist() {
    var searchParams = { 'appointmentType': this.bookingType };
    this._appointmentService.getAllTimeSlots(searchParams).subscribe(res => {
      this.returnedappointmenttimelist =  res;
    });    
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedappointmentlist = this.appointmentlist.slice(startItem, endItem);
  }


  searchHeadBranch(){
    var searchParams2 = { 'branchId': 3, 'appointmentType': this.bookingType };
    this._appointmentService.getadminAppointmentList(searchParams2).subscribe(res => {
      this.appointmentlist = res;     
      this.returnedappointmentlist = this.appointmentlist.slice(0, 10);
    });
  }

  loadAppointmentListByBranch(event){
    this.branchId = event.target.value;
    var searchParams = { 'branchId': event.target.value, 'appointmentType': this.bookingType,'sortOrder':'desc','selectedDate': this.searchdateString,'selectedTime': this.searchtimeString };    
    this._appointmentService.getadminAppointmentList(searchParams).subscribe(res => {
      this.appointmentlist = res;     
      this.returnedappointmentlist = this.appointmentlist.slice(0, 10);
    });
  }

  getBranchList(): any {
    this._branchservice.getBranchList().subscribe(res=>{
 this.branchList = res;
    });
   }
}
