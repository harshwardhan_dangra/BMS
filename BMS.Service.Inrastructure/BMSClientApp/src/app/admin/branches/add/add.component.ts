import { Component, OnInit, ViewChild } from '@angular/core';
import { BookingList } from '../../../models/bookingModel';
import { Branch } from '../../../models/branch.model';
import { NgForm } from '@angular/forms';
import { BranchService } from '../../../services/miscservices/branch.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers:[BranchService]
})
export class AddComponent implements OnInit {
  @ViewChild('custForm') custForm: NgForm;
  Name : string;
  Address:  string;
  VideoScreens: number;
  branch: Branch=null;  
  actionName: string = '';
  branchId:number;
  BookingNotes: string;
  MondayOpeningHours:string;
  MondayClosingHours:string;
  TuesdayOpeningHours:string;
  TuesdayClosingHours:string;
  WednesdayOpeningHours:string;
  WednesdayClosingHours:string;
  ThursdayOpeningHours:string;
  ThursdayClosingHours:string;
  FridayOpeningHours:string;
  FridayClosingHours:string;
  SaturdayOpeningHours:string;
  SaturdayClosingHours:string;
  SundayOpeningHours:string;
  SundayClosingHours:string;

  constructor(private _branchService:BranchService,
    private route: ActivatedRoute,private router: Router) { 


      var paramId = this.route.snapshot.params["id"];
      this.branchId = paramId;
      if (paramId > 0) {
        this.actionName = 'Update';
        this.FillCustomerForm(paramId);
      } else {
        this.actionName = 'Save';
      }


    }

  ngOnInit() {
  }



  FillCustomerForm(custId: number) {
    this._branchService.getBranchByID(custId).subscribe(res => {
      this.branch = res;
      if (this.branch != null && this.branch.Id > 0) {        
        this.Name = this.branch.Name;
        this.Address = this.branch.Address;
        this.VideoScreens = this.branch.VideoCapacity; 
        this.BookingNotes = this.branch.BookingNotes;    
        this.MondayOpeningHours = this.branch.MondayOpeningHours;
        this.MondayClosingHours = this.branch.MondayClosingHours;
        this.TuesdayOpeningHours = this.branch.TuesdayOpeningHours;
        this.TuesdayClosingHours= this.branch.TuesdayClosingHours;
        this.WednesdayOpeningHours =this.branch.WednesdayOpeningHours;
        this.WednesdayClosingHours = this.branch.WednesdayClosingHours;
        this.ThursdayOpeningHours = this.branch.ThursdayOpeningHours;
        this.ThursdayClosingHours = this.branch.ThursdayClosingHours;
        this.FridayOpeningHours = this.branch.FridayOpeningHours;
        this.FridayClosingHours = this.branch.FridayClosingHours;
        this.SaturdayOpeningHours = this.branch.SaturdayOpeningHours;
        this.SaturdayClosingHours = this.branch.SaturdayClosingHours;
        this.SundayOpeningHours = this.branch.SundayOpeningHours;
        this.SundayClosingHours = this.branch.SundayClosingHours;  
      }
    });

  }



  AddBranch (){
    if (this.custForm.valid) {
      this.CreateBooking();
    }
  }



  CreateBooking() {
    var bid: number = 0;
    if (this.actionName == "Update") {
      bid = this.branch.Id;
    }    
    var newbranch = new Branch(bid, 
      this.custForm.value.Name,
      this.custForm.value.Address,
      this.custForm.value.VideoScreens,
      this.custForm.value.BookingNotes,
      this.custForm.value.MondayOpeningHours,
      this.custForm.value.MondayClosingHours,
      this.custForm.value.TuesdayOpeningHours,
      this.custForm.value.TuesdayClosingHours,
      this.custForm.value.WednesdayOpeningHours,
      this.custForm.value.WednesdayClosingHours,
      this.custForm.value.ThursdayOpeningHours,
      this.custForm.value.ThursdayClosingHours,
      this.custForm.value.FridayOpeningHours,
      this.custForm.value.FridayClosingHours,
      this.custForm.value.SaturdayOpeningHours,
      this.custForm.value.SaturdayClosingHours,
      this.custForm.value.SundayOpeningHours,
      this.custForm.value.SundayClosingHours,
      );
    this._branchService.createBranch(newbranch).subscribe(res => {

      this.branch = res;
      if (this.branch != null && this.branch.Id >= 0) {
        alert('successful');
        if(this.branchId==3)
        {
          this.router.navigate(['/admin/dashboard']);
          this.branch = null;
        }else{
          this.router.navigate(['/admin/branches']);
        this.branch = null;}
      } else {
      }
    });

    // this._customerservice.createCustomer().subscribe(res => {
    //   this.ServiceResult = res;
    //   if (this.ServiceResult == 201) {
    //     this.router.navigate(['/admin/customer']);
    //   }
    // });
  }
}
