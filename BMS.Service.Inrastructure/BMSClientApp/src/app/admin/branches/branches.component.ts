import { Component, OnInit } from '@angular/core';
import { BranchService } from '../../services/miscservices/branch.service';
import { Branch } from '../../models/branch.model';
import { ActivatedRoute, Router } from '@angular/router';
import { UUID } from 'angular2-uuid';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { CommonService } from 'src/app/services/common/common.service';
export interface PageChangedEvent {
  itemsPerPage: number;
  page: number;
}

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css'],
  providers:[BranchService]
})
export class BranchesComponent implements OnInit {
  branchlist : Branch[] = null;
  returnedbranchlist: Branch[] = null;
  mytime: Date = new Date();
status:number;
  filename: string;
  constructor(private _branchservice: BranchService,
    private route: ActivatedRoute,
    private router: Router) { 
    var status = this.route.snapshot.params["status"];  
    this.status= status;
  }

  ngOnInit() {

    if(this.status!=0)
   { this.loadBranchesList();
  }else{
    this.loadArchiveBranchesList();
  }
  }
  loadArchiveBranchesList(): any {
    var search = {status:this.status};
    this._branchservice.getArchiveBranchList(search).subscribe(res => {
      this.branchlist = res;
      this.returnedbranchlist = this.branchlist.slice(0, 10);
    });
  }

  loadBranchesList(){
    this._branchservice.getBranchList().subscribe(res => {
      this.branchlist = res;
      this.returnedbranchlist = this.branchlist.slice(0, 10);
    });
  }

  
  isDeleteSuccess:boolean;
  DeleteBranch(id: number) {
    if (confirm("Are you sure to delete this record?")) {
      this._branchservice.deleteBranch(id)
        .subscribe(res => {
          this.isDeleteSuccess = res;
          if (this.isDeleteSuccess) {
            this.loadBranchesList();
            alert("Deleted Successfully");
            
          }
        })
    }
  }

  ArchiveBranch(id: number) {
    if (confirm("Are you sure to archive this record?")) {
      this._branchservice.archiveBranch(id)
        .subscribe(res => {
            this.loadBranchesList();
            alert("Archived Successfully");            
        })
    }
  }

  UnArchiveBranch(id: number) {
    if (confirm("Are you sure to unarchive this record?")) {
      this._branchservice.unarchiveBranch(id)
        .subscribe(res => {
            this.loadBranchesList();
            alert("UnArchived Successfully");            
        })
    }
  }
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedbranchlist = this.branchlist.slice(startItem, endItem);
  }


  ExportBranch(){
    var options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: false, 
      showTitle: false,
      useBom: false,
      noDownload: false,
      headers: ["Branch", "Address", "Video Capacity"]
    };
    var returnedbranchlist = [];
    this.returnedbranchlist.forEach(element=>{
      returnedbranchlist.push({
        Branch:element.Name==null?'':element.Name,
        Address:element.Address==null?'':element.Address,
        VideoCapacity:element.VideoCapacity==null?'':element.VideoCapacity
    });

    });
    if(this.status==0){
      this.filename = 'Archived Branches' + UUID.UUID();
    }else{
    this.filename = 'Branches' + UUID.UUID();
  }
    new Angular5Csv(returnedbranchlist, this.filename, options);
  }

}
