import { Component, OnInit } from '@angular/core';
import { BookingService } from '../../services/booking/booking.service';
import { BookingList } from '../../models/bookingModel';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-scheduledmail',
  templateUrl: './scheduledmail.component.html',
  styleUrls: ['./scheduledmail.component.css'],
  providers:[BookingService]
})
export class scheduledmailComponent implements OnInit {
  FirstName: String;
  LastName: String;
  ContactNumber: String;
  EmailAddress: String;
  Reason: String;
  Branch: String;
  BookingDateandTime: Date;
  actionName: string = '';
  BooingType: number;
  Comment:string;
  AssignedTo:string;
  AccessCode:string;
  updateBranchId: number;
  customerToUpdate: BookingList;
  bookingType: number;
  CallType: string;
  Location: string;

  constructor(private _bookingService: BookingService,
    private route: ActivatedRoute
  ) { 
    
  }

  ngOnInit() {
    var paramId = this.route.snapshot.params["id"];
    var bookingId = atob(decodeURIComponent(paramId));
    this.FillCustomerForm(bookingId);
  }
  FillCustomerForm(custId) {
    this._bookingService.getBookingByID(custId).subscribe(res => {
      this.customerToUpdate = res;
      if (this.customerToUpdate != null && this.customerToUpdate.Id > 0) {

        this.FirstName = this.customerToUpdate.FirstName;
        this.LastName = this.customerToUpdate.LastName;
        this.ContactNumber = this.customerToUpdate.ContactNumber;
        this.Comment = this.customerToUpdate.Comment;
        this.EmailAddress = this.customerToUpdate.EmailAddress;
        this.Reason = this.customerToUpdate.Reasonforenquiry;
        this.Branch = this.customerToUpdate.Branch;
        this.BookingDateandTime = this.customerToUpdate.BookingDateandTime;
        this.BooingType = this.customerToUpdate.BookingType;
        this.AssignedTo = this.customerToUpdate.AssignedUser;
        this.AccessCode = this.customerToUpdate.AccessCode;
        this.Location = this.customerToUpdate.Location;
        this.CallType = (this.BooingType==2?"Video":"Audio");
        
      }
    });
  }

}
