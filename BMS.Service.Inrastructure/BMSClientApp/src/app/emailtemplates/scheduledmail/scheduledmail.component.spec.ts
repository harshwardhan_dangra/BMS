import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { scheduledmailComponent } from './scheduledmail.component';

describe('scheduledmailComponent', () => {
  let component: scheduledmailComponent;
  let fixture: ComponentFixture<scheduledmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ scheduledmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(scheduledmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
