import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { immediatemailComponent } from './immediatemail.component';

describe('immediatemailComponent', () => {
  let component: immediatemailComponent;
  let fixture: ComponentFixture<immediatemailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ immediatemailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(immediatemailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
