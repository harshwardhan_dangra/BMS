import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { RouterModule } from "@angular/router";
import { LoginComponent } from "./account/login/login.component";
import { RegistrationComponent } from "./account/registration/registration.component";
import { AuthGaurd } from "./services/gaurds/auth.gaurds.service";
import { NotAuthorizedComponent } from "./not-authorized/not-authorized.component";
import { PagenotfoundComponent } from "./pagenotfound/pagenotfound.component";
import { MainappComponent } from "./mainapp/mainapp.component";
import { HomeComponent } from "./home/home.component";
import { BookinglistComponent } from "./bookinglist/bookinglist.component";
import { AdmindashboardComponent } from "./admin/admindashboard/admindashboard.component";
import { BranchesComponent } from "./admin/branches/branches.component";
import { CustomersComponent } from "./admin/customers/customers.component";
import { AvailabilityComponent } from "./admin/availability/availability.component";
import { AddComponent } from "./admin/branches/add/add.component";
import { AddcustomersComponent } from "./admin/customers/addcustomers/addcustomers.component";
import { AddavailabilityComponent } from "./admin/availability/addavailability/addavailability.component";
import { UsersComponent } from "./admin/users/users.component";
import { AdduserComponent } from "./admin/users/adduser/adduser.component";
import { UserdetailComponent } from "./admin/users/userdetail/userdetail.component";
import { SettingsComponent } from "./admin/settings/settings.component";
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { ViewbookingComponent } from "./bookinglist/view/viewbooking.component";
import { ViewCustomersComponent } from "./admin/customers/view/viewcustomers.component";
import { GuestBookingComponent } from "./guestbooking/guestbooking.component";
import { ThankComponent } from "./thank/thank.component";
import { immediatemailComponent } from "./emailtemplates/immediatemail/immediatemail.component";
import { scheduledmailComponent } from "./emailtemplates/scheduledmail/scheduledmail.component";


const appERouts: Routes = [
    { path:'', component:LoginComponent},
    { path:'booking/:type', component:GuestBookingComponent},
    { path: 'admin', component: LoginComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegistrationComponent },
    { path: 'no-auth', component: NotAuthorizedComponent },
    { path:'thank/:id',component:ThankComponent},
    { path: 'immediatemail/:id',component:immediatemailComponent},
    { path:'scheduledmail/:id',component:scheduledmailComponent},   
    {
        path: 'admin', canActivate: [AuthGaurd], component: MainappComponent,
        children: [            
            { path: 'home/:type',canActivate: [AuthGaurd], component: HomeComponent }, 
            { path: 'no-auth', component: NotAuthorizedComponent },                       
            { path: 'home/:type/:id', canActivate: [AuthGaurd], component: HomeComponent },
            { path: 'viewbooking/:type/:id', canActivate: [AuthGaurd], component: ViewbookingComponent },
            { path: 'archivebooking/:type/:status', canActivate: [AuthGaurd], component: BookinglistComponent },
            { path: 'bookinglist/:type', canActivate: [AuthGaurd], component: BookinglistComponent },
            { path: 'branchbookinglist/:type/:branchId', canActivate: [AuthGaurd], component: BookinglistComponent },
            { path: 'dashboard', canActivate: [AuthGaurd], component: AdmindashboardComponent },
            { path: 'branches', canActivate: [AuthGaurd], component: BranchesComponent },
            { path: 'archivebranches/:status', canActivate: [AuthGaurd], component: BranchesComponent },
            { path: 'customers', canActivate: [AuthGaurd], component: CustomersComponent },
            { path: 'availability/:type', canActivate: [AuthGaurd], component: AvailabilityComponent },
            { path: 'headbranchavailability/:type/:branchId', canActivate: [AuthGaurd], component: AvailabilityComponent },
            { path: 'addbranch', canActivate: [AuthGaurd], component: AddComponent },
            { path: 'addbranch/:id', canActivate: [AuthGaurd], component: AddComponent },            
            { path: 'viewcustomers/:id', canActivate: [AuthGaurd], component: ViewCustomersComponent },
            { path: 'addcustomers/:id', canActivate: [AuthGaurd], component: AddcustomersComponent },
            { path: 'addavailablity/:type/:id', canActivate: [AuthGaurd], component: AddavailabilityComponent },
            { path: 'addavailablity/:type', canActivate: [AuthGaurd], component: AddavailabilityComponent },
            { path: 'userlist', canActivate: [AuthGaurd], component: UsersComponent },
            { path: 'adduser', canActivate: [AuthGaurd], component: AdduserComponent },
            { path: 'adduser/:id', canActivate: [AuthGaurd], component: AdduserComponent },
            { path: 'profile/:type/:id', canActivate: [AuthGaurd], component: AdduserComponent },            
            { path: 'userdetail/:id', canActivate: [AuthGaurd], component: UserdetailComponent },
            { path: 'messagesettings', canActivate: [AuthGaurd], component: SettingsComponent }
            
            // { path: 'customer', canActivate: [AuthGaurd], component: CustomerComponent },
            // { path: 'department', canActivate: [AdminAuthGaurd], component: DepartmentComponent },
            // { path: 'reports', canActivate: [AuthGaurd], component: ReportsComponent },
            // { path: 'newcustomer', canActivate: [AuthGaurd], component: CustomerManageComponent },
            // { path: 'newcustomer/:id', canActivate: [AuthGaurd], component: CustomerManageComponent },
            // { path: 'newdepartment', canActivate: [AdminAuthGaurd], component: DepartmentManageComponent },
            // { path: 'newdepartment/:id', canActivate: [AdminAuthGaurd], component: DepartmentManageComponent }
        ]
    },
    { path: '**', component: PagenotfoundComponent }
]

@NgModule({    
    imports: [BrowserModule,RouterModule.forRoot(appERouts)],
    providers: [{provide: LocationStrategy, useClass: PathLocationStrategy}],
    exports: [RouterModule]    
})
export class AppRoutingModuel {
}