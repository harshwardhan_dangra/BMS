﻿using BMS.Core.Entities;

namespace BMS.Core.Services
{
    public class CommonAppLog : BaseEntity
    {

        public CommonAppLog() : base("CommonAppLogId") {
            MachineIp = "";
            ApplicationName = "";
            LogType = LogType.None;
            ComponentName = "";
            MethodName = "";
            Message = "";
            StackTrace = "";
            GatewayRequest = "";
            GatewayResponse = "";
            UserId = 0;
            ReportingEventName = "";
            //IsActive = true;
        }

        public long CommonAppLogId {get; set;}
        public string MachineIp {get; set;}
        public string ApplicationName {get; set;}
        public LogType LogType {get; set;}
        public string ComponentName {get; set;}
        public string MethodName {get; set;}
        public string Message {get; set;}
        public string StackTrace {get; set;}
        public string GatewayRequest {get; set;}
        public string GatewayResponse {get; set;}
        public long UserId {get; set;}
        public string ReportingEventName { get; set; }
    }
}