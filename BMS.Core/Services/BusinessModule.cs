﻿namespace BMS.Core.Services
{
    public class BusinessModule
    {
        public enum Module
        {
            Core,
            Security,
            Contract,
            SalesAnalysis,
            Royalty,
            Common
        }
    }
}
