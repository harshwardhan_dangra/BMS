﻿namespace BMS.Core.Data
{
    public enum Status
    {
          Active	= 1   
        , Published = 2
        , Deleted   = 3
        , Archived  = 4

    }
}