﻿using BMS.Common.Data;

namespace BMS.Common.Services
{
    public class CommonService : ICommonService
    {
        public ICommonDbContext CommonDbContext;

        public CommonService(ICommonDbContext commonDbContext)
        {

            this.CommonDbContext = commonDbContext;
        }

    }
}
