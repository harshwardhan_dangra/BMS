﻿using BMS.Common.BusinessObjects;
using BMS.Common.Entities;
using System.Collections.Generic;

namespace BMS.Common.Services
{
    public interface ITranslationService
    {
        Locale GetLocale(long id);
        Dictionary<string,string> GetDictionaryLocale(string culture);
        Locales GetAllLocales(string culture);
        Locale SaveLocale(Locale locale);
        bool DeleteLocale(long id);
    }
}
