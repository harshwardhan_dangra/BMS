﻿using BMS.Common.Entities;

namespace BMS.Common.Services
{
    public interface IUploadFileService
    {
        UploadFile SaveUploadFile(UploadFile uploadFile);
    }
}
