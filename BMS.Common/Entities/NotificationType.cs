﻿//using BMS.Core.Entities;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.Serialization;
//using System.Web;
using BMS.Core.Entities;
using System;
using System.Runtime.Serialization;

namespace BMS.Common.Entities
{
    [DataContract]
    public class NotificationType : BaseEntity
    {

        public NotificationType()
            : base("Id") { }

        [DataMember(Name = "id")]
        public int Id { get; set; }
        
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "dateCreated")]
        public DateTime DateCreated { get; set; }

        [DataMember(Name = "isActive")]
        public int IsActive { get; set; }

    }
}