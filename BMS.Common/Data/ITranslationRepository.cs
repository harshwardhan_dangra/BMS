﻿using BMS.Core.Data;
using BMS.Common.Entities;

namespace BMS.Common.Data
{
    public interface ITranslationRepository : IRepository<Locale>
    {

    }
}
