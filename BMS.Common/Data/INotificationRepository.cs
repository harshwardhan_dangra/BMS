﻿using BMS.Core.Data;
using BMS.Common.Entities;
using System.Collections.Generic;

namespace BMS.Common.Data
{
    public interface INotificationRepository : IRepository<Notification>
    {
        List<Notification> GetAllNotications();
        List<NotificationType> GetAllNoticationTypes();
    }
}
