﻿using BMS.Core.Data;

namespace BMS.Common.Data
{
    public interface ICommonDbContext : IDbContext
    {
        IUploadFileRepository UploadFileRepository { get; }
        ICommonBusinessRepository CommonBusinessRepository { get; }
        INotificationRepository NotificationRepository { get; }
        ITranslationRepository TranslationRepository { get; }
    
    }
}
