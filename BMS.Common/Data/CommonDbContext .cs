﻿using BMS.Core.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace BMS.Common.Data
{
    public class CommonDbContext : BaseDbContext, ICommonDbContext
    {
        public IUploadFileRepository UploadFileRepository { get { return new UploadFileRepository(this); } }
        public ICommonBusinessRepository CommonBusinessRepository { get { return new CommonBusinessRepository(this); } }

        public INotificationRepository NotificationRepository { get { return new NotificationRepository(this); } }


        public ITranslationRepository TranslationRepository { get { return new TranslationRepository(this); } }
        public CommonDbContext(Database database)
            : base(database)
        {
        }
    }

     
}

